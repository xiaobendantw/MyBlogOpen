﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.MyBlog.Domain.Base
{
    public class EntityList<T> where T : new()
    {
        public EntityList()
        {
            Data = new List<T>();
        }
        public int Page { get; set; }
        public int Total { get; set; }
        public List<T> Data { get; set; }
        public int TotalPage { get; set; }
    }
}

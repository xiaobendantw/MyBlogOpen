using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_Reptilian_Info : EntityBase
    {
        
        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 爬虫id
        /// </summary>
        [DisplayName(@"爬虫id")]
        public int ReptilianId { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [DisplayName(@"内容")]
        public string Content { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        [DisplayName(@"来源")]
        [MaxLength(500)]
        public string Source { get; set; }

        /// <summary>
        /// 火热度
        /// </summary>
        [DisplayName(@"火热度")]
        public int FieryDegree { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        [DisplayName(@"分类名称")]
        [MaxLength(200)]
        public string CategoryName { get; set; }

        /// <summary>
        /// 抓取时间
        /// </summary>
        [DisplayName(@"抓取时间")]
        public DateTime GrabTime { get; set; }


    }
}
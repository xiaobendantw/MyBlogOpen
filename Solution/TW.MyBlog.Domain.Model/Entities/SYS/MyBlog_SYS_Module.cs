﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_SYS_Module : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 父编号
        /// </summary>
        [DisplayName(@"父编号")]
        public Guid PID { get; set; }

        /// <summary>
        /// 模块名称
        /// </summary>
        [DisplayName(@"模块名称")]
        [MaxLength(255)]
        public string ModuleName { get; set; }

        /// <summary>
        /// 模块地址
        /// </summary>
        [DisplayName(@"模块地址")]
        [MaxLength(500)]
        public string ModuleUrl { get; set; }

        /// <summary>
        /// 排序编号
        /// </summary>
        [DisplayName(@"排序编号")]
        public int OrderID { get; set; }

        /// <summary>
        /// 模块样式
        /// </summary>
        [DisplayName(@"模块样式")]
        [MaxLength(40)]
        public string ModuleCls { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName(@"创建时间")]
        public DateTime CreateTime { get; set; }
        [NotMapped]
        public virtual MyBlog_SYS_Module FatherModule { get; set; }
        [NotMapped]
        public virtual List<MyBlog_SYS_Module> ChildModules { get; set; }
        [NotMapped]
        public virtual List<MyBlog_SYS_ModuleAction> ModuleActions { get; set; }

        [NotMapped]
        public virtual MyBlog_SYS_RoleAction RoleAction { get; set; }
    }
}
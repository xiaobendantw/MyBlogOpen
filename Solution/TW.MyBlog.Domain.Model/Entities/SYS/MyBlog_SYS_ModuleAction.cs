﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_SYS_ModuleAction : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 模块表编号
        /// </summary>
        [DisplayName(@"模块表编号")]
        public Guid ModuleID { get; set; }

        /// <summary>
        /// 功能名称
        /// </summary>
        [DisplayName(@"功能名称")]
        [MaxLength(255)]
        public string ActionName { get; set; }

        /// <summary>
        /// 页面地址
        /// </summary>
        [DisplayName(@"页面地址")]
        [MaxLength(500)]
        public string ActionUrl { get; set; }

        /// <summary>
        /// 排序编号
        /// </summary>
        [DisplayName(@"排序编号")]
        public int OrderID { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// 是否在菜单上显示
        /// </summary>
        [DisplayName(@"是否在菜单上显示")]
        public bool ShowEnum { get; set; }

        /// <summary>
        /// 操作按钮样式
        /// </summary>
        [DisplayName(@"操作按钮样式")]
        [MaxLength(40)]
        public string ActionCls { get; set; }

        /// <summary>
        /// 操作按钮事件
        /// </summary>
        [DisplayName(@"操作按钮事件")]
        [MaxLength(40)]
        public string ActionFun { get; set; }

        /// <summary>
        /// 权重
        /// </summary>
        [DisplayName(@"权重")]
        public int Weight { get; set; }
        [NotMapped]
        public virtual MyBlog_SYS_Module Module { get; set; }


    }
}
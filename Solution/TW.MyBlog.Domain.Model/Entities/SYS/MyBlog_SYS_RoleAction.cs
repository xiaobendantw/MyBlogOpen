﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_SYS_RoleAction : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 角色编号
        /// </summary>
        [DisplayName(@"角色编号")]
        public Guid RoleID { get; set; }

        /// <summary>
        /// 模块编号
        /// </summary>
        [DisplayName(@"模块编号")]
        public Guid ModuleID { get; set; }

        /// <summary>
        /// 权重
        /// </summary>
        [DisplayName(@"权重")]
        public int Weight { get; set; }
        [NotMapped]
        public virtual MyBlog_SYS_Role Role { get; set; }
        [NotMapped]
        public virtual MyBlog_SYS_Module Module { get; set; }
    }
}
﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_SYS_Province : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        [DisplayName(@"名称")]
        [MaxLength(50)]
        public string ProvinceName { get; set; }

        /// <summary>
        /// 省市编码
        /// </summary>
        [DisplayName(@"省市编码")]
        [MaxLength(50)]
        public string ProvinceCode { get; set; }
        public virtual string PyProvinceName { get; set; }
    }
}
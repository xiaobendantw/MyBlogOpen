﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_SYS_Role : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        [DisplayName(@"角色名称")]
        [MaxLength(20)]
        public string RoleName { get; set; }

        /// <summary>
        /// 角色描述
        /// </summary>
        [DisplayName(@"角色描述")]
        [MaxLength(50)]
        public string Memo { get; set; }

        /// <summary>
        /// 排序编号
        /// </summary>
        [DisplayName(@"排序编号")]
        public int OrderID { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }
        [NotMapped]
        public virtual List<MyBlog_SYS_RoleAction> RoleActions { get; set; } 
    }
}
﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_SYS_PhoneMessageRecord : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 发送时间
        /// </summary>
        [DisplayName(@"发送时间")]
        public DateTime SendTime { get; set; }

        /// <summary>
        /// 发送手机号
        /// </summary>
        [DisplayName(@"发送手机号")]
        [MaxLength(11)]
        public string SendPhoneNumber { get; set; }

        /// <summary>
        /// 发送人OpenId
        /// </summary>
        [DisplayName(@"发送人OpenId")]
        [MaxLength(50)]
        public string SendOpenId { get; set; }

        /// <summary>
        /// 发送人IP地址
        /// </summary>
        [DisplayName(@"发送人IP地址")]
        [MaxLength(15)]
        public string SendIpAddr { get; set; }

        /// <summary>
        /// 验证码
        /// </summary>
        [DisplayName(@"验证码")]
        [MaxLength(6)]
        public string Code { get; set; }

        /// <summary>
        /// 完整短信内容
        /// </summary>
        [DisplayName(@"完整短信内容")]
        [MaxLength(200)]
        public string Contents { get; set; }

        /// <summary>
        /// 发送人类型
        /// </summary>
        [DisplayName(@"发送人类型")]
        public Enum_PhoneMessageSendUserType SendUserType { get; set; }

        /// <summary>
        /// 发送人编号
        /// </summary>
        [DisplayName(@"发送人编号")]
        public Guid? SendUserId { get; set; }

        /// <summary>
        /// 发送类型
        /// </summary>
        [DisplayName(@"发送类型")]
        public Enum_PhoneMessageSendType SendType { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }

        public void SendPhone(Enum_PhoneMessageSendType sendType, Enum_PhoneMessageSendUserType sendUserType, Guid? sendUserId,string code,string contents,string sendPhoneNumber,string sendOpenId,string sendIpAddr)
        {
            ID = Guid.NewGuid();
            IsDeleted = false;
            SendUserType = sendUserType;
            SendType = sendType;
            SendUserId = sendUserId;
            Code = code;
            Contents = contents;
            SendTime=DateTime.Now;
            SendPhoneNumber = sendPhoneNumber;
            SendOpenId = sendOpenId;
            SendIpAddr = sendIpAddr;
        }
    }
}
﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_SYS_District : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 所属城市编号
        /// </summary>
        [DisplayName(@"所属城市编号")]
        public Guid CityId { get; set; }

        /// <summary>
        /// 区县名称
        /// </summary>
        [DisplayName(@"区县名称")]
        [MaxLength(50)]
        public string DistrictName { get; set; }

        /// <summary>
        /// 区县编码
        /// </summary>
        [DisplayName(@"区县编码")]
        [MaxLength(50)]
        public string DistrictCode { get; set; }


    }
}
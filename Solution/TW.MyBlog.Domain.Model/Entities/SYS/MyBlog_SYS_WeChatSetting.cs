﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_SYS_WeChatSetting : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        public Guid ID { get; set; }

        /// <summary>
        /// 公众号名称
        /// </summary>
        [DisplayName(@"公众号名称")]
        [MaxLength(50)]
        public string WeChatName { get; set; }

        /// <summary>
        /// 公众号原始ID
        /// </summary>
        [DisplayName(@"公众号原始ID")]
        [MaxLength(50)]
        public string WxChat_gID { get; set; }

        /// <summary>
        /// 微信Token
        /// </summary>
        [DisplayName(@"微信Token")]
        [MaxLength(50)]
        public string Token { get; set; }

        /// <summary>
        /// 微信绑定Url
        /// </summary>
        [DisplayName(@"微信绑定Url")]
        [MaxLength(200)]
        public string BindUrl { get; set; }

        /// <summary>
        /// AppID
        /// </summary>
        [DisplayName(@"AppID")]
        [MaxLength(50)]
        public string AppID { get; set; }

        /// <summary>
        /// AppSecret
        /// </summary>
        [DisplayName(@"AppSecret")]
        [MaxLength(50)]
        public string AppSecret { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName(@"创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }
    }
}
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_Post_PostComment : EntityBase
    {
        
        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 文章编号
        /// </summary>
        [DisplayName(@"文章编号")]
        public Guid PostId { get; set; }

        /// <summary>
        /// 评论编号
        /// </summary>
        [DisplayName(@"评论编号")]
        public Guid CommentId { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [DisplayName(@"内容")]
        public string Comment { get; set; }

        /// <summary>
        /// 评论人
        /// </summary>
        [DisplayName(@"评论人")]
        public Guid UserId { get; set; }

        /// <summary>
        /// IP
        /// </summary>
        [DisplayName(@"IP")]
        [MaxLength(200)]
        public string CommentIp { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName(@"创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }


    }
}
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_Post_PostInfo : EntityBase
    {
        
        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [DisplayName(@"标题")]
        [MaxLength(50)]
        public string Title { get; set; }

        /// <summary>
        /// 摘要
        /// </summary>
        [DisplayName(@"摘要")]
        [MaxLength(200)]
        public string Summary { get; set; }

        /// <summary>
        /// 详情
        /// </summary>
        [DisplayName(@"详情")]
        public string Content { get; set; }

        /// <summary>
        /// 分类
        /// </summary>
        [DisplayName(@"分类")]
        public Guid? CategoryId { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        [DisplayName(@"作者")]
        [MaxLength(20)]
        public string Author { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        [DisplayName(@"来源")]
        [MaxLength(40)]
        public string Source { get; set; }

        /// <summary>
        /// 链接
        /// </summary>
        [DisplayName(@"链接")]
        [MaxLength(200)]
        public string Liinks { get; set; }

        /// <summary>
        /// 发布日期
        /// </summary>
        [DisplayName(@"发布日期")]
        public DateTime? PubDate { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [DisplayName(@"更新时间")]
        public DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 点击
        /// </summary>
        [DisplayName(@"点击")]
        public int Clicks { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        [DisplayName(@"图片地址")]
        [MaxLength(200)]
        public string ImageUrl { get; set; }
        /// <summary>
        /// 三方图片地址
        /// </summary>
        [DisplayName(@"图片地址")]
        public string ImgUrlLink { get; set; }

        /// <summary>
        /// 是否运行评论
        /// </summary>
        [DisplayName(@"是否运行评论")]
        public bool IsComment { get; set; }

        /// <summary>
        /// seo关键字
        /// </summary>
        [DisplayName(@"seo关键字")]
        [MaxLength(200)]
        public string SeoKey { get; set; }

        /// <summary>
        /// seo描述
        /// </summary>
        [DisplayName(@"seo描述")]
        [MaxLength(400)]
        public string SeoDescrition { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName(@"创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }
        /// <summary>
        /// 是否推荐
        /// </summary>
        [DisplayName(@"是否推荐")]
        public bool IsSelect { get; set; }

    }
}
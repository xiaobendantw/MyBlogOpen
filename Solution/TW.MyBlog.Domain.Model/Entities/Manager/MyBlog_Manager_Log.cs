﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_Manager_Log : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 管理员编号
        /// </summary>
        [DisplayName(@"管理员编号")]
        public Guid UId { get; set; }

        /// <summary>
        /// 操作时间
        /// </summary>
        [DisplayName(@"操作时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [DisplayName(@"标题")]
        [MaxLength(200)]
        public string Titles { get; set; }
        /// <summary>
        /// 操作内容
        /// </summary>
        [DisplayName(@"操作内容")]
        public string Contents { get; set; }


        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// IP地址
        /// </summary>
        [DisplayName(@"IP地址")]
        [MaxLength(20)]
        public string IpAddress { get; set; }
        [NotMapped]
        public virtual MyBlog_Manager_BaseInfo UserBaseInfo { get; set; }
    }
}
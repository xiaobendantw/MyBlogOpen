﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_Manager_Role : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 角色编号
        /// </summary>
        [DisplayName(@"角色编号")]
        public Guid RoleId { get; set; }

        /// <summary>
        /// 会员编号
        /// </summary>
        [DisplayName(@"会员编号")]
        public Guid MemberId { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName(@"创建时间")]
        public DateTime CreateTime { get; set; }

        public virtual MyBlog_Manager_BaseInfo BaseInfo { get; set; }
        public virtual MyBlog_SYS_Role Role { get; set; }
    }
}
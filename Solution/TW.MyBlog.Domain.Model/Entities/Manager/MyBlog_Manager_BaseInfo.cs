﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Common.Enums;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_Manager_BaseInfo : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 登录名
        /// </summary>
        [DisplayName(@"登录名")]
        [MaxLength(20)]
        public string LoginName { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [DisplayName(@"密码")]
        [MaxLength(32)]
        public string PassWord { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [DisplayName(@"昵称")]
        [MaxLength(20)]
        public string NickName { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [DisplayName(@"状态")]
        public Enum_ManagerUserState State { get; set; }

        /// <summary>
        /// 上次登录IP
        /// </summary>
        [DisplayName(@"上次登录IP")]
        [MaxLength(20)]
        public string LastLoginIP { get; set; }

        /// <summary>
        /// 上次登录时间
        /// </summary>
        [DisplayName(@"上次登录时间")]
        public DateTime? LastLoginTime { get; set; }

        /// <summary>
        /// 本次登录IP
        /// </summary>
        [DisplayName(@"本次登录IP")]
        [MaxLength(20)]
        public string ThisLoginIP { get; set; }

        /// <summary>
        /// 本次登录时间
        /// </summary>
        [DisplayName(@"本次登录时间")]
        public DateTime? ThisLoginTime { get; set; }

        /// <summary>
        /// 累计登录次数
        /// </summary>
        [DisplayName(@"累计登录次数")]
        public int? LoginNumber { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName(@"创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }

        [NotMapped]
        public virtual List<MyBlog_Manager_Log> Logs { get; set; }
        [NotMapped]
        public virtual List<MyBlog_Manager_Role> Roles { get; set; }
    }
}
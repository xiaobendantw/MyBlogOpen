using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_Album_AlbumInfo : EntityBase
    {
        
        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        [DisplayName(@"图片地址")]
        [MaxLength(200)]
        public string ImageUrl { get; set; }

        /// <summary>
        /// 图片描述
        /// </summary>
        [DisplayName(@"图片描述")]
        [MaxLength(50)]
        public string ImageDecription { get; set; }

        /// <summary>
        /// 是否公开
        /// </summary>
        [DisplayName(@"是否公开")]
        public bool IsOpen { get; set; }

        /// <summary>
        /// 图片分类
        /// </summary>
        [DisplayName(@"图片分类")]
        public Guid ImageCategory { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName(@"创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }


    }
}
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_Web_WebSeting : EntityBase
    {
        
        /// <summary>
        /// ID
        /// </summary>
        [DisplayName(@"ID")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        [DisplayName(@"Title")]
        [MaxLength(20)]
        public string Title { get; set; }

        /// <summary>
        /// SummaryTitle
        /// </summary>
        [DisplayName(@"SummaryTitle")]
        [MaxLength(30)]
        public string SummaryTitle { get; set; }

        /// <summary>
        /// Copyright
        /// </summary>
        [DisplayName(@"Copyright")]
        [MaxLength(200)]
        public string Copyright { get; set; }

        /// <summary>
        /// BackGroundImage
        /// </summary>
        [DisplayName(@"BackGroundImage")]
        [MaxLength(200)]
        public string BackGroundImage { get; set; }

        /// <summary>
        /// BackGroudVideo
        /// </summary>
        [DisplayName(@"BackGroudVideo")]
        [MaxLength(300)]
        public string BackGroudVideo { get; set; }

        /// <summary>
        /// SeoKey
        /// </summary>
        [DisplayName(@"SeoKey")]
        [MaxLength(20)]
        public string SeoKey { get; set; }

        /// <summary>
        /// SeoDecrtion
        /// </summary>
        [DisplayName(@"SeoDecrtion")]
        [MaxLength(50)]
        public string SeoDecrtion { get; set; }

        /// <summary>
        /// CreateTime
        /// </summary>
        [DisplayName(@"CreateTime")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// IsDeleted
        /// </summary>
        [DisplayName(@"IsDeleted")]
        public bool IsDeleted { get; set; }


    }
}
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_Web_LiveMessage : EntityBase
    {
        
        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        [DisplayName(@"用户编号")]
        public Guid? UserId { get; set; }

        /// <summary>
        /// IP
        /// </summary>
        [DisplayName(@"IP")]
        [MaxLength(200)]
        public string IP { get; set; }

        /// <summary>
        /// 留言信息
        /// </summary>
        [DisplayName(@"留言信息")]
        public string Message { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName(@"创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }


    }
}
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_Web_IPVisit : EntityBase
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// IP
        /// </summary>
        [DisplayName(@"IP")]
        [MaxLength(200)]
        public string IP { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName(@"创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }
        /// <summary>
        /// 访问类型
        /// </summary>
        [DisplayName(@"访问类型")]
        public Enum_VisitType VisitType { get; set; }


    }
}
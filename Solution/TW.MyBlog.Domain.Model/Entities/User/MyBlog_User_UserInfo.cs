using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Domain.Model
{
    public class MyBlog_User_UserInfo : EntityBase
    {
        
        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [DisplayName(@"昵称")]
        [MaxLength(10)]
        public string Nickame { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [DisplayName(@"密码")]
        [MaxLength(200)]
        public string PawssWord { get; set; }

        /// <summary>
        /// qq三方
        /// </summary>
        [DisplayName(@"qq三方")]
        [MaxLength(200)]
        public string QQThird { get; set; }

        /// <summary>
        /// 微信三方
        /// </summary>
        [DisplayName(@"微信三方")]
        [MaxLength(200)]
        public string WeChatThird { get; set; }

        /// <summary>
        /// 最后一次登陆
        /// </summary>
        [DisplayName(@"最后一次登陆")]
        public DateTime? LastLoginTime { get; set; }

        /// <summary>
        /// Ip
        /// </summary>
        [DisplayName(@"Ip")]
        [MaxLength(200)]
        public string Ip { get; set; }

        /// <summary>
        /// 登陆次数
        /// </summary>
        [DisplayName(@"登陆次数")]
        public int? LoginCount { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [DisplayName(@"邮箱")]
        [MaxLength(200)]
        public string Email { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        [DisplayName(@"电话")]
        [MaxLength(11)]
        public string Tel { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName(@"创建时间")]
        public DateTime CreateTime { get; set; }

        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }


    }
}
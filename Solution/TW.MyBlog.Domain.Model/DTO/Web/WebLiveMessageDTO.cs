using System;
using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class WebLiveMessageDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// 用户编号
        /// </summary>
        [DisplayName(@"用户编号")]
        public Guid? UserId { get; set; }

        /// <summary>
        /// IP
        /// </summary>
        [DisplayName(@"IP")]
        public string IP { get; set; }

        /// <summary>
        /// 留言信息
        /// </summary>
        [DisplayName(@"留言信息")]
        public string Message { get; set; }

        public virtual DateTime CreateTime { get; set; }


    }
}
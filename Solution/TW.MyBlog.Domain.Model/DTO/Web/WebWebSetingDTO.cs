using System;
using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class WebWebSetingDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// Title
        /// </summary>
        [DisplayName(@"Title")]
        public string Title { get; set; }

        /// <summary>
        /// SummaryTitle
        /// </summary>
        [DisplayName(@"SummaryTitle")]
        public string SummaryTitle { get; set; }

        /// <summary>
        /// Copyright
        /// </summary>
        [DisplayName(@"Copyright")]
        public string Copyright { get; set; }

        /// <summary>
        /// BackGroundImage
        /// </summary>
        [DisplayName(@"BackGroundImage")]
        public string BackGroundImage { get; set; }

        /// <summary>
        /// BackGroudVideo
        /// </summary>
        [DisplayName(@"BackGroudVideo")]
        public string BackGroudVideo { get; set; }

        /// <summary>
        /// SeoKey
        /// </summary>
        [DisplayName(@"SeoKey")]
        public string SeoKey { get; set; }

        /// <summary>
        /// SeoDecrtion
        /// </summary>
        [DisplayName(@"SeoDecrtion")]
        public string SeoDecrtion { get; set; }

        public virtual DateTime CreateTime { get; set; }


    }
}
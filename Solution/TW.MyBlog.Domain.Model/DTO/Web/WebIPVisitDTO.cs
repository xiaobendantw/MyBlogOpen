using System;
using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class WebIPVisitDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// IP
        /// </summary>
        [DisplayName(@"IP")]
        public string IP { get; set; }

        public virtual DateTime CreateTime { get; set; }


    }
}
using System;
using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class AlbumAlbumInfoDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        [DisplayName(@"图片地址")]
        public string ImageUrl { get; set; }

        /// <summary>
        /// 图片描述
        /// </summary>
        [DisplayName(@"图片描述")]
        public string ImageDecription { get; set; }

        /// <summary>
        /// 是否公开
        /// </summary>
        [DisplayName(@"是否公开")]
        [JqGridColumnOpt(sortName:@"IsOpen")]
        public string IsOpenStr { get; set; }

        public virtual bool IsOpen { get; set; }

        /// <summary>
        /// 图片分类
        /// </summary>
        [DisplayName(@"图片分类")]
        public Guid ImageCategory { get; set; }

        public virtual DateTime CreateTime { get; set; }


    }
}
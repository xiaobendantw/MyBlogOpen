using System;
using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class AlbumAlbumCategoryDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        [DisplayName(@"分类名称")]
        public string CategoryName { get; set; }

        /// <summary>
        /// 分类描述
        /// </summary>
        [DisplayName(@"分类描述")]
        public string CategoryDecrtion { get; set; }

        public virtual DateTime CreateTime { get; set; }


    }
}
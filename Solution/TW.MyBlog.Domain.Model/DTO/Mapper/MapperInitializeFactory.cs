﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Domain.Model.DTO.Manager;
using Microsoft.Extensions.DependencyInjection;

namespace TW.MyBlog.Domain.Model.DTO
{
    public static class MapperInitializeFactory
    {
        public static void MapperInitialize(this IServiceCollection services)
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<MyBlog_Manager_BaseInfo, LoginUserDTO>();
                x.CreateMap<MyBlog_SYS_RoleAction, RoleActionDTO>();
            });
        }
    }
}
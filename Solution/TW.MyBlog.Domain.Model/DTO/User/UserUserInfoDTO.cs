using System;
using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class UserUserInfoDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// 昵称
        /// </summary>
        [DisplayName(@"昵称")]
        public string Nickame { get; set; }

        /// <summary>
        /// 密码
        /// </summary>
        [DisplayName(@"密码")]
        public string PawssWord { get; set; }

        /// <summary>
        /// qq三方
        /// </summary>
        [DisplayName(@"qq三方")]
        public string QQThird { get; set; }

        /// <summary>
        /// 微信三方
        /// </summary>
        [DisplayName(@"微信三方")]
        public string WeChatThird { get; set; }

        /// <summary>
        /// 最后一次登陆
        /// </summary>
        [DisplayName(@"最后一次登陆")]
        [JqGridColumnOpt(sortName:@"LastLoginTime")]
        public string LastLoginTimeStr { get; set; }

        public virtual DateTime? LastLoginTime { get; set; }

        /// <summary>
        /// Ip
        /// </summary>
        [DisplayName(@"Ip")]
        public string Ip { get; set; }

        /// <summary>
        /// 登陆次数
        /// </summary>
        [DisplayName(@"登陆次数")]
        public int? LoginCount { get; set; }

        /// <summary>
        /// 邮箱
        /// </summary>
        [DisplayName(@"邮箱")]
        public string Email { get; set; }

        /// <summary>
        /// 电话
        /// </summary>
        [DisplayName(@"电话")]
        public string Tel { get; set; }

        public virtual DateTime CreateTime { get; set; }


    }
}
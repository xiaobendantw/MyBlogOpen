using System;
using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class PostPostImageRelationDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// 文章编号
        /// </summary>
        [DisplayName(@"文章编号")]
        public Guid PostId { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        [DisplayName(@"图片地址")]
        public string ImageUrl { get; set; }

        public virtual DateTime CreateTime { get; set; }


    }
}
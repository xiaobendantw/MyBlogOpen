using System;
using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class PostPostInfoDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// 标题
        /// </summary>
        [DisplayName(@"标题")]
        public string Title { get; set; }
        /// <summary>
        /// 博文分类
        /// </summary>
        [DisplayName(@"博文分类")]
        public string CategoryName { get; set; }

        public virtual Guid? CategoryId { get; set; }

        /// <summary>
        /// 作者
        /// </summary>
        [DisplayName(@"作者")]
        public string Author { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        [DisplayName(@"来源")]
        public string Source { get; set; }

        /// <summary>
        /// 链接
        /// </summary>
        [DisplayName(@"链接")]
        public string Liinks { get; set; }

        /// <summary>
        /// 发布日期
        /// </summary>
        [DisplayName(@"发布日期")]
        [JqGridColumnOpt(sortName: @"PubDate")]
        public string PubDateStr { get; set; }

        public virtual DateTime? PubDate { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        [DisplayName(@"更新时间")]
        [JqGridColumnOpt(sortName: @"UpdateTime")]
        public string UpdateTimeStr { get; set; }

        public virtual DateTime? UpdateTime { get; set; }

        /// <summary>
        /// 点击
        /// </summary>
        [DisplayName(@"点击")]
        public int? Clicks { get; set; }

        /// <summary>
        /// 图片地址
        /// </summary>
        [DisplayName(@"图片地址")]
        [JqGridColumnOpt(renderFun: @"ShowImageUrl")]
        public string ImageUrl { get; set; }

        public virtual string ImageUrllink { get; set; }

        /// <summary>
        /// 是否运行评论
        /// </summary>
        [DisplayName(@"是否运行评论")]
        [JqGridColumnOpt(sortName: @"IsComment")]
        public string IsCommentStr { get; set; }

        public virtual bool? IsComment { get; set; }
        public virtual DateTime CreateTime { get; set; }


    }
}
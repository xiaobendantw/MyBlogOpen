using System;
using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class PostPostCommentDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// 文章编号
        /// </summary>
        [DisplayName(@"文章编号")]
        public Guid PostId { get; set; }

        /// <summary>
        /// 评论编号
        /// </summary>
        [DisplayName(@"评论编号")]
        public Guid CommentId { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [DisplayName(@"内容")]
        public string Comment { get; set; }

        /// <summary>
        /// 评论人
        /// </summary>
        [DisplayName(@"评论人")]
        public Guid UserId { get; set; }

        /// <summary>
        /// IP
        /// </summary>
        [DisplayName(@"IP")]
        public string CommentIp { get; set; }

        public virtual DateTime CreateTime { get; set; }


    }
}
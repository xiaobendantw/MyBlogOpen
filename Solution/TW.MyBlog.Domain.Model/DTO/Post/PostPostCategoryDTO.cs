using System;
using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class PostPostCategoryDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        [DisplayName(@"分类名称")]
        public string CategoryNAme { get; set; }

        /// <summary>
        /// FId
        /// </summary>
        [DisplayName(@"FId")]
        public Guid FId { get; set; }

        public virtual DateTime CreateTime { get; set; }
        public virtual bool IsDeleted { get; set; }

    }
}
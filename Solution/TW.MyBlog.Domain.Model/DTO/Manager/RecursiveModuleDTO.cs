﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.MyBlog.Domain.Model.DTO.Manager
{
    public class RecursiveModuleDTO
    {

        /// <summary>
        /// 编号
        /// </summary>
        [DisplayName(@"编号")]
        [Key]
        public Guid ID { get; set; }

        /// <summary>
        /// 父编号
        /// </summary>
        [DisplayName(@"父编号")]
        public Guid PID { get; set; }

        /// <summary>
        /// 模块名称
        /// </summary>
        [DisplayName(@"模块名称")]
        public string ModuleName { get; set; }

        /// <summary>
        /// 模块地址
        /// </summary>
        [DisplayName(@"模块地址")]
        public string ModuleUrl { get; set; }

        /// <summary>
        /// 排序编号
        /// </summary>
        [DisplayName(@"排序编号")]
        public int OrderID { get; set; }

        /// <summary>
        /// 模块地址
        /// </summary>
        [DisplayName(@"模块样式")]
        public string ModuleCls { get; set; }
        /// <summary>
        /// 逻辑删除
        /// </summary>
        [DisplayName(@"逻辑删除")]
        public bool IsDeleted { get; set; }

        /// <summary>
        /// 创建时间
        /// </summary>
        [DisplayName(@"创建时间")]
        public DateTime CreateTime { get; set; }
        public int Level { get; set; }
    }
}

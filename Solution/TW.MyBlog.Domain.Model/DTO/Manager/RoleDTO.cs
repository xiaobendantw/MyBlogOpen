﻿using System;
using System.ComponentModel;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class RoleDTO
    {
        /// <summary>
        /// 排序编号
        /// </summary>
        [DisplayName(@"排序编号")]
        [JqGridColumnOpt(80, JqGridAlign.center)]
        public int OrderID { get; set; }

        /// <summary>
        /// 角色名称
        /// </summary>
        [DisplayName(@"角色名称")]
        [JqGridColumnOpt(200)]
        public string RoleName { get; set; }

        /// <summary>
        /// 角色描述
        /// </summary>
        [DisplayName(@"角色描述")]
        public string Memo { get; set; }


        public virtual Guid ID { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO.Manager
{
    public class ModuleDTO
    {
        [DisplayName(@"排序编号")]
        [JqGridColumnOpt(80,JqGridAlign.center)]
        public int OrderID { get; set; }
        [DisplayName(@"模块名称")]
        public string ModuleName { get; set; }
        [DisplayName(@"图标样式")]
        public string ModuleCls { get; set; }

        public virtual Guid ID { get; set; }
        public virtual int ChildModuleCount { get; set; }
    }
}

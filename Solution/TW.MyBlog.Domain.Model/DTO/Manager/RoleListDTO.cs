﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.MyBlog.Domain.Model.DTO.Manager
{
    public class RoleListDTO
    {

        public virtual Guid ID { get; set; }
        public string RoleName { get; set; }
        public bool Ischeck { get; set; }

    }
}

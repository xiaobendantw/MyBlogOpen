﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.MyBlog.Infrastructure.Common.Enums;

namespace TW.MyBlog.Domain.Model.DTO.Manager
{
    public class LoginUserDTO
    {
        /// <summary>
        /// 编号
        /// </summary>
        public Guid ID { get; set; }

        /// <summary>
        /// 登录名
        /// </summary>
        public string LoginName { get; set; }
        /// <summary>
        /// 昵称
        /// </summary>
        public string NickName { get; set; }

        /// <summary>
        /// 角色编号
        /// </summary>
        public Guid RoleId { get; set; }
        public string PassWord { get; set; }
        public List<RoleActionDTO> UserActionList { get; set; }

        public List<RoleAction_ModuleDTO> DefActionList { get; set; }
        public string Menu { get; set; }

        public string LoginIp { get; set; }
        public Enum_ManagerUserState State { get; set; }
        public bool IsDeleted { get; set; }
    }
}

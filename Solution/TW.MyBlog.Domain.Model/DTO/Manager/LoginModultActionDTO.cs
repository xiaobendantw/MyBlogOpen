﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.MyBlog.Domain.Model.DTO.Manager
{
    public class LoginModultActionDTO
    {
        public string ActionUrl { get; set; }
        public bool ShowEnum { get; set; }
        public string ActionCls { get; set; }
        public string ActionFun { get; set; }
        public int OrderID { get; set; }
        public string ActionName { get; set; }
        public string FatherModuleName { get; set; }
        public string ModuleName { get; set; }
        public Guid ModuleID { get; set; }
        public int Weight { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.MyBlog.Domain.Model.DTO.Manager
{
    public class MenuModuleListDTO
    {
        public string ModuleName { get; set; }
        public IEnumerable<MenuChildModuleListDTO> ChildModule { get; set; } 
    }
    public class MenuChildModuleListDTO
    {
        public string ModuleName { get; set; }
        public IEnumerable<MenuChildActionListDTO> ChildAction { get; set; }
        public IEnumerable<MenuThreeChildModuleListDTO> ThreeChildModule { get; set; }
    }
    public class MenuThreeChildModuleListDTO
    {
        public string ModuleName { get; set; }
        public IEnumerable<MenuChildActionListDTO> ChildAction { get; set; }
    }
    public class MenuChildActionListDTO
    {
        public Guid ID { get; set; }
        public Guid ModuleId { get; set; }
        public string ActionName { get; set; }
        public bool IsCheck { get; set; }
        public int Weight { get; set; }
        public bool IsMenu { get; set; }
    }
}

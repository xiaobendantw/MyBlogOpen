﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.MyBlog.Domain.Model.DTO.Manager
{
    public class RoleActionDTO
    {
        public Guid RoleID { get; set; }

        /// <summary>
        /// 模块编号
        /// </summary>
        public Guid ModuleID { get; set; }
        public int Weight { get; set; }

        public List<RoleAction_ModuleDTO> ModuleDtos { get; set; }

    }
    public class RoleAction_ModuleDTO
    {
        public Guid ModuleID { get; set; }
        public Guid ActionID { get; set; }
        public string areaName { get; set; }
        public string controllerName { get; set; }
        public string ModuleActionName { get; set; }
        public string FatherModuleName { get; set; }
        public string ModuleName { get; set; }
        public string actionName { get; set; }
        public bool ShowEnum { get; set; }
        public string ActionCls { get; set; }
        public string ActionFun { get; set; }
        public int OrderId { get; set; }
        public string ActionUrl { get; set; }
    }
}

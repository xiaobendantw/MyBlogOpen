﻿using System;
using System.ComponentModel;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class LogDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// 管理员编号
        /// </summary>
        [DisplayName("登录名")]
        [JqGridColumnOpt(100)]
        public string LoginName { get; set; }

        /// <summary>
        /// 操作内容
        /// </summary>
        [DisplayName(@"操作标题")]
        [JqGridColumnOpt(200)]
        public string Titles { get; set; }
        /// <summary>
        /// 操作内容
        /// </summary>
        [DisplayName(@"操作内容")]
        [JqGridColumnOpt(isSort: false)]
        public string Contents { get; set; }
        [DisplayName("记录时间")]
        [JqGridColumnOpt(170,sortName: "CreateTime")]
        public string CreateTimeChs { get; set; }
        /// <summary>
        /// IP地址
        /// </summary>
        [DisplayName(@"IP地址")]
        [JqGridColumnOpt(120)]
        public string IpAddress { get; set; }
        public virtual DateTime CreateTime { get; set; }

    }
}
﻿using System;
using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.Utility.CustomerAttribute;
using System.Collections.Generic;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class BaseInfoDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// 登录名
        /// </summary>
        [DisplayName(@"登录名")]
        [JqGridColumnOpt(200)]
        public string LoginName { get; set; }


        /// <summary>
        /// 昵称
        /// </summary>
        [DisplayName(@"昵称")]
        [JqGridColumnOpt(200)]
        public string NickName { get; set; }

        /// <summary>
        /// 状态
        /// </summary>
        [DisplayName(@"状态")]
        [JqGridColumnOpt(40, sortName: "State")]
        public string StateName { get; set; }
        public virtual Enum_ManagerUserState State { get; set; }
        public virtual DateTime CreateTime { get; set; }
        /// <summary>
        /// 用户角色
        /// </summary>
        [DisplayName("用户角色")]
        public string MangerRoleName { get; set; }

        /// <summary>
        /// 上次登录IP
        /// </summary>
        [DisplayName(@"上次登录IP")]
        public string LastLoginIP { get; set; }

        /// <summary>
        /// 上次登录时间
        /// </summary>
        [DisplayName(@"上次登录时间")]
        [JqGridColumnOpt(sortName: "LastLoginTime")]
        public string LastLoginTimeStr { get; set; }
        public virtual DateTime? LastLoginTime { get; set; }

        /// <summary>
        /// 本次登录IP
        /// </summary>
        [DisplayName(@"本次登录IP")]
        public string ThisLoginIP { get; set; }

        /// <summary>
        /// 本次登录时间
        /// </summary>
        [DisplayName(@"本次登录时间")]
        [JqGridColumnOpt(sortName: "ThisLoginTime")]
        public string ThisLoginTimeStr { get; set; }
        public virtual DateTime? ThisLoginTime { get; set; }

        /// <summary>
        /// 累计登录次数
        /// </summary>
        [DisplayName(@"累计登录次数")]
        [JqGridColumnOpt(100,JqGridAlign.center)]
        public int? LoginNumber { get; set; }
        public virtual IEnumerable<string> MangerRole { get; set; }
    }
}
using System;
using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Domain.Model.DTO
{
    public class ReptilianInfoDTO
    {
        public virtual Guid ID { get; set; }

        /// <summary>
        /// 爬虫id
        /// </summary>
        [DisplayName(@"爬虫id")]
        public int ReptilianId { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        [DisplayName(@"内容")]
        public string Content { get; set; }

        /// <summary>
        /// 来源
        /// </summary>
        [DisplayName(@"来源")]
        public string Source { get; set; }

        /// <summary>
        /// 火热度
        /// </summary>
        [DisplayName(@"火热度")]
        public int FieryDegree { get; set; }

        /// <summary>
        /// 分类名称
        /// </summary>
        [DisplayName(@"分类名称")]
        public string CategoryName { get; set; }

        /// <summary>
        /// 抓取时间
        /// </summary>
        [DisplayName(@"抓取时间")]
        [JqGridColumnOpt(sortName:@"GrabTime")]
        public string GrabTimeStr { get; set; }

        public virtual DateTime GrabTime { get; set; }


    }
}
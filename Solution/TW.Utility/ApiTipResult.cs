﻿using System;
using System.Collections.Generic;
using System.Text;
using TW.MyBlog.Infrastructure.Utility.BaseEnums;

namespace TW.Utility
{

    public partial class APITipResult
    {
        public int code { get; set; }
        public string message { get; set; }
        public dynamic data { get; set; }

        public void AddMessage(Enum_ApiResultCode iCode, string sMessage = null)
        {
            code = iCode.GetValueToInt();
            message = sMessage ?? iCode.GetLocalizedDescription();
        }
    }
    public static class ApiTipResultExtensions
    {
        public static APITipResult RunWithTry(this APITipResult jsonResultEntry, Action<APITipResult> runMethod)
        {
            try
            {
                runMethod(jsonResultEntry);
            }
            catch (CustomException cex)
            {
                jsonResultEntry.code = cex.code;
                jsonResultEntry.message = cex.message;
            }
            catch (Exception e)
            {
                jsonResultEntry.code = Enum_ApiResultCode.OtherErr.GetValueToInt();
                jsonResultEntry.message = e.Message;
            }
            return jsonResultEntry;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Numerics;
using System.Text;
using TW.Utility.StackExchangeRedis;
using ImageSharp;
using ImageSharp.Drawing.Brushes;
using ImageSharp.Drawing.Pens;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SixLabors.Fonts;
using SixLabors.Primitives;

namespace TW.Utility
{
    public class Verification
    {
        private static char[] chars = "23456789abcdefghjkmnpqrstuvwxyz".ToCharArray();

        public static byte[] IdentifyingCode(string ttfpath,out string validateKey)
        {
            var code = GetRandomNumberString(4);
            var nowcode = Guid.NewGuid();
            validateKey = "ValidateKey" + nowcode;
            new DataCache().SetCache(validateKey, code, new TimeSpan(DateTime.Now.AddMinutes(5).Ticks));
            Random random = new Random();
            //颜色列表，用于验证码、噪线、噪点 
            Rgba32[] color = { Rgba32.Black, Rgba32.Red, Rgba32.DarkBlue, Rgba32.Green, Rgba32.Orange, Rgba32.Brown, Rgba32.DarkCyan, Rgba32.Purple };
            //创建画布
            using (var image = new Image<Rgba32>(90, 34))
            {
                using (var output = new MemoryStream())
                {
                    //将背景涂白
                    image.Fill(new SolidBrush<Rgba32>(Rgba32.White), new RectangleF(0, 0, image.Width, image.Height), new GraphicsOptions());
                    //向画板中绘制贝塞尔样条  
                    for (int i = 0; i < 7; i++)
                    {
                        var p1 = new Vector2(0, random.Next(image.Height));
                        var p2 = new Vector2(random.Next(image.Width), random.Next(image.Height));
                        var p3 = new Vector2(random.Next(image.Width), random.Next(image.Height));
                        var p4 = new Vector2(image.Width, random.Next(image.Height));
                        PointF[] p = { p1, p2, p3, p4 };
                        Rgba32 clr = color[random.Next(color.Length)];
                        Pen<Rgba32> pen = new Pen<Rgba32>(clr, 1);
                        image.DrawBeziers(pen, p);
                    }
                    //画噪点
                    for (int i = 0; i < 80; i++)
                    {
                        GraphicsOptions noneDefault = new GraphicsOptions();
                        RectangleF rectangle = new RectangleF(random.Next(image.Width), random.Next(image.Height), 1, 1);
                        Rgba32 clr = color[random.Next(color.Length)];
                        image.Draw(new Pen<Rgba32>(clr, 1), rectangle, noneDefault);
                    }
                    //画验证码字符串 
                    var fontCollection = new FontCollection();
                    var fontTemple = fontCollection.Install(ttfpath);
                    var font = new Font(fontTemple, 30);
                    int cindex = random.Next(3);//随机颜色-深色为主
                    for (int i = 0; i < code.Length; i++)
                    {
                        image.DrawText(code.Substring(i, 1), font, color[cindex], new PointF(3 + (i * 16), -10));//绘制一个验证字符  
                    }
                    image.Save(output, ImageFormats.Jpeg);
                    return output.ToArray();
                }
            }
        }
        // 生成随机数字字符串
        public static string GetRandomNumberString(int int_NumberLength)
        {
            Random random = new Random(Guid.NewGuid().GetHashCode());
            string validateCode = string.Empty;
            for (int i = 0; i < int_NumberLength; i++)
                validateCode += chars[random.Next(0, chars.Length)].ToString();
            return validateCode;
        }
    }
}

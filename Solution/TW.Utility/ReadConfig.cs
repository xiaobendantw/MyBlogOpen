﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace TW.Utility
{
    public class ReadConfig
    {
        /// <summary>
        /// 读取配置文件中的节点值
        /// </summary>
        /// <param name="appName"></param>
        /// <returns></returns>
        public static string ReadAppSetting(string appName)
        {
            string result = string.Empty;
            try
            {
                result = ConfigurationManager.AppSettings[appName];
            }
            catch (Exception e)
            {
                LoggerHelper.Log("读取配置文件失败:" + e.Message);
            }
            return result;
        }
    }
}

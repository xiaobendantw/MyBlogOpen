﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using TW.MyBlog.Infrastructure.Utility.BaseEnums;
using Microsoft.AspNetCore.Razor.Language;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using TW.Utility.CustomerAttribute;

namespace TW.Utility.Encrypt
{
    public class MD5Helper
    {
        #region 加密
        public static string GetMd5(string myString)
        {
            #region md5加密
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] data = Encoding.UTF8.GetBytes(myString);
            byte[] md5data = md5.ComputeHash(data);
            md5.Clear();
            StringBuilder sBuilder = new StringBuilder();
            for (int i = 0; i < md5data.Length; i++)
            {
                sBuilder.Append(md5data[i].ToString("X2"));
            }
            return sBuilder.ToString();
            #endregion
        }

        /// <summary>
        /// MD5加密
        /// </summary>
        public static String GetMd5Hash(string str)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(System.Text.Encoding.UTF8.GetBytes(str));
            StringBuilder strReturn = new StringBuilder();

            for (int i = 0; i < result.Length; i++)
            {
                strReturn.Append(Convert.ToString(result[i], 16).PadLeft(2, '0'));
            }
            return strReturn.ToString().PadLeft(32, '0');
        }
        #endregion
    }
}

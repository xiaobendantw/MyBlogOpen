﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TW.Utility
{
    public class IPHelper
    {
        public static string getIPAddr()
        {
            string user_IP = string.Empty;
            try
            {
                user_IP = HttpContext.Current.Request.Headers["X-Forwarded-For"].FirstOrDefault();
                if (string.IsNullOrEmpty(user_IP))
                {
                    user_IP = HttpContext.Current.Connection.RemoteIpAddress.ToString();
                }
            }
            catch
            {
                user_IP = "";

            }
            return user_IP;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TW.Utility
{
    public class CustomException : Exception
    {
        public CustomException() : base()
        {
        }

        public CustomException(string message) : base(message)
        {
        }

        public CustomException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public CustomException(int code, string message)
            : base()
        {
            this.code = code;
            this.message = message;
        }
        public int code { set; get; }

        public string message { set; get; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace TW.Utility
{
    public class SelectListItemHelper
    {
        public static SelectListItem _def = new SelectListItem() { Text = "-全部-", Value = "" };
        public static dynamic GetListByT(IEnumerable<SelectListItem> list, bool shoowEmpty)
        {
            var result = new List<SelectListItem>();
            if (shoowEmpty)
            {
                result.Add(_def);
            }
            if (list != null && list.Any())
            {
                list.ToList().ForEach(x =>
                {
                    result.Add(new SelectListItem() { Text = x.Text, Value = x.Value, Selected = x.Selected });
                });
            }
            return result;
        }

        public static string LoadBitOption(bool? val = null, bool IsEmpty = false)
        {
            return (IsEmpty ? "<option value=\"\">--全部--</option>" : "") +
                   "<option  value=\"true\">是</option><option " + (val != null ? val.Value ? "selected=\"selected\"" : "" : "") + " value=\"false\">否</option>";
        }
    }
}

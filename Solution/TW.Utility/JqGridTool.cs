﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using TW.Utility.CustomerAttribute;

namespace TW.Utility
{
    public class JqGridOpt
    {
        public string Title { get; set; }
        public string OptMainFun { get; set; }
        public int OptWidth { get; set; }
    }
}

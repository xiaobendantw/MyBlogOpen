﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TW.Utility.BaseEnums
{
    public enum Enum_DeviceType
    {
        IOS = 0,
        Android = 1,
        SMS = 2
    }
}

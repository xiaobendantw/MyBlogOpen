﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TW.Utility.BaseEnums
{
    public enum Enum_MessageCycle
    {
        None = 0,
        Day = 1,
        Week = 2,
        Month = 3,
        Quarter = 4,
        Year = 5
    }
}

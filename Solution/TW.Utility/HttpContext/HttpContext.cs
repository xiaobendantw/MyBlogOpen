﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace TW.Utility
{
    public static class HttpContext
    {
        private static IHttpContextAccessor _accessor;

        public static Microsoft.AspNetCore.Http.HttpContext Current => _accessor.HttpContext;

        public static List<string> OrginDomain => ReadConfig.ReadAppSetting("CorsDomain").Split(";").ToList();

        internal static void Configure(IHttpContextAccessor accessor)
        {
            _accessor = accessor;
        }

        public static bool IsAjaxRequest(this HttpRequest request)
        {
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (request.Headers["X-Requested-With"] == "XMLHttpRequest")
                return true;
            return false;
        }

        public static bool IsOriginRequest(this HttpRequest request)
        {
            LoggerHelper.Log(JsonConvert.SerializeObject(OrginDomain.Contains(request.Headers["Origin"].ToString())));
            if (request == null)
                throw new ArgumentNullException(nameof(request));
            if (OrginDomain.Contains(request.Headers["Origin"]))
                return true;
            return false;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TW.Utility.CustomerAttribute
{
    public class JqGridColumnOpt : Attribute
    {
        /// <summary>
        /// 宽度
        /// </summary>
        public int Width { get; set; }
        /// <summary>
        /// 位置
        /// </summary>
        public JqGridAlign Align { get; set; }
        /// <summary>
        /// 排序字段
        /// </summary>
        public string SortName { get; set; }
        /// <summary>
        /// 是否排序
        /// </summary>
        public bool IsSort { get; set; }
        /// <summary>
        /// 转换js方法名
        /// </summary>
        public string RenderFun { get; set; }
        /// <summary>
        /// 是否隐藏
        /// </summary>
        public bool IsHidden { get; set; }

        public JqGridColumnOpt(int width = 0, JqGridAlign align = JqGridAlign.left, bool isSort = true, string sortName = "", string renderFun = "", bool isHidden = false)
        {
            Width = width;
            Align = align;
            IsSort = isSort;
            SortName = sortName;
            RenderFun = renderFun;
            IsHidden = isHidden;
        }
    }

    public enum JqGridAlign
    {
        left = 0,
        center = 1,
        right = 2
    }
}

﻿using System;
using System.Collections.Generic;
using System.DrawingCore;
using System.DrawingCore.Imaging;
using System.IO;
using System.Text;

namespace TW.Utility.ImageUpload
{
    public class ImageServer_Access : UploadAccess
    {
        private static object _lock = new object();
        public ImageServer_Access()
            : base()
        {

        }
        /// <summary>
        /// 商城图片上传功能类
        /// </summary>
        /// <param name="iLimitFileSize"></param>
        /// <param name="limitFileType"></param>
        /// <param name="sSavePath"></param>
        /// <param name="file"></param>
        /// <param name="iWidth"></param>
        /// <param name="iHeight"></param>
        /// <param name="sCutMode"></param>
        public ImageServer_Access(int iLimitFileSize, List<string> limitFileType, string sSavePath, Bitmap file, int iWidth
            , int iHeight, string sCutMode, string name)
            : base(iLimitFileSize, limitFileType, sSavePath, file, name)
        {
            this._sCutMode = sCutMode;
            this._iWidth = iWidth;
            this._iHeight = iHeight;
        }

        /// <summary>
        /// 默认裁剪缩略图
        /// </summary>
        private string _sCutMode = "W";
        /// <summary>
        /// 图片是否是轮播图
        /// </summary>
        public string CutMode
        {
            get
            {
                return _sCutMode;
            }
        }
        private int _iWidth = 0;
        /// <summary>
        /// 保存图片的最大宽度
        /// </summary>
        public int Width
        {
            get { return this._iWidth; }
        }

        private int _iHeight = 0;
        /// <summary>
        /// 保存图片的最大高度
        /// </summary>
        public int Height
        {
            get { return this._iHeight; }
        }
        /// <summary>
        /// 保存文件
        /// </summary>
        /// <param name="sFilePath"></param>
        /// <returns></returns>
        public override int SaveFile(ref string sFilePath)
        {
            int iResult = 0;
            try
            {
                if (PostedFile != null && SavePath != string.Empty)
                {//客户端传来的参数都不为空
                    if (base.CheckUpload(ref iResult))
                    {
                        string sExtension = System.IO.Path.GetExtension(filename);//后缀名
                        string sFileName = DateTime.Now.ToString("ddHHmmssfff") + sExtension;//组装大图文件名
                        var ms = new MemoryStream();
                        PostedFile.Save(ms, ImageFormat.Bmp);
                        Stream imageStream = ms;
                        Image originalImage = Image.FromStream(imageStream);
                        if (CutMode == "W")
                        {
                            Thumb.MakeThumbnail_Mall(originalImage, SavePath + sFileName, Width, Height, "W");//切割原图
                        }
                        else if (CutMode == "H")
                        {
                            Thumb.MakeThumbnail_Mall(originalImage, SavePath + sFileName, Width, Height, "H");//切割原图
                        }
                        else if (CutMode == "HW")
                        {
                            Thumb.MakeThumbnail_Mall(originalImage, SavePath + sFileName, Width, Height, "HW");//切割原图
                        }
                        else if (CutMode == "Cut")
                        {
                            Thumb.MakeThumbnail_Mall(originalImage, SavePath + sFileName, Width, Height, "Cut");//切割原图
                        }
                        sFilePath = sFileName;
                        iResult = 1;//成功
                        ms.Close();
                        originalImage.Dispose();
                        imageStream.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                LoggerHelper.Log("上传失败：" + ex.ToString());
            }
            return iResult;
        }
    }
}

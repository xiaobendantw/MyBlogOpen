﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TW.Utility.Encrypt;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;

namespace TW.Utility.ImageUpload
{
    public class ProxyImageServices
    {
        public static readonly string ImageUploadUrl = ReadConfig.ReadAppSetting("ImageUploadUrl");
        public static readonly string ImageUploadKey = ReadConfig.ReadAppSetting("ImageUploadKey");
        public static FileResult UploadImage(IFormFile file, string Dic, CuteMode CuteMode, int? w = 0, int? h = 0)
        {
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                var inputStream = reader.BaseStream;
                byte[] fileBytes = new byte[inputStream.Length];
                inputStream.Read(fileBytes, 0, fileBytes.Length);
                inputStream.Seek(0, SeekOrigin.Begin);
                var name = file.FileName;
                var now = DateTime.Now;
                var md5 = MD5Helper.GetMd5(ImageUploadKey + name + now + ImageUploadKey);
                var dic = new Dictionary<string, object>
                {
                    {"sign", md5},
                    {"name", name},
                    {"Path", Dic},
                    {"UploadFile", HttpUtility.UrlEncode(Convert.ToBase64String(fileBytes))},
                    {"Width", w},
                    {"Height", h},
                    {"CutMode", CuteMode},
                    {"date", now}
                };
                string postData = string.Join("&", dic.Select(x => x.Key + "=" + x.Value));
                var webclient = new WebClient();
                webclient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                byte[] buffer = webclient.UploadData(ImageUploadUrl + "/Image/Upload", "POST", byteArray);
                var msg = Encoding.UTF8.GetString(buffer);
                return JsonConvert.DeserializeObject<FileResult>(msg);
            }
        }
        public static FileResult UploadFile(IFormFile file, string Dic)
        {
            using (var reader = new StreamReader(file.OpenReadStream()))
            {
                var inputStream = reader.BaseStream;
                byte[] fileBytes = new byte[inputStream.Length];
                inputStream.Read(fileBytes, 0, fileBytes.Length);
                inputStream.Seek(0, SeekOrigin.Begin);
                var name = file.FileName;
                var now = DateTime.Now;
                var md5 = MD5Helper.GetMd5(ImageUploadKey + name + now + ImageUploadKey);
                var dic = new Dictionary<string, object>
                {
                    {"sign", md5},
                    {"name", name},
                    {"Path", Dic},
                    {"UploadFile", HttpUtility.UrlEncode(Convert.ToBase64String(fileBytes))},
                    {"date", now}
                };
                string postData = string.Join("&", dic.Select(x => x.Key + "=" + x.Value));
                var webclient = new WebClient();
                webclient.Headers.Add("Content-Type", "application/x-www-form-urlencoded");
                byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                byte[] buffer = webclient.UploadData(ImageUploadUrl + "/File/Upload", "POST", byteArray);
                var msg = Encoding.UTF8.GetString(buffer);
                return JsonConvert.DeserializeObject<FileResult>(msg);
            }
            //return msg;
        }
        public enum CuteMode
        {
            /// <summary>
            /// 不裁剪
            /// </summary>
            NoCut = 0,

            /// <summary>
            /// 按照宽度缩放
            /// </summary>
            W = 1,

            /// <summary>
            /// 按照高度缩放
            /// </summary>
            H = 2,

            /// <summary>
            /// 按照宽高缩放(可能变形)
            /// </summary>
            HW = 3,
            /// <summary>
            /// 按照宽高缩放(不变形)
            /// </summary>
            Cut = 4
        }
    }
    public class FileResult
    {
        public bool success { get; set; }
        public string filename { get; set; }
    }
}

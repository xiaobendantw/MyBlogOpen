﻿using System;
using System.Collections.Generic;
using System.DrawingCore;
using System.DrawingCore.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace TW.Utility.ImageUpload
{
    /// <summary>
    /// 文件上传父类
    /// </summary>
    public class UploadAccess
    {
        /// <summary>
        /// 允许上传的文件最大大小
        /// </summary>
        private readonly int _iMaxSize = TConvert.toInt(ReadConfig.ReadAppSetting("MaxSize"));

        /// <summary>
        /// 禁止上传的文件类型
        /// </summary>
        private readonly List<string> _sForbbidenExs = ReadConfig.ReadAppSetting("ForbbidenExs").Split(';').ToList();


        /// <summary>
        /// 限制上传的文件大小 KB
        /// </summary>
        private int _iLimitFileSize = 0;
        /// <summary>
        /// 限制上传的文件大小 KB
        /// </summary>
        public int LimitFileSize
        {
            get
            {
                return _iLimitFileSize;
            }
            set
            {
                this._iLimitFileSize = value;
            }
        }


        /// <summary>
        /// 限制上传的文件类型 
        /// </summary>
        private List<string> _limitFileType;
        /// <summary>
        /// 限制上传的文件类型 
        /// </summary>
        public List<string> LimitFileType
        {
            get
            {
                return _limitFileType;
            }
            set
            {
                this._limitFileType = value;
            }
        }

        /// <summary>
        /// 文件存放路径
        /// </summary>
        private string _sSavePath = string.Empty;
        /// <summary>
        /// 文件存放路径
        /// </summary>
        public string SavePath
        {
            get
            {
                return _sSavePath;
            }
            set
            {
                this._sSavePath = value;
            }
        }

        /// <summary>
        /// 上传的文件
        /// </summary>
        private Bitmap _postedFile;
        /// <summary>
        /// 上传的文件
        /// </summary>
        public Bitmap PostedFile
        {
            get
            {
                return _postedFile;
            }
            set
            {
                this._postedFile = value;
            }
        }

        /// <summary>
        /// 后缀名
        /// </summary>
        private string _sExtension = string.Empty;
        /// <summary>
        /// 后缀名
        /// </summary>
        public string Extension
        {
            get
            {
                return this._sExtension;
            }
            set
            {
                this._sExtension = value;
            }
        }

        /// <summary>
        /// 上传的流文件
        /// </summary>
        private Stream _streamFile;
        /// <summary>
        /// 上传的流文件
        /// </summary>
        public Stream StreamFile
        {
            get
            {
                return this._streamFile;
            }
            set
            {
                this._streamFile = value;
            }
        }

        /// <summary>
        /// 文件长度
        /// </summary>
        public long FileLength
        {
            get
            {
                if (PostedFile != null)
                {
                    var ms = new MemoryStream();
                    PostedFile.Save(ms, ImageFormat.Bmp);
                    byte[] bytes = ms.GetBuffer();
                    ms.Close();
                    return bytes.Length;
                }
                else if (StreamFile != null)
                {
                    return StreamFile.Length;
                }
                else
                {
                    return 0;
                }
            }
        }
        /// <summary>
        /// 保存文件方法
        /// </summary>
        public virtual int SaveFile(ref string filePath)
        {
            return 0;
        }


        /// <summary>
        /// 验证上传文件
        /// </summary>
        /// <param name="iResult"></param>
        /// <returns></returns>
        public virtual bool CheckUpload(ref int iResult)
        {
            bool bResult = false;
            if (LimitFileSize > 0 && ((LimitFileSize * 1024) < this.FileLength ||
                                      (_iMaxSize > 0 && (_iMaxSize * 1024) < this.FileLength)))
            {//文件超出限制
                iResult = -1;
                var ms = new MemoryStream();
                PostedFile.Save(ms, ImageFormat.Bmp);
                byte[] bytes = ms.GetBuffer();
                ms.Close();
                LoggerHelper.Log("文件超出大小限制，该文件大小为：" + bytes.Length / 1024 + " KB");
                return bResult;
            }
            if ((LimitFileType != null && LimitFileType.Count > 0 &&
                 LimitFileType.Where(m => TString.Compare2String(m, "*" + _sExtension)).Count() <= 0) ||
                _sForbbidenExs.Where(m => TString.Compare2String(m, "*" + _sExtension)).Count() > 0)
            {//上传的文件后缀名格式错误
                iResult = -2;
                LoggerHelper.Log("文件为禁止上传类型，该文件后缀为：" + _sExtension);
                return bResult;
            }
            bResult = true;
            return bResult;
        }
        public UploadAccess()
        {

        }
        public UploadAccess(int iLimitFileSize, List<string> limitFileType, string sSavePath)
        {
            this._iLimitFileSize = iLimitFileSize;
            this._limitFileType = limitFileType;
            if (sSavePath != string.Empty)
            {
                if (sSavePath.Last() != '/')
                {
                    this._sSavePath = sSavePath + "/";
                }
                else
                {
                    this._sSavePath = sSavePath;
                }
            }
        }
        public string filename { get; set; }
        public UploadAccess(int iLimitFileSize, List<string> limitFileType, string sSavePath, Bitmap file, string name)
        {
            this._iLimitFileSize = iLimitFileSize;
            this._limitFileType = limitFileType;
            if (sSavePath != string.Empty)
            {
                if (sSavePath.Last() != '\\')
                {
                    this._sSavePath = sSavePath + @"\";
                }
                else
                {
                    this._sSavePath = sSavePath;
                }
            }
            this._postedFile = file;
            if (file != null)
            {
                this._sExtension = System.IO.Path.GetExtension(name);
            }
            filename = name;
        }

        public UploadAccess(int iLimitFileSize, List<string> limitFileType, string sSavePath, Stream file, string sExtension)
        {
            this._iLimitFileSize = iLimitFileSize;
            this._limitFileType = limitFileType;
            if (sSavePath != string.Empty)
            {
                if (sSavePath.Last() != '/')
                {
                    this._sSavePath = sSavePath + "/";
                }
                else
                {
                    this._sSavePath = sSavePath;
                }
            }
            this.StreamFile = file;
            this._sExtension = sExtension;
        }
    }
}

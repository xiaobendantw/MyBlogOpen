﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using TW.MyBlog.Infrastructure.Utility.BaseEnums;

namespace TW.Utility
{
    public partial class TipResult
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public string RedirectUrl { get; set; }
        public object Data { get; set; }
        /// <summary>
        /// 添加错误信息
        /// </summary>
        /// <param name="message"></param>
        public void AddErrorMessage(string message)
        {
            Message = message;
            Success = false;
        }
        /// <summary>
        /// 添加异常信息
        /// </summary>
        /// <param name="e">The e.</param>
        /// <returns></returns>
        public void AddException(Exception e)
        {
            AddErrorMessage(e.Message);
        }
    }
    public static class TipResultExtensions
    {
        public static TipResult RunWithTry(this TipResult jsonResultEntry, Action<TipResult> runMethod)
        {
            try
            {
                runMethod(jsonResultEntry);
            }
            catch (DbUpdateConcurrencyException uex)//事务异常
            {
                jsonResultEntry.AddErrorMessage(uex.InnerException.Message);
                LoggerHelper.Log(uex.Message);
            }
            catch (DbUpdateException dbEx)//模型更新异常
            {
                jsonResultEntry.AddErrorMessage(dbEx.GetBaseException().Message);
                LoggerHelper.Log(dbEx.Message);
            }
            catch (CustomException e)
            {
                jsonResultEntry.AddException(e);
                if (e.InnerException != null)
                {
                    jsonResultEntry.AddException(e.InnerException.InnerException);
                }
                else
                {
                }
            }
            catch (Exception e)
            {
                jsonResultEntry.AddException(new Exception("出错了,请稍后再试!"));
                LoggerHelper.Log(e.GetBaseException().Message);
            }
            return jsonResultEntry;
        }
        public static async Task<TipResult> RunWithAsyncTry(this TipResult jsonResultEntry, Func<TipResult, Task> runMethod)
        {
            try
            {
                await runMethod(jsonResultEntry);
            }
            catch (DbUpdateConcurrencyException uex)//事务异常
            {
                jsonResultEntry.AddErrorMessage(uex.InnerException.Message);
                LoggerHelper.Log(uex.Message);
            }
            catch (DbUpdateException dbEx)//模型更新异常
            {
                jsonResultEntry.AddErrorMessage(dbEx.GetBaseException().Message);
                LoggerHelper.Log(dbEx.Message);
            }
            catch (CustomException e)
            {
                jsonResultEntry.AddException(e);
                if (e.InnerException != null)
                {
                    jsonResultEntry.AddException(e.InnerException.InnerException);
                }
                else
                {
                }
            }
            catch (Exception e)
            {
                jsonResultEntry.AddException(new Exception("出错了,请稍后再试!"));
                LoggerHelper.Log(e.GetBaseException().Message);
            }
            return jsonResultEntry;
        }
    }
}

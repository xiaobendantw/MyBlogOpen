﻿using System;

namespace TW.Utility.Extensions
{
    public static class DateTimeExtensions
    {
        public static string ToDisplayTime(this DateTime source)
        {
            string reult;
            var value = DateTime.Now.Subtract(source).TotalSeconds;
            if (value < 60 * 5)
                reult = "刚刚";
            else if (value / 60 < 60)
                reult = Math.Round(value / 60) + "分钟前";
            else if (value / 60 / 60 < 24)
                reult = Math.Round(value / 60 / 60) + "小时前";
            else
                reult = source.ToString("yyyy-MM-dd");
            return reult;
        }
        public static string ToDisplayTime2(this DateTime source)
        {
            string reult;
            var value = DateTime.Now.Subtract(source).TotalSeconds;
            if (value < 60 * 5)
                reult = "刚刚";
            else if (value / 60 < 60)
                reult = Math.Round(value / 60) + "分钟前";
            else if (value / 60 / 60 < 24)
                reult = Math.Round(value / 60 / 60) + "小时前";
            else
                reult = source.ToString("yyyy-MM-dd HH:mm");
            return reult;
        }
        public static string ToHourTime(this DateTime source)
        {
            string reult;
            if (source.Hour == 0)
                reult = source.ToString("yyyy-MM-dd");
            else
                reult = source.ToString("yyyy-MM-dd HH:00");
            return reult;
        }
        public static string ToChsTime(this DateTime? source)
        {
            string reult;
            if (source == DateTime.MinValue || string.IsNullOrEmpty(source.ToString()))
                reult = "";
            else
                reult = source?.ToString("yyyy-MM-dd");
            return reult;
        }





        // DateTime时间格式转换为Unix时间戳格式 
        public static int ConvertDateTimeInt(this DateTime time)
        {
            DateTime startTime;
            startTime = TimeZoneInfo.ConvertTime(new DateTime(1970, 1, 1),TimeZoneInfo.Local);
            return (int)(time - startTime).TotalSeconds;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TW.Utility.Extensions
{
    public static class CollectionsExtensions
    {
        public static void FetchNext<T>(this IEnumerable<T> resource, int num, Action<IEnumerable<T>> action)
        {
            if (resource.Any())
            {
                var len = resource.Count() < num
                    ? 1
                    : resource.Count() % num != 0
                        ? resource.Count() / num + 1
                        : resource.Count() / num;
                for (var i = 0; i < len; i++)
                {
                    var result = resource.Skip(i * 50).Take(num);
                    action.Invoke(result);
                }
            }
        }
    }
}

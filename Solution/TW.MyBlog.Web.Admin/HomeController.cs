﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using TW.Utility;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TW.MyBlog.Web.Admin
{
    public class HomeController : Controller
    {

        // GET: /<controller>/
        public IActionResult Index()
        {
            return RedirectToAction("Login", "Home", new { area = "Admin" });
        }
        /// <summary>
        /// 捕获全局异常
        /// </summary>
        /// <returns></returns>
        public IActionResult Error()
        {
            try
            {
                var feature = HttpContext.Features.Get<IExceptionHandlerFeature>();
                if (feature != null)
                {
                    LoggerHelper.Log("异常:" + feature.Error?.GetBaseException()?.Message);
                }
            }
            catch (Exception e)
            {
                LoggerHelper.Log("日志写入异常:" + e.Message);
            }
            return Content("出错了,请稍后再试");
        }

        //只有debugger模式先才开启接口调试
        private static readonly string ConfigPrivatekey = ReadConfig.ReadAppSetting("MD5_KEY");
        // GET: /<controller>/
        public IActionResult ApiTest()
        {
            return View(@"\Views\Test\Index.cshtml");
        }
        [HttpPost]
        public IActionResult ApiTestSubmit(string url, string parmer)
        {
            try
            {
                TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
                var timespan = Convert.ToInt64(ts.TotalSeconds).ToString();
                JObject json = JObject.Parse(parmer);
                var parameters = new Dictionary<string, dynamic>();
                parameters.Add("timespan", timespan);
                foreach (var jItem in json)
                {
                    parameters.Add(jItem.Key, jItem.Value);
                }
                var sign = DetectionParamHelper.GetMd5Parm(parameters, ConfigPrivatekey, true);
                json.Add("sign", sign);
                json.Add("timespan", timespan);
                var client = new WebClient();
                var postData = Encoding.UTF8.GetBytes(JsonConvert.SerializeObject(json));
                var responseData = client.UploadData(url, "post", postData);
                client.Dispose();
                var str = Encoding.UTF8.GetString(responseData);
                return Content(str);
            }
            catch (WebException ex)
            {
                Stream stream = ex.Response.GetResponseStream();
                StreamReader reader = new StreamReader(stream);
                string text = reader.ReadToEnd();
                return Content("程序错误,请手动调试,异常信息：" + text);
            }
            catch (Exception e)
            {
                return Content("程序错误,请手动调试,异常信息：" + e.Message);
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.Utility;
using TW.Utility.ImageUpload;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Npoi.Core.SS.Formula.Functions;
using UEditorNetCore;

namespace TW.MyBlog.Web.Admin.Areas.Tool.Controllers
{
    [Route("api/[controller]")] //配置路由(UEditorNetCore需要以webapi的方式进行工作)
    public class UEditorController : Controller
    {
        private readonly UEditorService _ue;
        public UEditorController(UEditorService ue)
        {
            this._ue = ue;
        }

        public void Do()
        {
            _ue.DoAction(HttpContext);
        }
    }
}
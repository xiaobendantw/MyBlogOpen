﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.Utility;
using TW.Utility.ImageUpload;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace TW.MyBlog.Web.Admin.Areas.Tool.Controllers
{
    [Area("Tool")]
    public class UploadController : Controller
    {
        [HttpPost]
        public IActionResult SingleUpload(IFormCollection collection, int width = 0, int height = 0, string dir = "Admin", bool isFile = false)
        {
            TipResult result = new TipResult();
            result.RunWithTry(x =>
            {
                if (!collection.Files.Any())
                {
                    throw new CustomException("请选择一个文件!");
                }
                var file = collection.Files.FirstOrDefault();
                var cutModel = ProxyImageServices.CuteMode.NoCut;
                if (!isFile)
                {
                    if (width != 0 && height == 0)
                    {
                        cutModel = ProxyImageServices.CuteMode.W;
                    }
                    else if (width != 0 && height != 0)
                    {
                        cutModel = ProxyImageServices.CuteMode.Cut;
                    }
                    else if (width == 0 && height != 0)
                    {
                        cutModel = ProxyImageServices.CuteMode.H;
                    }
                    var proxyResult = ProxyImageServices.UploadImage(file, dir, cutModel, width, height);
                    if (proxyResult.success)
                    {
                        //throw new  Exception("上传失败");
                        x.Success = true;
                        x.Data = proxyResult.filename;
                    }
                    else
                    {
                        x.Success = false;
                        x.Message = "上传失败,请重试!";
                        x.Data = "";
                    }
                }
                else
                {
                    var proxyResult = ProxyImageServices.UploadFile(file, dir);
                    if (proxyResult.success)
                    {
                        //throw new  Exception("上传失败");
                        x.Success = true;
                        x.Data = proxyResult.filename;
                    }
                    else
                    {
                        x.Success = false;
                        x.Message = string.IsNullOrEmpty(proxyResult.filename) ? "上传失败,请重试！" : proxyResult.filename;
                        x.Data = "";
                    }
                }
            });
            return Json(result);
        }
        [HttpPost]
        public IActionResult SingleMoreCutUpload(IFormCollection collection, string imageSizes, string dir = "Admin")
        {
            TipResult result = new TipResult();
            var resultdata = new List<TempcontainerOutput>();
            result.RunWithTry(x =>
            {
                if (!collection.Files.Any())
                {
                    throw new CustomException("请选择一个文件!");
                }
                var file = collection.Files.FirstOrDefault();
                var cutModel = ProxyImageServices.CuteMode.NoCut;
                int cussess = 0;
                var size = JsonConvert.DeserializeObject<List<TempcontainerInput>>(imageSizes);
                size.ForEach(y =>
                {
                    if (y.width != 0 && y.height == 0)
                    {
                        cutModel = ProxyImageServices.CuteMode.W;
                    }
                    else if (y.width != 0 && y.height != 0)
                    {
                        cutModel = ProxyImageServices.CuteMode.Cut;
                    }
                    else if (y.width == 0 && y.height != 0)
                    {
                        cutModel = ProxyImageServices.CuteMode.H;
                    }
                    var proxyResult = ProxyImageServices.UploadImage(file, dir, cutModel, y.width, y.height);
                    if (proxyResult.success)
                    {
                        cussess++;
                        var obj = new TempcontainerOutput();
                        obj.containerId = y.containerId;
                        obj.filename = proxyResult.filename;
                        resultdata.Add(obj);
                    }
                });
                if (cussess == size.Count)
                {
                    x.Success = true;
                    x.Data = resultdata;
                }
                else
                {
                    x.Success = false;
                    x.Message = "上传失败,请重试!";
                    x.Data = "";
                }
            });
            return Json(result);
        }
    }

    internal class TempcontainerInput
    {
        public string containerId { get; set; }
        public int width { get; set; }
        public int height { get; set; }
    }
    internal class TempcontainerOutput
    {
        public string containerId { get; set; }
        public string filename { get; set; }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Web.Admin.Areas.Admin.Lib.Authorization;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace TW.MyBlog.Web.Admin.Areas.Admin.Lib
{
    public static class LocalContainerFactory
    {
        public static void RegLocalServices(this IServiceCollection services)
        {
            services.AddTransient<ILocalCommon, LocalCommon>()
                .AddTransient<IAuthorizationHandler, AdminRoleHandler>();
        }
    }
}

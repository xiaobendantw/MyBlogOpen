﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Newtonsoft.Json;
using TW.Utility;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Mvc.Filters;

namespace TW.MyBlog.Web.Areas.Admin.Attrbute
{
    //通过ValidateAntiForgery机制实现简易的幂等处理
    public class FormHandleAttribute: ActionFilterAttribute
    {
        public static List<FormToken> Tokens;
        public static string TokenInput = "__RequestVerificationToken";

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            if (Tokens == null)
            {
                Tokens = new List<FormToken>();
            }

            if (filterContext.HttpContext.Request.Method == "POST" && !string.IsNullOrEmpty(HttpContext.Current.Request.Form[TokenInput])
            )
            {
                var token = Tokens.FirstOrDefault(x => x.Key == filterContext.HttpContext.Request.Form[TokenInput]);
                if (token != null)
                {
                    if (token.Disabled)
                    {
                        filterContext.Result = new JsonResult(new TipResult() {Success = false, Message = "请不要重复提交表单"});
                    }
                }
                else
                {
                    Tokens.Add(new FormToken() {Key = filterContext.HttpContext.Request.Form[TokenInput], Disabled = false});
                }
            }
        }

        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            base.OnResultExecuted(filterContext);
            if (filterContext.HttpContext.Request.Method == "POST" && !string.IsNullOrEmpty(filterContext.HttpContext.Request.Form[TokenInput])
                )
            {
                var token = Tokens.FirstOrDefault(x => x.Key == filterContext.HttpContext.Request.Form[TokenInput]);
                if (token != null)
                {
                    if (filterContext.Result.GetType() == typeof (JsonResult))
                    {
                        try
                        {
                            var resultStr = JsonConvert.SerializeObject(((JsonResult) (filterContext.Result)).Value);
                            if (!string.IsNullOrEmpty(resultStr))
                            {
                                var resultJson = JsonConvert.DeserializeObject<TipResult>(resultStr);
                                if (resultJson.Success)
                                {
                                    token.Disabled = true;
                                }
                            }
                        }
                        catch (Exception)
                        {
                            // ignored
                        }
                    }
                }
            }
        }
    }

    public class FormToken
    {
        public string Key { get; set; }
        public bool Disabled { get; set; }
    }
}
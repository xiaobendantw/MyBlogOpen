﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper.Configuration.Conventions;

namespace TW.MyBlog.Web.Areas.Admin.Attrbute
{
    public class SubAuth : Attribute
    {
        public List<string> FatherName { get; set; }
        public SubAuth(params string[] fatherName)
        {
            FatherName = fatherName.ToList();
        }
    }
}
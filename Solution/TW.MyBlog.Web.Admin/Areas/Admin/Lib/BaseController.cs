﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model.DTO.Manager;
using TW.MyBlog.Web.Admin.Areas.Admin.Lib;
using TW.Utility;
using TW.Utility.Encrypt;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using Npoi.Core.SS.Formula.Functions;

namespace TW.MyBlog.Web.Admin.Lib
{
    [Authorize("Admin")]
    [Area("Admin")]
    public class BaseController : Controller
    {
        private readonly ILocalCommon _localCommon;

        public BaseController(ILocalCommon localCommon)
        {
           _localCommon = localCommon;
        }

        public dynamic GetRowToGrid<T>(EntityList<T> data) where T : new()
        {
            return NpoiHelper.GetRowToGrid(data);
        }

        public byte[] ReturnFile<T>(string excelName, List<string> titles, List<T> sOutput)
        {
            return new NpoiHelper(Response).ReturnFile(excelName, titles, sOutput);
        }

        public LoginUserDTO CurrentUser => _localCommon.CurrentFullUser();

        public string GetColumsName<T>(params JqGridOpt[] opt)
        {
            return _localCommon.GetColumsName<T>(opt);
        }
    }
}
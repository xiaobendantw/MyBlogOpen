﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc.Filters;

namespace TW.MyBlog.Web.Admin.Areas.Admin.Lib.Authorization
{
    public class AdminAuthorizationRequirement : IAuthorizationRequirement
    {
        public bool ChkRole { get; set; }
        public string FatherModule { get; set; }
        public AdminAuthorizationRequirement(bool chkRole = true, string fatherModule = "")
        {
            ChkRole = chkRole;
            FatherModule = fatherModule;
        }
    }
}

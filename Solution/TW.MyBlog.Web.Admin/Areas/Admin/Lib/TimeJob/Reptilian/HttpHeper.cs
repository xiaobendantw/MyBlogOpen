﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.IdentityModel.Logging;
using Newtonsoft.Json;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.EFWork;

namespace TW.MyBlog.Web.Admin.Areas.Admin.Lib.TimeJob.Reptilian
{
    public class HttpHeper
    {
        public static int count = 0;
        public static int page = 0;
        public static void WriteList()
        {
            Writelog("抓取服务启动...");
            using (var db = new Entities())
            {
                var tempath = Directory.GetCurrentDirectory() + @"\Reptilian\temputre\temp.txt";//模板文件路径
                var savepath = Directory.GetCurrentDirectory() + @"\Reptilian\SuccesReuslut\";//数据存放路径
                using (var file = new FileStream($"{tempath} ", FileMode.Open))
                {
                    using (var sw = new StreamReader(file, Encoding.GetEncoding("utf-8")))
                    {
                        while (sw.Peek() >= 0)
                        {
                            try
                            {
                                var obj = JsonConvert.DeserializeObject<Word>(sw.ReadLine());
                                ModifyContent(savepath, obj.url, obj.name);
                                Console.WriteLine("抓取页数" + page);
                            }
                            catch (Exception e)
                            {
                                Writelog(string.IsNullOrEmpty(e.Message) ? e.InnerException?.Message : e.Message);
                            }

                            //Console.WriteLine(JsonConvert.DeserializeObject<Word>(sw.ReadLine()).url);
                        }

                    }
                }
            }

        }

        /// <summary>
        /// 处理Http返回数据
        /// </summary>
        /// <param name="url"></param>
        /// <param name="name"></param>
        /// <param name="isdigui"></param>
        public static void ModifyContent(string savepath, string url, string name = "", bool isdigui = true)
        {
            try
            {
                var reptilianlist = new List<MyBlog_Reptilian_Info>();
                var result = HttpHeper.HttpGet(url);//获取数据
                var contntmachs = Regex.Matches(result, "<a href=\".*\" title=\"查看本句\" class=\"xlistju\">.*</div>");//匹内容段
                foreach (var x in contntmachs)//读取内容
                {
                    var source = Regex.Match(x.ToString(), "<div class=\"xqjulistwafo\">.*?</div>").Value;//获取出处
                    var fieryDegreemach = Regex.Match(x.ToString(), @"喜欢\(.*?\)").Value.Replace("喜欢(", "").Replace(")", "");//获取火热度
                    var content = Regex.Match(x.ToString(), "<a href=\".*\" title=\"查看本句\" class=\"xlistju\">.*?</a>").Value.Replace("<br/>", "\n");//匹文字内容
                    var id = Regex.Match(x.ToString(), "href=\".*?\"").Value.Replace("href=\"/ju/", "").Replace("\"", "");
                    var obj = new MyBlog_Reptilian_Info()
                    {
                        Content = GetContentSummary(content, content.Length),
                        Source = GetContentSummary(source, source.Length),
                        ReptilianId = Convert.ToInt32(id),
                        GrabTime = DateTime.Now,
                        CategoryName = name,
                        FieryDegree = Convert.ToInt32(GetContentSummary(fieryDegreemach, fieryDegreemach.Length)),
                        ID = Guid.NewGuid()
                    };
                    reptilianlist.Add(obj);
                    WriteToFile(savepath, obj.Content + obj.Source, name);
                    //var contentmath = Regex.Match(x.ToString(), "class=\"xlistju\">.*?</a>").Groups[0].Value.Replace("class=\"xlistju\">", " ").Replace("</a>", " ").Replace("<br/>", "\n").Trim();

                }
                WriteToDB(reptilianlist);
                var math = Regex.Match(result, "<li class=\"pager-last\"><a href=\".*\" title=\".*\" class=\"active\">.*?</a></li>");
                var str = math.Groups[0].Value;
                var lastcount = string.IsNullOrEmpty(str) ? 0 : Convert.ToInt32(Regex.Match(str, "class=\"active\">.*?</a>").Groups[0].Value.Replace("class=\"active\">", " ").Replace("</a>", " ").Trim());//获取最大页数
                if (lastcount > 0 && isdigui)
                {
                    for (int i = 0; i < lastcount; i++)
                    {
                        ModifyContent(savepath, $"{url}?page={i + 1}", name, false);//递归抓取
                        page = i;
                    }
                }

            }
            catch (Exception e)
            {
                Writelog(string.IsNullOrEmpty(e.Message) ? e.InnerException?.Message : e.Message);
            }
        }
        /// <summary>
        /// 写入文件
        /// </summary>
        /// <param name="savepath"></param>
        /// <param name="content"></param>
        /// <param name="name"></param>
        public static void WriteToFile(string savepath, string content, string name)
        {
            using (var file = new FileStream($"{savepath}{name}.txt", FileMode.Append))
            {
                using (var sw = new StreamWriter(file, Encoding.GetEncoding("gb2312")))
                {
                    sw.WriteLine($@"{content}");
                    sw.WriteLine("");
                    sw.Flush();
                    count++;
                }
            }
        }
        /// <summary>
        /// 写入数据库
        /// </summary>
        /// <param name="reptilianInfo"></param>
        public static void WriteToDB(List<MyBlog_Reptilian_Info> reptilianInfo)
        {
            using (var db = new Entities())
            {
                db.MyBlog_Reptilian_Info.AddRange(reptilianInfo);
                db.SaveChanges();
            }
        }
        /// <summary>
        /// HttpPost
        /// </summary>
        /// <param name="url"></param>
        /// <param name="paramData"></param>
        /// <param name="headerDic"></param>
        /// <returns></returns>
        public static string HttpPost(string url, string paramData, Dictionary<string, string> headerDic = null)
        {
            string result = string.Empty;
            try
            {
                HttpWebRequest wbRequest = (HttpWebRequest)WebRequest.Create(url);
                var userAgentList = new List<string>()
                {
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/14.0.835.163 Safari/535.1",//Chrome
                    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:6.0) Gecko/20100101 Firefox/6.0",//Firefox
                    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.50 (KHTML, like Gecko) Version/5.1 Safari/534.50",
                    "Opera/9.80 (Windows NT 6.1; U; zh-cn) Presto/2.9.168 Version/11.50",
                    "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Win64; x64; Trident/5.0; .NET CLR 2.0.50727; SLCC2; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; Tablet PC 2.0; .NET4.0E)",
                    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; WOW64; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; .NET4.0C; InfoPath.3)",
                    "Mozilla/5.0 (Windows; U; Windows NT 6.1; ) AppleWebKit/534.12 (KHTML, like Gecko) Maxthon/3.0 Safari/534.12",
                    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E; SE 2.X MetaSr 1.0)",
                    "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0; InfoPath.3; .NET4.0C; .NET4.0E)",
                    "Mozilla/5.0 (Windows NT 6.1) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.41 Safari/535.1 QQBrowser/6.9.11079.201",
                    "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; WOW64; Trident/5.0)"

                };
                wbRequest.Headers.Add("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)");
                wbRequest.Method = "POST";
                wbRequest.ContentType = "application/x-www-form-urlencoded";
                wbRequest.ContentLength = Encoding.UTF8.GetByteCount(paramData);
                if (headerDic != null && headerDic.Count > 0)
                {
                    foreach (var item in headerDic)
                    {
                        wbRequest.Headers.Add(item.Key, item.Value);
                    }
                }
                using (Stream requestStream = wbRequest.GetRequestStream())
                {
                    using (StreamWriter swrite = new StreamWriter(requestStream))
                    {
                        swrite.Write(paramData);
                    }
                }
                HttpWebResponse wbResponse = (HttpWebResponse)wbRequest.GetResponse();
                using (Stream responseStream = wbResponse.GetResponseStream())
                {
                    using (StreamReader sread = new StreamReader(responseStream))
                    {
                        result = sread.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            { }

            return result;
        }
        /// <summary>
        /// HttpGet
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string HttpGet(string url)
        {
            string result = string.Empty;
            var li = new int[6];
            try
            {
                HttpWebRequest wbRequest = (HttpWebRequest)WebRequest.Create(url);
                var userAgentList = new List<string>()//组装UserAgent,用户随机选择,防止主机IP被抓取网站禁
                {
                    "Mozilla/5.0(Macintosh;U;IntelMacOSX10_6_8;en-us)AppleWebKit/534.50(KHTML,likeGecko)Version/5.1Safari/534.50",//Chrome
                    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.2; .NET CLR 1.0.3705;)",
                    "Mozilla/5.0(Windows;U;WindowsNT6.1;en-us)AppleWebKit/534.50(KHTML,likeGecko)Version/5.1Safari/534.50",
                    "Mozilla/5.0(compatible;MSIE9.0;WindowsNT6.1;Trident/5.0",
                    "Mozilla/4.0(compatible;MSIE8.0;WindowsNT6.0;Trident/4.0",
                    "Mozilla/4.0(compatible;MSIE7.0;WindowsNT6.0)",
                    "Mozilla/4.0(compatible;MSIE6.0;WindowsNT5.1)",
                    "Mozilla/5.0(Macintosh;IntelMacOSX10.6;rv:2.0.1)Gecko/20100101Firefox/4.0.1",
                    "Mozilla/5.0(WindowsNT6.1;rv:2.0.1)Gecko/20100101Firefox/4.0.1",
                    "Opera/9.80(Macintosh;IntelMacOSX10.6.8;U;en)Presto/2.8.131Version/11.11",
                    "Opera/9.80(WindowsNT6.1;U;en)Presto/2.8.131Version/11.11",
                    "Mozilla/5.0(Macintosh;IntelMacOSX10_7_0)AppleWebKit/535.11(KHTML,likeGecko)Chrome/17.0.963.56Safari/535.11",
                    "Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;Maxthon2.0)",
                    "Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;TencentTraveler4.0)",
                    "Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1)",
                    "Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;TheWorld)",
                    "Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;Trident/4.0;SE2.XMetaSr1.0;SE2.XMetaSr1.0;.NETCLR2.0.50727;SE2.XMetaSr1.0)",
                    "Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;360SE)",
                    "Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1;AvantBrowser)",
                    "Mozilla/4.0(compatible;MSIE7.0;WindowsNT5.1)",


                };
                var index = new Random().Next(20);
                wbRequest.UserAgent = userAgentList[index];
                wbRequest.Method = "GET";
                HttpWebResponse wbResponse = (HttpWebResponse)wbRequest.GetResponse();
                using (Stream responseStream = wbResponse.GetResponseStream())
                {
                    using (StreamReader sReader = new StreamReader(responseStream))
                    {
                        result = sReader.ReadToEnd();
                    }
                }
            }
            catch
            {

            }
            return result;
        }
        /// <summary>
        /// 提取摘要，是否清除HTML代码 (完美版)
        /// </summary>
        /// <param name="content"></param>
        /// <param name="length"></param>
        /// <param name="stripHtml"></param>
        /// <returns></returns>
        public static string GetContentSummary(string content, int length, bool stripHtml = true)
        {
            if (string.IsNullOrEmpty(content) || length == 0)
                return "";
            if (stripHtml)
            {
                content = content.Replace("（全文）", "");
                //content = content.Replace("\r\n", "");
                content = Regex.Replace(content, @"<script.*?</script>", "", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"<style.*?</style>", "", RegexOptions.IgnoreCase);
                //content = Regex.Replace(content, @"<.+>", "", RegexOptions.IgnoreCase);
                //删除HTML
                content = Regex.Replace(content, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
                //content = Regex.Replace(content, @"([\r\n])|([\s])+", "", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"-->", "", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"<!--.*", "", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"&(nbsp|#160);", "", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"&(iexcl|#161);", "\xa1", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"&(cent|#162);", "\xa2", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"&(pound|#163);", "\xa3", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"&(copy|#169);", "\xa9", RegexOptions.IgnoreCase);
                content = Regex.Replace(content, @"&#(\d+);", "", RegexOptions.IgnoreCase);
                content = content.Replace("<", "&lt;");
                content = content.Replace(">", "&gt;");
                if (content.Length <= length)
                    return content;
                else
                    return content.Substring(0, length) + "……";
            }
            else
            {
                if (content.Length <= length)
                    return content;

                int pos = 0, npos = 0, size = 0;
                bool firststop = false, notr = false, noli = false;
                StringBuilder sb = new StringBuilder();
                while (true)
                {
                    if (pos >= content.Length)
                        break;
                    string cur = content.Substring(pos, 1);
                    if (cur == "<")
                    {
                        string next = content.Substring(pos + 1, 3).ToLower();
                        if (next.IndexOf("p") == 0 && next.IndexOf("pre") != 0)
                        {
                            npos = content.IndexOf(">", pos) + 1;
                        }
                        else if (next.IndexOf("/p") == 0 && next.IndexOf("/pr") != 0)
                        {
                            npos = content.IndexOf(">", pos) + 1;
                            if (size < length)
                                sb.Append("<br/>");
                        }
                        else if (next.IndexOf("br") == 0)
                        {
                            npos = content.IndexOf(">", pos) + 1;
                            if (size < length)
                                sb.Append("<br/>");
                        }
                        else if (next.IndexOf("img") == 0)
                        {
                            npos = content.IndexOf(">", pos) + 1;
                            if (size < length)
                            {
                                sb.Append(content.Substring(pos, npos - pos));
                                size += npos - pos + 1;
                            }
                        }
                        else if (next.IndexOf("li") == 0 || next.IndexOf("/li") == 0)
                        {
                            npos = content.IndexOf(">", pos) + 1;
                            if (size < length)
                            {
                                sb.Append(content.Substring(pos, npos - pos));
                            }
                            else
                            {
                                if (!noli && next.IndexOf("/li") == 0)
                                {
                                    sb.Append(content.Substring(pos, npos - pos));
                                    noli = true;
                                }
                            }
                        }
                        else if (next.IndexOf("tr") == 0 || next.IndexOf("/tr") == 0)
                        {
                            npos = content.IndexOf(">", pos) + 1;
                            if (size < length)
                            {
                                sb.Append(content.Substring(pos, npos - pos));
                            }
                            else
                            {
                                if (!notr && next.IndexOf("/tr") == 0)
                                {
                                    sb.Append(content.Substring(pos, npos - pos));
                                    notr = true;
                                }
                            }
                        }
                        else if (next.IndexOf("td") == 0 || next.IndexOf("/td") == 0)
                        {
                            npos = content.IndexOf(">", pos) + 1;
                            if (size < length)
                            {
                                sb.Append(content.Substring(pos, npos - pos));
                            }
                            else
                            {
                                if (!notr)
                                {
                                    sb.Append(content.Substring(pos, npos - pos));
                                }
                            }
                        }
                        else
                        {
                            npos = content.IndexOf(">", pos) + 1;
                            sb.Append(content.Substring(pos, npos - pos));
                        }
                        if (npos <= pos)
                            npos = pos + 1;
                        pos = npos;
                    }
                    else
                    {
                        if (size < length)
                        {
                            sb.Append(cur);
                            size++;
                        }
                        else
                        {
                            if (!firststop)
                            {
                                sb.Append("……");
                                firststop = true;
                            }
                        }
                        pos++;
                    }

                }
                return sb.ToString();
            }
        }
        #region 日志记录

        private const string DirectPath = "Log/ReptilianJobLog";

        static void Writelog(string msg)
        {
            var dt = DateTime.Now;
            DirectoryInfo di = new DirectoryInfo(Directory.GetCurrentDirectory() + "/" + DirectPath);
            if (di.Exists == false) di.Create();
            DirectoryInfo disub = new DirectoryInfo(di.ToString() + "/" + dt.ToString("yyyy-MM"));
            if (disub.Exists == false) disub.Create();
            var FilePath = disub.ToString() + "/" + dt.ToString("yyyy-MM-dd") + ".txt";
            if (!File.Exists(FilePath))
            {
                using (var fs = File.Create(FilePath))
                {
                    fs.Flush();
                }
            }
            using (
                var filestream = new System.IO.FileStream(FilePath, System.IO.FileMode.Append,
                    System.IO.FileAccess.Write, FileShare.ReadWrite))
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using (var sw = new StreamWriter(filestream, Encoding.GetEncoding("gb2312")))
                {
                    sw.WriteLine("[" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "]");
                    sw.WriteLine(msg);
                    sw.WriteLine("");
                    sw.Flush();
                }
            }
        }
        #endregion
    }

    public class Word
    {
        public string url { get; set; }
        public string name { get; set; }
    }
}

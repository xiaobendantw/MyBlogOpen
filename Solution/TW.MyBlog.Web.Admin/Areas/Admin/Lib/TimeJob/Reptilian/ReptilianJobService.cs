﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pomelo.AspNetCore.TimedJob;

namespace TW.MyBlog.Web.Admin.Areas.Admin.Lib.TimeJob.Reptilian
{
    public class ReptilianJobService : Job
    {
        public const int _interval = 24*60*60*1000; //程序循环时间(秒为单位)
        public const string _begin = "2018-05-07 16:15:00"; //程序开始时间

        /// <summary>
        /// 定义执行参数
        /// SkipWhileExecuting 是否等待上一个执行完成
        /// </summary>
        [Invoke(Begin = _begin, Interval = _interval, SkipWhileExecuting = true)]
        public void Main()
        {
            //处理程序
            HttpHeper.WriteList();
        }
    }

}

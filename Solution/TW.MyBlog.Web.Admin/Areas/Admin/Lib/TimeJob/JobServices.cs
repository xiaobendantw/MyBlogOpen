﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Pomelo.AspNetCore.TimedJob;

namespace TW.MyBlog.Web.Admin.Areas.Admin.Lib.TimeJob
{
    /// <summary>
    /// 定时任务类
    /// </summary>
    public class JobServices:Job
    {
        public const int _interval = 1000 * 15;//程序循环时间(秒为单位)
        public const  string _begin = "2018-02-09";//程序开始时间

        /// <summary>
        /// 定义执行参数
        /// SkipWhileExecuting 是否等待上一个执行完成
        /// </summary>
        [Invoke(Begin = _begin, Interval = _interval, SkipWhileExecuting = true)]
        public void Main()
        {
            //处理程序
        }
        #region 日志记录
        private const string DirectPath = "Log/TimeJobLog";
        static void Writelog(string msg)
        {
            var dt = DateTime.Now;
            DirectoryInfo di = new DirectoryInfo(Directory.GetCurrentDirectory() + "/" + DirectPath);
            if (di.Exists == false) di.Create();
            DirectoryInfo disub = new DirectoryInfo(di.ToString() + "/" + dt.ToString("yyyy-MM"));
            if (disub.Exists == false) disub.Create();
            var FilePath = disub.ToString() + "/" + dt.ToString("yyyy-MM-dd") + ".txt";
            if (!File.Exists(FilePath))
            {
                using (var fs = File.Create(FilePath))
                {
                    fs.Flush();
                }
            }
            using (
                var filestream = new System.IO.FileStream(FilePath, System.IO.FileMode.Append,
                    System.IO.FileAccess.Write, FileShare.ReadWrite))
            {
                Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
                using (var sw = new StreamWriter(filestream, Encoding.GetEncoding("gb2312")))
                {
                    sw.WriteLine("[" + dt.ToString("yyyy-MM-dd HH:mm:ss") + "]");
                    sw.WriteLine(msg);
                    sw.WriteLine("");
                    sw.Flush();
                }
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Web.Admin.Areas.Admin.Lib;
using TW.MyBlog.Web.Admin.Lib;
using TW.MyBlog.Web.Areas.Admin.Attrbute;
using Microsoft.AspNetCore.Mvc;
using TW.MyBlog.Infrastructure.Common.Enums;

namespace TW.MyBlog.Web.Areas.Admin.Controllers
{
    public class ChartController : BaseController
    {
        // GET: Admin/Chart
        private readonly IMyBlog_Manager_LogServices _logServices;
        public ChartController(
            IMyBlog_Manager_LogServices logServices, ILocalCommon localCommon
            ):base(localCommon)
        {
            _logServices = logServices;
        }
        #region 系统日志统计
        public IActionResult ManagerLogChart()
        {
            dynamic obj = new ExpandoObject();
            obj.Url = "/Admin/Chart/ManagerLogCount";
            obj.Title = "系统日志统计";
            obj.ChartsType = Enum_ChartsType.line;
            return View("~/Areas/Admin/Views/Chart/Chart.cshtml", obj);
        }
        [SubAuth(nameof(ManagerLogChart))]
        public async Task<IActionResult> ManagerLogCount(DateTime? searchStartCreateTime, DateTime? searchEndCreateTime)
        {
            var result = await _logServices.ManagerLogChart(searchStartCreateTime, searchEndCreateTime);
            return Json(result);
        }

        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Web.Admin.Areas.Admin.Lib;
using TW.MyBlog.Web.Admin.Lib;
using TW.MyBlog.Web.Areas.Admin.Attrbute;
using TW.Utility;

namespace TW.MyBlog.Web.Admin.Areas.Admin.Controllers
{
    /// <summary>
    /// 相册相关
    /// </summary>
    [FormHandle]
    public class AlbumController : BaseController
    {
        private readonly IMyBlog_Album_AlbumCategoryServices _albumcategoryServices;
        private readonly ILocalCommon _localCommon;
        public AlbumController(ILocalCommon localCommon, IMyBlog_Album_AlbumCategoryServices albumcategoryServices) : base(localCommon)
        {
            _localCommon = localCommon;
            _albumcategoryServices = albumcategoryServices;
        }

        #region AlbumCategory管理
        public IActionResult AlbumCategory()
        {
            ViewBag.ColumsTitle = _localCommon.GetColumsName<AlbumAlbumCategoryDTO>();
            ViewBag.DataUrl = "/Admin/Album/GetAlbumCategoryList";
            return View();
        }
        public IActionResult AddAlbumCategory()
        {
            return View("AlbumCategoryDetail", new MyBlog_Album_AlbumCategory());
        }

        public async Task<IActionResult> EditAlbumCategory(Guid Id)
        {
            return View("AlbumCategoryDetail", await _albumcategoryServices.GetByKeyAsync(Id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [SubAuth(nameof(AddAlbumCategory), nameof(EditAlbumCategory))]
        public async Task<IActionResult> SaveAlbumCategory(MyBlog_Album_AlbumCategory model)
        {
            var oldid = model.ID;
            TipResult result = new TipResult();
            await result.RunWithAsyncTry(async x =>
            {
                x.Success = true;
                await _albumcategoryServices.ModifyModel(model);
                x.Message = "" + (oldid == Guid.Empty ? "添加" : "编辑") + "成功!";
            });
            return Json(result);
        }

        [SubAuth(nameof(AlbumCategory), nameof(ExportAlbumCategory))]
        public async Task<IActionResult> GetAlbumCategoryList(int page, int rows, string sidx, string sord, string exportTitles, bool export = false)
        {
            var sort = !string.IsNullOrEmpty(sidx) ? new Dictionary<string, string>() { { sidx, sord.ToUpper() } } : new Dictionary<string, string>() { { "CreateTime", "DESC" } };
            if (!export)
            {
                return Json(GetRowToGrid((await _albumcategoryServices.GetAlbumCategoryList(page, rows, sort, export))));
            }
            else
            {
                var fileName = "导出AlbumCategory管理";
                var titles = exportTitles.Split(',').ToList();
                var record = (await _albumcategoryServices.GetAlbumCategoryList(page, rows, sort, export)).Data;
                return File(ReturnFile(fileName, titles, record), "application/vnd.ms-excel");
            }
        }
        public async Task<IActionResult> DelAlbumCategory(string ids)
        {
            TipResult result = new TipResult();
            await result.RunWithAsyncTry(async x =>
            {
                if (ids.Split(',').Any())
                {
                    var delId = ids.Split(',').Select(Guid.Parse);
                    await _albumcategoryServices.DelModelsById(delId);
                    x.Success = true;
                    x.Message = "删除成功!";
                }
            });
            return Json(result);
        }
        public IActionResult ExportAlbumCategory()
        {
            ViewBag.ColumsTitle = _localCommon.GetExportColumsName<AlbumAlbumCategoryDTO>();
            ViewBag.SeaechCondition = Request.QueryString.Value?.ToString();
            ViewBag.DataUrl = "/Admin/Album/GetAlbumCategoryList";
            return View("~/Areas/Admin/Views/Home/ExportRecord.cshtml");
        }
        #endregion
    }
}
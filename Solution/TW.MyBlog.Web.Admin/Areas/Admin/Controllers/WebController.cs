﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Web.Admin.Areas.Admin.Lib;
using TW.MyBlog.Web.Admin.Lib;
using TW.MyBlog.Web.Areas.Admin.Attrbute;
using TW.Utility;

namespace TW.MyBlog.Web.Admin.Areas.Admin.Controllers
{
    /// <summary>
    /// 网站设置相关
    /// </summary>
    [FormHandle]
    public class WebController : BaseController
    {
        private readonly IMyBlog_Web_WebSetingServices _websetingServices;
        private readonly ILocalCommon _localCommon;
        public WebController(ILocalCommon localCommon, IMyBlog_Web_WebSetingServices websetingServices) : base(localCommon)
        {
            _websetingServices = websetingServices;
            _localCommon = localCommon;
        }
        #region WebSeting管理
        public async Task<IActionResult> EditWebSeting(Guid Id)
        {
            return View("WebSetingDetail", await _websetingServices.GetByConditionAsync(new DirectSpecification<MyBlog_Web_WebSeting>(x=>!x.IsDeleted)));
        }

        [HttpPost]
        [SubAuth(nameof(EditWebSeting))]
        public async Task<IActionResult> SaveWebSeting(MyBlog_Web_WebSeting model)
        {
            var oldid = model.ID;
            TipResult result = new TipResult();
            await result.RunWithAsyncTry(async x =>
            {
                x.Success = true;
                await _websetingServices.ModifyModel(model);
                x.Message = "" + (oldid == Guid.Empty ? "添加" : "编辑") + "成功!";
            });
            return Json(result);
        }

       
        public async Task<ActionResult> GetWebSetingList(int page, int rows, string sidx, string sord, string exportTitles, bool export = false)
        {
            var sort = !string.IsNullOrEmpty(sidx) ? new Dictionary<string, string>() { { sidx, sord.ToUpper() } } : new Dictionary<string, string>() { { "CreateTime", "DESC" } };
            if (!export)
            {
                return Json(GetRowToGrid((await _websetingServices.GetWebSetingList(page, rows, sort, export))));
            }
            else
            {
                var fileName = "导出WebSeting管理";
                var titles = exportTitles.Split(',').ToList();
                var record = (await _websetingServices.GetWebSetingList(page, rows, sort, export)).Data;
                return File(ReturnFile(fileName, titles, record), "application/vnd.ms-excel");
            }
        }
        public async Task<ActionResult> DelWebSeting(string ids)
        {
            TipResult result = new TipResult();
            await result.RunWithAsyncTry(async x =>
            {
                if (ids.Split(',').Any())
                {
                    var delId = ids.Split(',').Select(Guid.Parse);
                    await _websetingServices.DelModelsById(delId);
                    x.Success = true;
                    x.Message = "删除成功!";
                }
            });
            return Json(result);
        }
        public ActionResult ExportWebSeting()
        {
            //ViewBag.ColumsTitle = _localCommon.GetExportColumsName<WebWebSetingDTO>();
            ViewBag.SeaechCondition = Request.QueryString.Value?.ToString();
            ViewBag.DataUrl = "/Admin/Web/GetWebSetingList";
            return View("~/Areas/Admin/Views/Home/ExportRecord.cshtml");
        }
        #endregion

    }
}
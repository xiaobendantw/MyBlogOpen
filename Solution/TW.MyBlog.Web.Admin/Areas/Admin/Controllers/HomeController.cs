﻿using TW.MyBlog.Application.Service.CacheService;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Infrastructure.Utility.BaseEnums;
using TW.MyBlog.Web.Admin.Areas.Admin.Lib;
using TW.MyBlog.Web.Admin.Lib;
using TW.MyBlog.Web.Areas.Admin.Attrbute;
using TW.Utility;
using TW.Utility.ImageUpload;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System;
using System.Dynamic;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Infrastructure.Common.Enums;

namespace TW.MyBlog.Web.Areas.Admin.Controllers
{
    //
    public class HomeController : BaseController
    {

        private readonly IMyBlog_Manager_BaseInfoServices _baseInfoServices;
        private readonly IMyBlog_SYS_ModuleServices _moduleServices;
        private readonly IMyBlog_SYS_ModuleActionServices _moduleActionServices;
        private readonly IMyBlog_SYS_RoleServices _roleServices;
        private readonly IMyBlog_SYS_RoleActionServices _roleActionServices;
        private readonly IMyBlog_SYS_BaseCacheService _testCacheService;
        private readonly ILocalCommon _localCommon;
        private readonly IMyBlog_Manager_LogServices _logServices;
        private readonly IMyBlog_Web_IPVisitServices _blogWebIpVisitServices;

        public HomeController(
            IMyBlog_Manager_BaseInfoServices baseInfoServices,
            IMyBlog_SYS_ModuleServices moduleServices,
            IMyBlog_SYS_ModuleActionServices moduleActionServices,
            IMyBlog_SYS_RoleServices roleServices,
            IMyBlog_SYS_RoleActionServices roleActionServices,
            IMyBlog_SYS_BaseCacheService testCacheService, ILocalCommon localCommon, IMyBlog_Manager_LogServices logServices, IMyBlog_Web_IPVisitServices blogWebIpVisitServices) : base(localCommon)
        {
            _baseInfoServices = baseInfoServices;
            _moduleServices = moduleServices;
            _moduleActionServices = moduleActionServices;
            _roleServices = roleServices;
            _roleActionServices = roleActionServices;
            _testCacheService = testCacheService;
            _localCommon = localCommon;
            _logServices = logServices;
            _blogWebIpVisitServices = blogWebIpVisitServices;
        }

        // GET: Admin/Home

        [AllowAnonymous]
        public IActionResult GetEnum_ApiResultCode(Enum_ApiResultCode? code = null, bool isEmpty = false)
        {
            return Json(EnumHelper.EnumTypeGetListItem<Enum_ApiResultCode>(code?.GetValueToInt().ToString(), isEmpty));
        }

        [AllowAnonymous]
        public async Task<IActionResult> Login()
        {
            try
            {
                var user = CurrentUser;
                if (user.ID != Guid.Empty)
                {
                    await _baseInfoServices.CheckLogin(CurrentUser.LoginName, CurrentUser.PassWord, false, true);
                    return RedirectToAction("Index");
                }
            }
            catch (Exception)
            {
                await Common.LoginOut();
            }
            return View();
        }

        [AllowAnonymous]
        [HttpPost]

        [FormHandle]
        public async Task<IActionResult> CheckLogin(string remeberMe)
        {
            var result = new TipResult();
            await result.RunWithAsyncTry(async x =>
            {
                string sLoginName = Request.Form["sUserName"];
                Regex clearSpecialSymbol = new Regex("[?*=]");
                string sPassWord = clearSpecialSymbol.Replace(Request.Form["sPassword"], string.Empty);
                var rememberMe = remeberMe != null;
                var ValidateCode = Request.Form["ValidateCode"];
                await _baseInfoServices.CheckLogin(sLoginName, sPassWord, rememberMe, false, ValidateCode);
                x.Message = "登录成功!";
                x.Success = true;
            });
            return Json(result);
        }

        [AllowAnonymous]
        public async Task<IActionResult> LoginOut(string msg, bool goOut = true)
        {
            TempData["ErrMsg"] = msg;
            if (goOut)
            {
                await Common.LoginOut();
                return RedirectToAction("Login");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]

        [FormHandle]
        public async Task<IActionResult> ChangeUserInfo(string nickName, string newPassword, string confirmPassword,
            int userChangeType, string oldpassword)
        {
            var result = new TipResult();
            await result.RunWithAsyncTry(async x =>
            {
                await
                    _baseInfoServices.ChangeUserInfo(CurrentUser.ID, nickName, newPassword, confirmPassword,
                        userChangeType, oldpassword);
                x.Message = userChangeType == 1 ? "密码修改成功" : "基本信息修改成功!";
                x.Success = true;
            });
            return Json(result);
        }

        [AllowAnonymous]
        public IActionResult VerCode()
        {
            //由于跨平台的原因,需内置字体文件用于生成验证码
            var codeImg = Verification.IdentifyingCode($"{Directory.GetCurrentDirectory()}/wwwroot/font/calibri.ttf", out var validateKey);
            Response.Cookies.Append("ValidateKey", validateKey);
            return File(codeImg, "image/png");
        }


        public IActionResult Index()
        {
            return View();
        }
        [FormHandle]
        public IActionResult MyDesktop()
        {
            dynamic obj = new ExpandoObject();
            obj.HomeUrl = "/Admin/Home/WebVisitCount";
            obj.BlogUrl = "/Admin/Home/BlogClickCount";
            obj.Title = "系统日志统计";
            obj.ChartsType = Enum_ChartsType.line;
            return View(obj);
        }
        [SubAuth(nameof(MyDesktop))]
        public async Task<IActionResult> WebVisitCount(DateTime? searchStartCreateTime, DateTime? searchEndCreateTime)
        {
            var result = await _blogWebIpVisitServices.GetChartData(searchStartCreateTime, searchEndCreateTime, Enum_VisitType.Home);
            return Json(result);
        }
        [SubAuth(nameof(MyDesktop))]
        public async Task<IActionResult> BlogClickCount(DateTime? searchStartCreateTime, DateTime? searchEndCreateTime)
        {
            var result = await _blogWebIpVisitServices.GetChartData(searchStartCreateTime, searchEndCreateTime, Enum_VisitType.Blog);
            return Json(result);
        }
        [HttpPost]
        public IActionResult UploadTest(IFormFile file)
        {
            var FileItem = ProxyImageServices.UploadImage(file, "Admin", ProxyImageServices.CuteMode.W, 750, 0);
            return Content(ReadConfig.ReadAppSetting("ImageUploadUrl") + FileItem.filename);
        }

        public async Task<IActionResult> ChangeLoginInfo(int type)
        {
            if (type == 0)
            {
                return View("~/Areas/Admin/Views/Home/_ManagerInfo.cshtml",
                    (await _baseInfoServices.GetByKeyAsync(CurrentUser.ID)));
            }
            return View("~/Areas/Admin/Views/Home/_ManagerPwd.cshtml");
        }

        public async Task<IActionResult> GetLoginByAjax()
        {
            var result = new TipResult();
            await result.RunWithAsyncTry(async x =>
            {
                x.Success = true;
                var data = await _localCommon.CurrentFullUserAsync();
                var Menu = JsonConvert.DeserializeObject<dynamic>(data.Menu);
                x.Data = new { data.NickName, Menu };
            });
            return Json(result);
        }

        public IActionResult Test()
        {
            return View();
        }
    }
}
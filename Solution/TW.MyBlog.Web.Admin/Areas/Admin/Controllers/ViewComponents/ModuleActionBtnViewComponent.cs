﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Web.Admin.Areas.Admin.Lib;
using Microsoft.AspNetCore.Mvc;

namespace TW.MyBlog.Web.Admin.Areas.Admin
{
    public class ModuleActionBtnViewComponent : ViewComponent
    {
        private readonly ILocalCommon _localCommon;
        public ModuleActionBtnViewComponent(ILocalCommon localCommon)
        {
            _localCommon = localCommon;
        }
        public async Task<IViewComponentResult> InvokeAsync()
        {
            return View("/Areas/Admin/Views/Shared/ViewComponents/ModuleActionBtn/Default.cshtml", await _localCommon.CurrentFullUserAsync());
        }
    }
}

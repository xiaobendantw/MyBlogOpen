﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common;
using TW.MyBlog.Web.Admin.Areas.Admin.Lib;
using TW.MyBlog.Web.Admin.Lib;
using TW.MyBlog.Web.Areas.Admin.Attrbute;
using TW.Utility;

namespace TW.MyBlog.Web.Admin.Areas.Admin.Controllers
{
    /// <summary>
    /// 博文信息相关
    /// </summary>
    [FormHandle]
    public class PostController : BaseController
    {
        private readonly IMyBlog_Post_PostInfoServices _postinfoServices;
        private readonly IMyBlog_Post_PostCategoryServices _postcategoryServices;
        private readonly ILocalCommon _localCommon;
        public PostController(ILocalCommon localCommon, IMyBlog_Post_PostInfoServices postinfoServices, IMyBlog_Post_PostCategoryServices postcategoryServices) : base(localCommon)
        {
            _postinfoServices = postinfoServices;
            _localCommon = localCommon;
            _postcategoryServices = postcategoryServices;
        }

        #region Public
        public async Task<IActionResult> GetPostCategory(string value, bool isEmpty = false)
        {
            return Json(SelectListItemHelper.GetListByT(
                (await _postcategoryServices.GetManyAsync(
                    new DirectSpecification<MyBlog_Post_PostCategory>(x => !x.IsDeleted))).Select(x => new SelectListItem()
                    {
                        Text = x.CategoryNAme,
                        Value = x.ID.ToString(),
                        Selected = value != null && (value == x.ID.ToString())
                    }), isEmpty));
        }


        #endregion

        #region PostInfo管理
        public IActionResult PostInfo()
        {
            ViewBag.ColumsTitle = _localCommon.GetColumsName<PostPostInfoDTO>();
            ViewBag.DataUrl = "/Admin/Post/GetPostInfoList";
            return View();
        }
        public IActionResult AddPostInfo()
        {
            return View("PostInfoDetail", new MyBlog_Post_PostInfo());
        }

        public async Task<IActionResult> EditPostInfo(Guid Id)
        {
            return View("PostInfoDetail", await _postinfoServices.GetByKeyAsync(Id));
        }

        [HttpPost]
        [SubAuth(nameof(AddPostInfo), nameof(EditPostInfo))]
        public async Task<IActionResult> SavePostInfo(MyBlog_Post_PostInfo model)
        {
            var oldid = model.ID;
            TipResult result = new TipResult();
            await result.RunWithAsyncTry(async x =>
            {
                x.Success = true;
                await _postinfoServices.ModifyModel(model);
                x.Message = "" + (oldid == Guid.Empty ? "添加" : "编辑") + "成功!";
            });
            return Json(result);
        }

        [SubAuth(nameof(PostInfo), nameof(ExportPostInfo))]
        public async Task<IActionResult> GetPostInfoList(int page, int rows, string sidx, string sord, string exportTitles, bool export = false)
        {
            var sort = !string.IsNullOrEmpty(sidx) ? new Dictionary<string, string>() { { sidx, sord.ToUpper() } } : new Dictionary<string, string>() { { "CreateTime", "DESC" } };
            if (!export)
            {
                return Json(GetRowToGrid((await _postinfoServices.GetPostInfoList(page, rows, sort, export))));
            }
            else
            {
                var fileName = "导出PostInfo管理";
                var titles = exportTitles.Split(',').ToList();
                var record = (await _postinfoServices.GetPostInfoList(page, rows, sort, export)).Data;
                return File(ReturnFile(fileName, titles, record), "application/vnd.ms-excel");
            }
        }
        public async Task<IActionResult> DelPostInfo(string ids)
        {
            TipResult result = new TipResult();
            await result.RunWithAsyncTry(async x =>
            {
                if (ids.Split(',').Any())
                {
                    var delId = ids.Split(',').Select(Guid.Parse);
                    await _postinfoServices.DelModelsById(delId);
                    x.Success = true;
                    x.Message = "删除成功!";
                }
            });
            return Json(result);
        }
        public ActionResult ExportPostInfo()
        {
            //ViewBag.ColumsTitle = _localCommon.GetExportColumsName<PostPostInfoDTO>();
            ViewBag.SeaechCondition = Request.QueryString.Value?.ToString();
            ViewBag.DataUrl = "/Admin/Post/GetPostInfoList";
            return View("~/Areas/Admin/Views/Home/ExportRecord.cshtml");
        }
        #endregion

        #region PostCategory管理
        public IActionResult PostCategory()
        {
            ViewBag.ColumsTitle = _localCommon.GetColumsName<PostPostCategoryDTO>();
            ViewBag.DataUrl = "/Admin/Post/GetPostCategoryList";
            return View();
        }
        public IActionResult AddPostCategory()
        {
            return View("PostCategoryDetail", new MyBlog_Post_PostCategory());
        }

        public async Task<IActionResult> EditPostCategory(Guid Id)
        {
            return View("PostCategoryDetail", await _postcategoryServices.GetByKeyAsync(Id));
        }

        [HttpPost]
        [SubAuth(nameof(AddPostCategory), nameof(EditPostCategory))]
        public async Task<IActionResult> SavePostCategory(MyBlog_Post_PostCategory model)
        {
            var oldid = model.ID;
            TipResult result = new TipResult();
            await result.RunWithAsyncTry(async x =>
            {
                x.Success = true;
                await _postcategoryServices.ModifyModel(model);
                x.Message = "" + (oldid == Guid.Empty ? "添加" : "编辑") + "成功!";
            });
            return Json(result);
        }

        [SubAuth(nameof(PostCategory), nameof(ExportPostCategory))]
        public async Task<ActionResult> GetPostCategoryList(int page, int rows, string sidx, string sord, string exportTitles, bool export = false)
        {
            var sort = !string.IsNullOrEmpty(sidx) ? new Dictionary<string, string>() { { sidx, sord.ToUpper() } } : new Dictionary<string, string>() { { "CreateTime", "DESC" } };
            if (!export)
            {
                return Json(GetRowToGrid((await _postcategoryServices.GetPostCategoryList(page, rows, sort, export))));
            }
            else
            {
                var fileName = "导出PostCategory管理";
                var titles = exportTitles.Split(',').ToList();
                var record = (await _postcategoryServices.GetPostCategoryList(page, rows, sort, export)).Data;
                return File(ReturnFile(fileName, titles, record), "application/vnd.ms-excel");
            }
        }
        public async Task<ActionResult> DelPostCategory(string ids)
        {
            TipResult result = new TipResult();
            await result.RunWithAsyncTry(async x =>
            {
                if (ids.Split(',').Any())
                {
                    var delId = ids.Split(',').Select(Guid.Parse);
                    await _postcategoryServices.DelModelsById(delId);
                    x.Success = true;
                    x.Message = "删除成功!";
                }
            });
            return Json(result);
        }
        public ActionResult ExportPostCategory()
        {
            ViewBag.ColumsTitle = _localCommon.GetExportColumsName<PostPostCategoryDTO>();
            ViewBag.SeaechCondition = Request.QueryString.Value?.ToString();
            ViewBag.DataUrl = "/Admin/Post/GetPostCategoryList";
            return View("~/Areas/Admin/Views/Home/ExportRecord.cshtml");
        }
        #endregion
    }
}
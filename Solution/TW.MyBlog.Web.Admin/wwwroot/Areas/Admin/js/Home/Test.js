﻿$(function () {
    $("#PubDate").datepicker();
    $("#PubDateTime").datetimepicker();
    loadajaxSelect("#testdorp");
    loadUploadify("#inputfile", "#downloadurl");
    loadImageUpload("#upimage", ["#smallImgUrl", "#bigImgUrl"]);
    loadProvince('', '', '', true, ".detialprovinceId", ".detialcityId", ".detialdistrictId");
    setTimeout(loadetditor, 100);//getEditor必须在XX毫秒内加载，不然会出现文字过多后editor高度错误的问题
    //若是通过模态窗口打开getEditor，则需要在$(function(){})中加载以下方法在关闭模态窗口时销毁Editor避免第二次打开窗口Editor无法正常工作
    //$(window.parent.document).contents().find("#mainFrame")[0].contentWindow.delUE("myEditor");
    $("#openMsg").click(function () {
        TipMsg("这是一个窗口");
    });
    $("#openMsgCall").click(function () {
        TipMsg("这是一个可回调窗口", callback);
    });
    $("#openMsgCallParm").click(function () {
        TipMsg("这是一个可回调窗口", callbackp, 'aaa');
    });
    $("#openConfirmCall").click(function () {
        TipConfirm("这是一个确认窗口", callback);
    });
    $("#openConfirmCallParm").click(function () {
        TipConfirm("这是一个确认窗口", callbackp, 'aaa');
    });
    $("#GetEditor").click(function () {
        $("#Content").text(UE.getEditor('myEditor').getContent());
        alert(UE.getEditor('myEditor').getContent());
    });
});
function loadetditor() {
    UE.getEditor('myEditor');
}
function callback() {
    alert("我是无参数回调");
}
function callbackp(parm) {
    alert("我是带参数回调,参数是" + parm);
}
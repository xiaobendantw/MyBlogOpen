﻿
$(function() {
    //解决权限不足回跳首页产生的TempData["ErrMsg"]缓存问题
    if (history && history.pushState) {
        var guid = new Date();
        var tmpurl = "/Admin/Home/Index?date=" + guid.getMilliseconds();
        history.replaceState(null, document.title, tmpurl);
    }
    $(window).resize(changeFrameHeight);
});

function changeFrameHeight() {
    //点击菜单跳转时,关闭模态窗口
    var ifm = document.getElementById("mainFrame");
    var subWeb = document.frames ? document.frames["mainFrame"].document : ifm.contentDocument;
    if (ifm != null && subWeb != null) {
        ifm.height = $(window).height() - $("#navbar").height() - $("#breadcrumbs").height() - 10; // subWeb.body.scrollHeight;
    }
}
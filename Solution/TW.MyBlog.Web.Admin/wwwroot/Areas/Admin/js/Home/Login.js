﻿jQuery(function ($) {
    $(document).on('click', '.toolbar a[data-target]', function (e) {
        e.preventDefault();
        var target = $(this).data('target');
        $('.widget-box.visible').removeClass('visible'); //hide others
        $(target).addClass('visible'); //show target
    });
    $('#btn-login-dark').on('click', function (e) {
        $('body').attr('class', 'login-layout');
        $('#id-text2').attr('class', 'white');
        $('#id-company-text').attr('class', 'blue');

        e.preventDefault();
    });
    $('#btn-login-light').on('click', function (e) {
        $('body').attr('class', 'login-layout light-login');
        $('#id-text2').attr('class', 'grey');
        $('#id-company-text').attr('class', 'blue');

        e.preventDefault();
    });
    $('#btn-login-blur').on('click', function (e) {
        $('body').attr('class', 'login-layout blur-login');
        $('#id-text2').attr('class', 'white');
        $('#id-company-text').attr('class', 'light-blue');

        e.preventDefault();
    });
    $('#loginbtn').on('click', login);
    $("#ValidateCode").on("keydown", function (e) {
        if (e.keyCode == 13) {
            login();
        }
    })
    if (msg.length > 0) {
        bootbox.alert(msg);
    }
    $(document).bind("selectstart", function () { return false; });
    $("#VerChg").click(function () {
        $(".VerCodeImg").prop("src", "/Admin/Home/VerCode?date=" + (new Date().getMilliseconds()));
    });
});

function login() {
    $.ajax({
        type: "POST",
        url: "/Admin/Home/CheckLogin", //用与处理ajax的地址及函数
        data: $("#loginForm").formSerialize(),
        dataType: "json",
        beforeSend: function () {
            $('#loginbtn').off('click');
            loginbtnCssChg();
        },
        success: function (result) {
            if (result.success) {
                window.location.href = '/Admin/Home/Index';
            } else {
                bootbox.alert(result.message);
                $('#loginbtn').on('click', login);
                loginbtnCssChg();
                $("#VerChg").click();
            }
        },
        error: function (xhr, msg, e) {
            bootbox.alert("登录出错,请重试");
            $('#loginbtn').on('click', login);
            loginbtnCssChg();
            $("#VerChg").click();
        }
    });
}
function loginbtnCssChg() {
    if (!$('#loginbtn').prop("disabled")) {
        $('#loginbtn').prop("disabled", true);
        $('#loginForm input').prop("disabled", true);
    } else {
        $('#loginbtn').prop("disabled", false);
        $('#loginForm input').prop("disabled", false);
    }
}
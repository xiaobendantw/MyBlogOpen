﻿$(function () {
    $("#saveBtn").off("click");
    $("#saveBtn").on("click", Save);
});
function Save() {
    if ($("#formDetial").valid()) {
        $.ajax({
            type: "Post",
            url: "/Admin/Web/SaveWebSeting",
            data: $("#formDetial").formSerialize(),
            beforeSend: function () {
                $("#saveBtn").off("click");
            },
            success: function (data) {
                if (data.success) {
                    TipMsg(data.message, function () {
                        location.reload();
                    });
                } else {
                    TipMsg(data.message);
                    $("#saveBtn").on("click", Save);
                }
            },
            error: function (e) {
                TipMsg("出错了,请稍后再试!");
                $("#saveBtn").on("click", Save);
            }
        });
    }
}
﻿$(function () {
    $("#saveBtn").off("click");
    $("#saveBtn").on("click", save);
});
function save() {
    if ($("#formDetial").valid()) {
        $.ajax({
            type: "Post",
            url: "/Admin/System/SaveWeChatSetting",
            data: $("#formDetial").formSerialize(),
            beforeSend: function () {
                $("#saveBtn").off("click");
            },
            success: function (data) {
                if (data.success) {
                    TipMsg(data.message, $(window.parent.document).contents().find("#mainFrame")[0].contentWindow.windowsLoad);
                } else {
                    TipMsg(data.message);
                    $("#saveBtn").on("click", save);
                }
            },
            error: function (e) {
                TipMsg("出错了,请稍后再试!");
                $("#saveBtn").on("click", save);
            }
        });
    }
}
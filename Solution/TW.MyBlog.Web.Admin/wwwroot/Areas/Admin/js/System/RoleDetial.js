﻿$(function () {
    $("#saveBtn").off("click");
    $("#saveBtn").on("click", save);
    $(".mainaction").change(function () {
        if (!$(this).prop("checked")) {
            var othermain = false;
            $(this).parents(".contents").find(".mainaction").each(function () {
                if ($(this).prop("checked")) {
                    othermain = true;
                }
            });
            if (!othermain) {
                $(this).parents(".contents").find(".actionid").prop("checked", false);
            }
        }
    });
    $(".otheraction").change(function () {
        if ($(this).prop("checked")) {
            var othermain = false;
            $(this).parents(".contents").find(".mainaction").each(function () {
                if ($(this).prop("checked")) {
                    othermain = true;
                }
            });
            if (!othermain) {
                $(this).parents(".contents").find(".mainaction").first().prop("checked", true);
            }
        }
    });
});
function save() {
    ReNameItem("actionid");
    if ($("#formDetial").valid()) {
        $.ajax({
            type: "Post",
            url: "/Admin/System/SaveRole",
            data: $("#formDetial").formSerialize(),
            beforeSend: function () {
                $("#saveBtn").off("click");
            },
            success: function (data) {
                if (data.success) {
                    TipMsg(data.message, $(window.parent.document).contents().find("#mainFrame")[0].contentWindow.windowsLoad);
                } else {
                    TipMsg(data.message);
                    $("#saveBtn").on("click", save);
                }
            },
            error: function (e) {
                TipMsg("出错了,请稍后再试!");
                $("#saveBtn").on("click", save);
            }
        });
    }
}
function backList() {
    windowsLoad();
}
function ReNameItem(className) {
    var index = 0;
    $("." + className).each(function () {
        if ($(this).prop("checked")) {
            $(this).prop("name", "moduleAction[" + index + "].ID");
            index++;
        }
    });
}
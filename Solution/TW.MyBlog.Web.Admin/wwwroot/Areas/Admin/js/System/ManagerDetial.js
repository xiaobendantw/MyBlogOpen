﻿
$(function () {
    loadajaxSelect("#State");
    $("#saveBtn").off("click");
    $("#saveBtn").on("click", save);
    setTimeout(function () {
        if ($("#ID").val() == "00000000-0000-0000-0000-000000000000") {
            $("#LoginName").val("");
            $("#PassWord").val("");
        }
    }, 300);
});
function save() {
    if ($("#formDetial").valid()) {
        ReNameItem("actionid");
        $.ajax({
            type: "Post",
            url: "/Admin/System/SaveManager",
            data: $("#formDetial").formSerialize(),
            beforeSend: function () {
                $("#saveBtn").off("click");
            },
            success: function (data) {
                if (data.success) {
                    TipMsg(data.message, $(window.parent.document).contents().find("#mainFrame")[0].contentWindow.windowsLoad);
                } else {
                    TipMsg(data.message);
                    $("#saveBtn").on("click", save);
                }
            },
            error: function (e) {
                TipMsg("出错了,请稍后再试!");
                $("#saveBtn").on("click", save);
            }
        });
    }
}
function ReNameItem(className) {
    var index = 0;
    $("." + className).each(function () {
        if ($(this).prop("checked")) {
            $(this).prop("name", "Roles[" + index + "].RoleId");
            index++;
        }
    });
}
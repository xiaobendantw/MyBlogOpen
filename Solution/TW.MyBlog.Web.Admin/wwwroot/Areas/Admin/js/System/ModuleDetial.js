﻿$(function () {
    $("#id-date-picker-1").datepicker();
    loadajaxSelect("#PID");
    $("#saveBtn").off("click");
    $("#saveBtn").on("click", Save);
    $("#addChild").click(function () {
        addItem('');
    });
    $("#addChildDef").click(function () {
        if ($("#defConroller").val().length == 0 || $("#defMainAction").val().length == 0) {
            TipMsg("请输入默认控制器与方法名!");
            return false;
        }
        addItem({ ActionName: "浏览", ActionUrl: "/Admin/" + $("#defConroller").val() + "/" + $("#defMainAction").val(), OrderID: 1, ShowEnum: "True", ActionCls: "", ActionFun: "" });
        addItem({ ActionName: "添加", ActionUrl: "/Admin/" + $("#defConroller").val() + "/Add" + $("#defMainAction").val(), OrderID: 2, ShowEnum: "False", ActionCls: "fa-plus-circle purple", ActionFun: "AddItem" });
        addItem({ ActionName: "编辑", ActionUrl: "/Admin/" + $("#defConroller").val() + "/Edit" + $("#defMainAction").val(), OrderID: 3, ShowEnum: "False", ActionCls: "fa-pencil blue", ActionFun: "EditItem" });
        addItem({ ActionName: "删除", ActionUrl: "/Admin/" + $("#defConroller").val() + "/Del" + $("#defMainAction").val(), OrderID: 4, ShowEnum: "False", ActionCls: "fa-trash-o red", ActionFun: "DelItem" });
        addItem({ ActionName: "导出", ActionUrl: "/Admin/" + $("#defConroller").val() + "/Export" + $("#defMainAction").val(), OrderID: 5, ShowEnum: "False", ActionCls: "fa-download green", ActionFun: "ExportItem" });
    });
    $(document).on("click", ".delAction", function () {
        var item = $(this).parent();
        item.empty();
        item.next().remove();
        item.remove();
    });
});
function Save() {
    if ($("#formDetial").valid()) {
        ReNameItem("ActionName");
        ReNameItem("ID");
        ReNameItem("ActionUrl");
        ReNameItem("OrderID");
        ReNameItem("ShowEnum");
        ReNameItem("ActionCls");
        ReNameItem("ActionFun");
        $.ajax({
            type: "Post",
            url: "/Admin/System/SaveModule",
            data: $("#formDetial").formSerialize(),
            beforeSend: function () {
                $("#saveBtn").off("click");
            },
            success: function (data) {
                if (data.success) {
                    TipMsg(data.message, $(window.parent.document).contents().find("#mainFrame")[0].contentWindow.windowsLoad);
                } else {
                    TipMsg(data.message);
                    $("#saveBtn").on("click", Save);
                }
            },
            error: function (e) {
                TipMsg("出错了,请稍后再试!");
                $("#saveBtn").on("click", Save);
            }
        });
    }
}
function loadmoduleCls() {
    if ($("#PID").val() == "00000000-0000-0000-0000-000000000000") {
        $("#md_cls").show();
    } else {
        $("#md_cls").hide();
        $("#md_cls input").val("");
    }
}
function ReNameItem(className) {
    var index = 0;
    $("." + className).each(function () {
        $(this).prop("name", "ModuleActions[" + index + "]." + className);
        index++;
    });
}
function addItem(data) {
    var html = "";
    html += "<div><input class=\"ID\" type=\"hidden\" value=\"" + (data == "" ? "" : data.ID) + "\" />";
    html += "<label style=\"float: left;line-height: 24px; margin: 5px;\">功能名称</label><input style=\"float: left\" type=\"text\" class=\"input-small ActionName\" value=\"" + (data == "" ? "" : data.ActionName) + "\" placeholder=\"请输入功能名称\">";
    html += "<label style=\"float: left;line-height: 24px; margin: 5px;\">功能地址</label><input style=\"float: left\" type=\"text\" class=\"col-sm-2 ActionUrl\" value=\"" + (data == "" ? "" : data.ActionUrl) + "\" placeholder=\"请输入功能地址\">";
    html += "<label style=\"float: left;line-height: 24px; margin: 5px;\">排序编号</label><input style=\"float: left\" type=\"text\" class=\"input-small OrderID\" value=\"" + (data == "" ? "" : data.OrderID) + "\" placeholder=\"请输入排序编号\">";
    html += "<label style=\"float: left;line-height: 24px; margin: 5px;\">是否主菜单展示</label><select style=\"float: left\" class=\"input-small ShowEnum\"><option " + (data == "" ? "selected=\"selected\"" : data.ShowEnum == "False" ? "selected=\"selected\"" : "") + " value=\"False\">否</option><option " + (data == "" ? "" : data.ShowEnum == "False" ? "" : "selected=\"selected\"") + " value=\"True\">是</option></select>";
    html += "<label style=\"float: left;line-height: 24px; margin: 5px;\">功能样式</label><input style=\"float: left\" type=\"text\" class=\"input-small ActionCls\" value=\"" + (data == "" ? "" : data.ActionCls) + "\" placeholder=\"请输入功能样式\">";
    html += "<label style=\"float: left;line-height: 24px; margin: 5px;\">功能方法名</label><input style=\"float: left\" type=\"text\" class=\"input-small ActionFun\" value=\"" + (data == "" ? "" : data.ActionFun) + "\" placeholder=\"请输入功能方法名\">";
    html += "<label style=\"float: left;line-height: 24px; margin: 5px;cursor:pointer\" class=\"ace-icon fa fa-trash-o bigger-120 orange delAction\"></label></div><div style=\"clear: both;padding-bottom:5px;width:100%\"></div>";
    $(".actionList").append(html);
}
﻿$(document).ready(function () {
    $("#SearchStartCreateTime").datepicker();
    $("#SearchEndCreateTime").datepicker();
    $("#SearchStartCreateTime1").datepicker();
    $("#SearchEndCreateTime1").datepicker();
    GetDeal();
    GetDeal1();
    $("#btnSearch1").click(function () {
        Search();
    });
   
});
function Search() {
    GetDeal1();
}
function GetDeal() {
    $.ajax({
        url: "" + Homeurl + "?SearchStartCreateTime=" + $("#SearchStartCreateTime").val() + "&SearchEndCreateTime=" + $("#SearchEndCreateTime").val(),
        type: "post",
        contentType: "application/json;charset=utf-8",
        beforeSend: function () {
            $("#btnSearch").off("click");
        },
        dataType: "json",
        success: function (datas) {
            var show = false;
            $("#btnSearch").on("click", GetDeal);
            var option;
            var myChart = echarts.init(document.getElementById('chart_bars_vertical'));
            if (charttype == "pie") {
                var title = "";
                var index = 0;
                var data = [];
                $(datas).each(function (i, v) {
                    if (index == 0) {
                        title = this.Key; //标题只取一次
                    }
                    $(v.data).each(function (j, k) {
                        data.push({ value: k.Value, name: k.Key });
                    });
                });
                option = {
                    tooltip: {
                        trigger: 'item',
                        formatter: "{b} : {c} ({d}%)"
                    },
                    series: [
                        {
                            name: title,
                            type: 'pie',
                            radius: '75%',
                            center: ['50%', '50%'],
                            data: data
                        }
                    ]
                };
                if (data.length > 0) {
                    show = true;
                }
            }
            else if (charttype == "bar" || charttype == "line") {
                var title = [];
                var foot = [];
                var data = [];
                var index = 0;
                $(datas).each(function () {
                    title.push(this.Title);
                    var list = [];
                    $(this.data).each(function () {
                        if (index == 0) {
                            foot.push(this.Key); //X轴只取一次
                        }
                        list.push(this.Value);
                    });
                    data.push({
                        name: this.Title,
                        type: charttype,
                        data: list
                    });
                    index++;
                });
                option = {
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                            type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                        }
                    },
                    legend: {
                        data: title
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    xAxis: [
                        {
                            type: 'category',
                            data: foot,
                            boundaryGap: (charttype == "bar")
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            splitNumber: 1
                        }
                    ],
                    color: ['#FF00FF'],
                    series: data
                };
                $(data).each(function (i, v) {
                    if (v.data.length > 0) {
                        show = true;
                        return;
                    }
                });
            }
            // 使用刚指定的配置项和数据显示图表。
            if (show) {
                myChart.setOption(option);
            } else {
                $("#chart_bars_vertical").css("text-align", "center");
                $("#chart_bars_vertical").css("line-height", "350px");
                $("#chart_bars_vertical").html("暂无数据");
            }
        }
    });
}
function GetDeal1() {
    $.ajax({
        url: "" + Blogurl + "?SearchStartCreateTime=" + $("#SearchStartCreateTime1").val() + "&SearchEndCreateTime=" + $("#SearchEndCreateTime1").val(),
        type: "post",
        contentType: "application/json;charset=utf-8",
        beforeSend: function () {
            $("#btnSearch").off("click");
        },
        dataType: "json",
        success: function (datas) {
            var show = false;
            $("#btnSearch").on("click", GetDeal);
            var option;
            var myChart = echarts.init(document.getElementById('chart_bars_vertical1'));
            if (charttype == "pie") {
                var title = "";
                var index = 0;
                var data = [];
                $(datas).each(function (i, v) {
                    if (index == 0) {
                        title = this.Key; //标题只取一次
                    }
                    $(v.data).each(function (j, k) {
                        data.push({ value: k.Value, name: k.Key });
                    });
                });
                option = {
                    tooltip: {
                        trigger: 'item',
                        formatter: "{b} : {c} ({d}%)"
                    },
                    series: [
                        {
                            name: title,
                            type: 'pie',
                            radius: '75%',
                            center: ['50%', '50%'],
                            data: data
                        }
                    ]
                };
                if (data.length > 0) {
                    show = true;
                }
            }
            else if (charttype == "bar" || charttype == "line") {
                var title = [];
                var foot = [];
                var data = [];
                var index = 0;
                $(datas).each(function () {
                    title.push(this.Title);
                    var list = [];
                    $(this.data).each(function () {
                        if (index == 0) {
                            foot.push(this.Key); //X轴只取一次
                        }
                        list.push(this.Value);
                    });
                    data.push({
                        name: this.Title,
                        type: charttype,
                        data: list
                    });
                    index++;
                });
                option = {
                    tooltip: {
                        trigger: 'axis',
                        axisPointer: {            // 坐标轴指示器，坐标轴触发有效
                            type: 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
                        }
                    },
                    legend: {
                        data: title
                    },
                    grid: {
                        left: '3%',
                        right: '4%',
                        bottom: '3%',
                        containLabel: true
                    },
                    xAxis: [
                        {
                            type: 'category',
                            data: foot,
                            boundaryGap: (charttype == "bar")
                        }
                    ],
                    yAxis: [
                        {
                            type: 'value',
                            splitNumber: 1
                        }
                    ],
                    color: ['#00BFFF'],
                    series: data
                };
                $(data).each(function (i, v) {
                    if (v.data.length > 0) {
                        show = true;
                        return;
                    }
                });
            }
            // 使用刚指定的配置项和数据显示图表。
            if (show) {
                myChart.setOption(option);
            } else {
                $("#chart_bars_vertical1").css("text-align", "center");
                $("#chart_bars_vertical1").css("line-height", "350px");
                $("#chart_bars_vertical1").html("暂无数据");
            }
        }
    });
}
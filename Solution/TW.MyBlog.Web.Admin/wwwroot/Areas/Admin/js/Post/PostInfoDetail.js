﻿$(function () {
    $("#saveBtn").off("click");
    $("#saveBtn").on("click", Save);
    $("#PubDate").datetimepicker();
    loadajaxSelect("#CategoryId");
    loadImageUpload("#upimage", ["#ImageUrl"], false, closeSmallImg);
    loadetditor();
    $(window.parent.document).contents().find("#mainFrame")[0].contentWindow.delUE("myEditor");
});
function loadetditor() {
    var editor_a = new baidu.editor.ui.Editor({
        initialFrameHeight: 1000,
        scaleEnabled: true,//不随内容自动扩展高度
        autoFloatEnabled: false//取消Ueditor工具栏浮动
    });
    editor_a.render('myEditor');
}
function Save() {
    if ($("#formDetial").valid()) {
        getContent();//获取富文本值
        $.ajax({
            type: "Post",
            url: "/Admin/Post/SavePostInfo",
            data: $("#formDetial").formSerialize(),
            beforeSend: function () {
                $("#saveBtn").off("click");
            },
            success: function (data) {
                if (data.success) {
                    TipMsg(data.message, $(window.parent.document).contents().find("#mainFrame")[0].contentWindow.windowsLoad);
                } else {
                    TipMsg(data.message);
                    $("#saveBtn").on("click", Save);
                }
            },
            error: function (e) {
                TipMsg("出错了,请稍后再试!");
                $("#saveBtn").on("click", Save);
            }
        });
    }
}
function closeSmallImg() {
    $("#showSmallImg").hide();
}
function getContent() {
    $("#Content").text(UE.getEditor('myEditor').getContent());

}
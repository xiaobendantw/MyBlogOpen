﻿$(function () {
		
		
		$("#saveBtn").off("click");
        $("#saveBtn").on("click", Save);
    });
	function Save(){
		if ($("#formDetail").valid()) {
                $.ajax({
                    type: "Post",
                    url: "/Admin/Post/SavePostComment",
                    data: $("#formDetail").formSerialize(),
					 beforeSend: function () {
						$("#saveBtn").off("click");
					},
                    success: function (data) {
                        if (data.success) {
                            TipMsg(data.message, $(window.parent.document).contents().find("#mainFrame")[0].contentWindow.windowsLoad);
                        } else {
                            TipMsg(data.message);
							$("#saveBtn").on("click", Save);
                        }
                    },
					error: function (e) {
						TipMsg("出错了,请稍后再试!");
						$("#saveBtn").on("click", Save);
					}
                });
            }
	}
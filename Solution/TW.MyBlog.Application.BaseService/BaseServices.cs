﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using TW.MyBlog.Infrastructure.Common;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Repository;

namespace TW.MyBlog.Application.BaseService
{
    public abstract class BaseServices<T> : BaseFrameworkServices<T>
        where T : class, new()
    {
        
        protected BaseServices(IUnitOfWorkFramework _unitOfWork, IRepository<T, EntityList<T>> _Repository)
            : base(_unitOfWork, _Repository)
        {
        }
    }
}
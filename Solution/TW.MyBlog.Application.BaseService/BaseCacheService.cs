﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Cache;

namespace TW.MyBlog.Application.BaseService
{
    public abstract class BaseCacheService : IBaseCacheService
    {

        protected readonly ICachePolicy _cachePolicy;

        protected BaseCacheService(ICachePolicy cachePolicy)
        {
            _cachePolicy = cachePolicy;
        }
        /// <summary>
        /// 获取实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetCache<T>(string key) where T : class
        {
            return _cachePolicy.Get<T>(key);
        }
        /// <summary>
        /// 添加一个实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="items"></param>
        public void Add<T>(string key, T items)
        {
            _cachePolicy.Add(key, items);
        }
        /// <summary>
        /// 添加一个实体并设置过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="items"></param>
        /// <param name="dt"></param>
        public void Add<T>(string key, T items,DateTime dt)
        {
            _cachePolicy.Add(key, items, dt);
        }
        /// <summary>
        /// 获取一个实体,不存在则添加一个
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="loadedItems"></param>
        /// <returns></returns>
        public T GetOrAdd<T>(string key, T loadedItems) where T : class
        {
            var items = _cachePolicy.Get<T>(key);

            if (items == null)
            {
                _cachePolicy.Add(key, loadedItems);
                return loadedItems;
            }

            return items;
        }
        /// <summary>
        /// 获取一个实体,不存在则添加一个
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="howToGet"></param>
        /// <returns></returns>
        public T GetOrAdd<T>(string key, Func<T> howToGet) where T : class
        {
            var items = _cachePolicy.Get<T>(key);


            if (items == null)
            {
                var loadedItems = howToGet();
                _cachePolicy.Add(key, loadedItems);
                return loadedItems;
            }

            var type = items.GetType();
            if (type == typeof(int) && items.Equals(0))
            {
                var loadedItems = howToGet();
                _cachePolicy.Add(key, loadedItems);
                return loadedItems;
            }

            return items;
        }
        /// <summary>
        /// 获取一个实体,不存在则添加一个,并设置过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="howToGet"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public T GetOrAdd<T>(string key, Func<T> howToGet, DateTime dt) where T : class
        {
            var items = _cachePolicy.Get<T>(key);
            if (items == null)
            {
                var loadedItems = howToGet();
                _cachePolicy.Add(key, loadedItems, dt);
                return loadedItems;
            }

            var type = items.GetType();
            if (type == typeof(int) && items.Equals(0))
            {
                var loadedItems = howToGet();
                _cachePolicy.Add(key, loadedItems, dt);
                return loadedItems;
            }
            return items;
        }
        /// <summary>
        /// 获取一个实体,不存在则添加一个,并设置过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="howToGet"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        public T GetOrAdd<T>(string key, Func<T> howToGet, TimeSpan dt) where T : class
        {
            var items = _cachePolicy.Get<T>(key);
            if (items == null)
            {
                var loadedItems = howToGet();
                _cachePolicy.Add(key, loadedItems, dt);
                return loadedItems;
            }

            var type = items.GetType();
            if (type == typeof(int) && items.Equals(0))
            {
                var loadedItems = howToGet();
                _cachePolicy.Add(key, loadedItems, dt);
                return loadedItems;
            }
            return items;
        }
        /// <summary>
        /// 移除一个实体
        /// </summary>
        /// <param name="key"></param>
        public void Remove(string key)
        {
            _cachePolicy.Delete(key);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Web;
using TW.MyBlog.Infrastructure.Common;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Repository;

namespace TW.MyBlog.Application.BaseService
{
    public abstract class BaseFrameworkServices<T> : IBaseServices<T>
        where T : class, new()
    {
        protected readonly IRepository<T, EntityList<T>> repository;
        protected readonly IUnitOfWorkFramework unitOfWork;

        protected BaseFrameworkServices(IUnitOfWorkFramework _unitOfWork, IRepository<T, EntityList<T>> _Repository)
        {
            this.unitOfWork = _unitOfWork;
            this.repository = _Repository;
        }

        #region 异步 IBaseServices<T> 成员
        /// <summary>
        /// 异步获取分页泛型实体(表达式排序,仅支持单一字段排序)
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="specification"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="IsDESC"></param>
        /// <param name="AsNoTracking"></param>
        /// <param name="thenByExpression"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task<EntityList<T>> FindAllAsync<S>(int PageIndex, int PageSize, ISpecification<T> specification, System.Linq.Expressions.Expression<Func<T, S>> orderByExpression, bool IsDESC, bool? AsNoTracking = true, List<SortParms<T>> thenByExpression = null)
        {
            return await this.repository.FindAllAsync<S>(PageIndex, PageSize, specification, orderByExpression, IsDESC, AsNoTracking, thenByExpression);
        }
        /// <summary>
        /// 异步获取分页泛型实体(字典排序,支持多关键字排序)
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="specification"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="AsNoTracking"></param>
        /// <param name="thenByExpression"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task<EntityList<T>> FindAllAsync<S>(int PageIndex, int PageSize, ISpecification<T> specification, Dictionary<string, string> orderByExpression, bool? AsNoTracking = true, List<SortParms<T>> thenByExpression = null)
        {
            return await this.repository.FindAllAsync<S>(PageIndex, PageSize, specification, orderByExpression, AsNoTracking, thenByExpression);
        }
        /// <summary>
        /// 异步获取所有整个泛型实体
        /// </summary>
        /// <param name="AsNoTracking"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetAllAsync(bool? AsNoTracking = true)
        {
            return await this.repository.GetAllAsync(AsNoTracking);
        }
        /// <summary>
        /// 异步获取部分泛型实体(规约查询)
        /// </summary>
        /// <param name="specification"></param>
        /// <param name="AsNoTracking"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetManyAsync(ISpecification<T> specification, bool? AsNoTracking = true)
        {
            return await this.repository.GetManyAsync(specification, AsNoTracking);
        }
        /// <summary>
        /// 异步获取TopN泛型实体(规约查询+表达式排序)
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="TopN"></param>
        /// <param name="specification"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="IsDESC"></param>
        /// <param name="AsNoTracking"></param>
        /// <param name="thenByExpression"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task<IEnumerable<T>> GetListByTopNAsync<S>(int TopN, ISpecification<T> specification, System.Linq.Expressions.Expression<Func<T, S>> orderByExpression, bool IsDESC, bool? AsNoTracking = true, List<SortParms<T>> thenByExpression = null)
        {
            return await this.repository.GetListByTopNAsync<S>(TopN, specification, orderByExpression, IsDESC, AsNoTracking, thenByExpression);
        }
        /// <summary>
        /// 异步获取单一实体(规约查询)
        /// </summary>
        /// <param name="specification"></param>
        /// <param name="AsNoTracking"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task<T> GetByConditionAsync(ISpecification<T> specification, bool? AsNoTracking = false)
        {
            return await this.repository.GetByConditionAsync(specification, AsNoTracking);
        }
        /// <summary>
        /// 异步根据默认主键返回单一实体
        /// </summary>
        /// <param name="key"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task<T> GetByKeyAsync(object key)
        {
            return await this.repository.GetByKeyAsync(key);
        }
        /// <summary>
        /// 异步添加一个实体并持久化
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task AddAsync(T entity)
        {
            this.repository.Add(entity);
            await this.unitOfWork.CommitAsync();
        }
        /// <summary>
        /// 异步批量添加实体并持久化
        /// </summary>
        /// <param name="entities"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task AddBatchAsync(IEnumerable<T> entities)
        {
            this.repository.AddBatch(entities);
            await this.unitOfWork.CommitAsync();
        }
        /// <summary>
        /// 异步更新一个实体(所有字段)并持久化
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task ModifyAsync(T entity)
        {
            this.repository.Modify(entity);
            await this.unitOfWork.CommitAsync();
        }
        /// <summary>
        /// 异步更新一个实体(按需字段)并持久化
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="configureAwait"></param>
        /// <param name="properties"></param>
        /// <returns></returns>
        public async Task UpdateAsync(T entity, params Expression<Func<T, object>>[] properties)
        {
            this.repository.Update(entity, properties);
            await this.unitOfWork.CommitAsync();
        }
        /// <summary>
        /// 根据条件异步逻辑删除实体(确保存在IsDeleted字段)
        /// </summary>
        /// <param name="specification"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public async Task LogicDeleteAsync(ISpecification<T> specification)
        {
            this.repository.LogicDelete(specification);
            await this.unitOfWork.CommitAsync();
        }
        /// <summary>
        /// 异步物理删除单一实体
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task RemoveAsync(T entity)
        {
            this.repository.Remove(entity);
            await this.unitOfWork.CommitAsync();
        }
        /// <summary>
        /// 根据条件异步物理删除实体
        /// </summary>
        /// <param name="specification"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task RemoveAsync(ISpecification<T> specification)
        {
            this.repository.Remove(specification);
            await this.unitOfWork.CommitAsync();
        }
        /// <summary>
        /// 异步判断实体是否存在
        /// </summary>
        /// <param name="specification"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public virtual async Task<bool> ExistsAsync(ISpecification<T> specification)
        {
            return await this.repository.ExistsAsync(specification);
        }
        /// <summary>
        /// 异步获取实体数量
        /// </summary>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public async Task<int> GetCountAsync(bool configureAwait = true)
        {
            return await this.repository.GetCountAsync(configureAwait);
        }
        /// <summary>
        /// 异步根据条件获取实体数量
        /// </summary>
        /// <param name="specification"></param>
        /// <param name="configureAwait"></param>
        /// <returns></returns>
        public async Task<int> GetCountAsync(ISpecification<T> specification)
        {
            return await this.repository.GetCountAsync(specification);
        }
        #endregion

        #region 同步 IBaseServices<T> 成员
        /// <summary>
        /// 获取分页泛型实体(表达式排序,仅支持单一字段排序)
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="specification"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="IsDESC"></param>
        /// <param name="AsNoTracking"></param>
        /// <param name="thenByExpression"></param>
        /// <returns></returns>
        public virtual EntityList<T> FindAll<S>(int PageIndex, int PageSize, ISpecification<T> specification, System.Linq.Expressions.Expression<Func<T, S>> orderByExpression, bool IsDESC, bool? AsNoTracking = true, List<SortParms<T>> thenByExpression = null)
        {
            return this.repository.FindAll<S>(PageIndex, PageSize, specification, orderByExpression, IsDESC, AsNoTracking, thenByExpression);
        }
        /// <summary>
        /// 获取分页泛型实体(字典排序,支持多关键字排序)
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="PageIndex"></param>
        /// <param name="PageSize"></param>
        /// <param name="specification"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="AsNoTracking"></param>
        /// <param name="thenByExpression"></param>
        /// <returns></returns>
        public virtual EntityList<T> FindAll<S>(int PageIndex, int PageSize, ISpecification<T> specification, Dictionary<string, string> orderByExpression, bool? AsNoTracking = true, List<SortParms<T>> thenByExpression = null)
        {
            return this.repository.FindAll<S>(PageIndex, PageSize, specification, orderByExpression, AsNoTracking, thenByExpression);
        }
        /// <summary>
        /// 获取所有整个泛型实体
        /// </summary>
        /// <param name="AsNoTracking"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetAll(bool? AsNoTracking = true)
        {
            return this.repository.GetAll(AsNoTracking);
        }
        /// <summary>
        /// 获取部分泛型实体(规约查询)
        /// </summary>
        /// <param name="specification"></param>
        /// <param name="AsNoTracking"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetMany(ISpecification<T> specification, bool? AsNoTracking = true)
        {
            return this.repository.GetMany(specification, AsNoTracking);
        }
        /// <summary>
        /// 获取TopN泛型实体(规约查询+表达式排序)
        /// </summary>
        /// <typeparam name="S"></typeparam>
        /// <param name="TopN"></param>
        /// <param name="specification"></param>
        /// <param name="orderByExpression"></param>
        /// <param name="IsDESC"></param>
        /// <param name="AsNoTracking"></param>
        /// <param name="thenByExpression"></param>
        /// <returns></returns>
        public virtual IEnumerable<T> GetListByTopN<S>(int TopN, ISpecification<T> specification, System.Linq.Expressions.Expression<Func<T, S>> orderByExpression, bool IsDESC, bool? AsNoTracking = true, List<SortParms<T>> thenByExpression = null)
        {
            return this.repository.GetListByTopN<S>(TopN, specification, orderByExpression, IsDESC, AsNoTracking, thenByExpression);
        }
        /// <summary>
        /// 获取单一实体(规约查询)
        /// </summary>
        /// <param name="specification"></param>
        /// <param name="AsNoTracking"></param>
        /// <returns></returns>
        public virtual T GetByCondition(ISpecification<T> specification, bool? AsNoTracking = false)
        {
            return this.repository.GetByCondition(specification, AsNoTracking);
        }
        /// <summary>
        /// 根据默认主键返回单一实体
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public virtual T GetByKey(object key)
        {
            return this.repository.GetByKey(key);
        }
        /// <summary>
        /// 添加一个实体并持久化
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Add(T entity)
        {
            this.repository.Add(entity);
            this.unitOfWork.Commit();
        }
        /// <summary>
        /// 批量添加实体并持久化
        /// </summary>
        /// <param name="entities"></param>
        public virtual void AddBatch(IEnumerable<T> entities)
        {
            this.repository.AddBatch(entities);
            this.unitOfWork.Commit();
        }
        /// <summary>
        /// 更新一个实体(所有字段)并持久化
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Modify(T entity)
        {
            this.repository.Modify(entity);
            this.unitOfWork.Commit();
        }
        /// <summary>
        /// 更新一个实体(按需字段)并持久化
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="properties"></param>
        public void Update(T entity, params Expression<Func<T, object>>[] properties)
        {
            this.repository.Update(entity, properties);
            this.unitOfWork.Commit();
        }
        /// <summary>
        /// 根据条件逻辑删除实体(确保存在IsDeleted字段)
        /// </summary>
        /// <param name="specification"></param>
        public void LogicDelete(ISpecification<T> specification)
        {
            this.repository.LogicDelete(specification);
            this.unitOfWork.Commit();
        }
        /// <summary>
        /// 物理删除单一实体
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Remove(T entity)
        {
            this.repository.Remove(entity);
            this.unitOfWork.Commit();
        }
        /// <summary>
        /// 根据条件物理删除实体
        /// </summary>
        /// <param name="specification"></param>
        public virtual void Remove(ISpecification<T> specification)
        {
            this.repository.Remove(specification);
            this.unitOfWork.Commit();
        }
        /// <summary>
        /// 判断实体是否存在
        /// </summary>
        /// <param name="specification"></param>
        /// <returns></returns>
        public virtual bool Exists(ISpecification<T> specification)
        {
            return this.repository.Exists(specification);
        }

        /// <summary>
        /// 持久化by工作单元模式
        /// </summary>
        public virtual void SaveChanges()
        {
            this.unitOfWork.Commit();
        }



        /// <summary>
        /// 获取实体数量
        /// </summary>
        /// <returns></returns>
        public int GetCount()
        {
            return this.repository.GetCount();
        }
        /// <summary>
        /// 根据条件获取实体数量
        /// </summary>
        /// <param name="specification"></param>
        /// <returns></returns>
        public int GetCount(ISpecification<T> specification)
        {
            return this.repository.GetCount(specification);
        }

        #endregion
    }
}
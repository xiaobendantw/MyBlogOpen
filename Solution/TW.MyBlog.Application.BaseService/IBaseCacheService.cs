﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TW.MyBlog.Application.BaseService
{
    public interface IBaseCacheService
     {
         /// <summary>
         /// 获取实体
         /// </summary>
         /// <typeparam name="T"></typeparam>
         /// <param name="key"></param>
         /// <returns></returns>
         T GetCache<T>(string key) where T : class;

        /// <summary>
        /// 添加一个实体
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="items"></param>
        void Add<T>(string key, T items);

        /// <summary>
        /// 添加一个实体并设置过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="items"></param>
        /// <param name="dt"></param>
        void Add<T>(string key, T items, DateTime dt);

        /// <summary>
        /// 获取一个实体,不存在则添加一个
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="loadedItems"></param>
        /// <returns></returns>
        T GetOrAdd<T>(string key, T loadedItems) where T : class;

        /// <summary>
        /// 获取一个实体,不存在则添加一个
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="howToGet"></param>
        /// <returns></returns>
        T GetOrAdd<T>(string key, Func<T> howToGet) where T : class;

        /// <summary>
        /// 获取一个实体,不存在则添加一个,并设置过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="howToGet"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        T GetOrAdd<T>(string key, Func<T> howToGet, DateTime dt) where T : class;

        /// <summary>
        /// 获取一个实体,不存在则添加一个,并设置过期时间
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="howToGet"></param>
        /// <param name="dt"></param>
        /// <returns></returns>
        T GetOrAdd<T>(string key, Func<T> howToGet, TimeSpan dt) where T : class;

        /// <summary>
        /// 移除一个实体
        /// </summary>
        /// <param name="key"></param>
        void Remove(string key);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Loader;
using System.Text;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Repository;
using TW.Utility;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyModel;
using Microsoft.IdentityModel.Logging;

namespace TW.MyBlog.Infrastructure.IoC.CoreNative
{
    /// <summary>
    /// 通过反射动态构建依赖注入
    /// </summary>
    public static class IocCoreNativeServiceExtension
    {
        public static IServiceCollection RegisterAssembly(this IServiceCollection service)
        {
            AssemblyHelper.GetAllAssemblies().ForEach(interfaceAssembly =>
            {
                var types = interfaceAssembly.GetTypes().Where(t =>
                    t.GetTypeInfo().IsInterface && !t.GetTypeInfo().IsGenericType &&
                    t.GetInterfaces().Contains(typeof(IDependencyDynamicService)));
                foreach (var type in types)
                {
                    var implementTypeName = type.Name.Substring(1);
                    var implementType = AssemblyHelper.GetImplementType(implementTypeName, type);
                    if (implementType != null)
                    {
                        service.AddScoped(type, implementType);
                    }
                }
            });
            return service;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Cache;
using TW.MyBlog.Infrastructure.Repository;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.DependencyInjection;

namespace TW.MyBlog.Infrastructure.IoC.CoreNative
{
    public static class IocCoreNativeContainerFactory
    {
        public static void RegServices(this IServiceCollection services)
        {
            //对基础设施的依赖注入
            services.AddScoped<IUnitOfWorkFramework, UnitOfWork>()
                .AddScoped<IDatabaseFactory, DatabaseFactory>()
                .AddTransient<ICachePolicy, HttpRunTimeCachePolicy>()
                .AddTransient<IHttpContextAccessor, HttpContextAccessor>();
            //对业务的依赖注入
            services.RegisterAssembly();
        }
    }
}

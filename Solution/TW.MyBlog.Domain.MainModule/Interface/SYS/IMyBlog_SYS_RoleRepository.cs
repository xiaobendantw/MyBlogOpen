﻿using System.Collections.Generic;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository;
using System;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Model.DTO.Manager;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_SYS_RoleRepository : IRepository<MyBlog_SYS_Role, EntityList<MyBlog_SYS_Role>>, IDependencyDynamicService
    {
        Task<EntityList<RoleDTO>> GetRoleList(int page, int rows, ISpecification<MyBlog_SYS_Role> specification,
            Dictionary<string, string> sort);
        Task<List<RoleListDTO>> LoadMangerRoleList(Guid id);

        Task<MyBlog_SYS_Role> GetRoleAndAction(Guid id);
    }
}
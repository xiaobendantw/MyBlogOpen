﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_SYS_RoleDomainServices : IDependencyDynamicService
    {
        Task AddRole(MyBlog_SYS_Role model, List<MyBlog_SYS_ModuleAction> moduleAction);
        Task<IEnumerable<string>> DelRole(IEnumerable<Guid> ids);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Infrastructure.Common.Enums;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_SYS_PhoneMessageRecordDomainServices : IDependencyDynamicService
    {
        Task<string> SendSingleMessage(string telPhone, Enum_PhoneMessageSendType sendType,
            Enum_PhoneMessageSendUserType sendUserType, string sendOpenId, string sendIpAddr, Guid? sendUserId);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO.Manager;
using TW.MyBlog.Infrastructure.Repository;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_SYS_ModuleRepository : IRepository<MyBlog_SYS_Module, EntityList<MyBlog_SYS_Module>>, IDependencyDynamicService
    {
        Task<List<MenuModuleListDTO>> LoadModuleList(Guid RoleId);
        Task<dynamic> GetFatherModule(Guid fatherId, Guid itemId);

        Task<IEnumerable<RecursiveModuleDTO>> GetModuleByRecursive(Guid? Id = null);
        Task<EntityList<ModuleDTO>> GetModuleList(int page, int rows, ISpecification<MyBlog_SYS_Module> specification,
            Dictionary<string, string> sort);

        Task<List<MyBlog_SYS_Module>> GetLoginModule();
    }
}
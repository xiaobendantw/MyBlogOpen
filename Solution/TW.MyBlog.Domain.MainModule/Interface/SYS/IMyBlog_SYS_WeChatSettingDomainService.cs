﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_SYS_WeChatSettingDomainServices : IDependencyDynamicService
    {
        void ModifyModel(MyBlog_SYS_WeChatSetting item);
    }
}

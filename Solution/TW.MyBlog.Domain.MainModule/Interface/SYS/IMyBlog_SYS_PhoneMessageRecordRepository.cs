﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Infrastructure.Repository;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_SYS_PhoneMessageRecordRepository : IRepository<MyBlog_SYS_PhoneMessageRecord, EntityList<MyBlog_SYS_PhoneMessageRecord>>, IDependencyDynamicService
    {
        Task<bool> CheckCodeValidity(string phone, string code, Enum_PhoneMessageSendType sendType,
            Enum_PhoneMessageSendUserType sendUserType, string openId, Guid? userId);
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO.Manager;
using TW.MyBlog.Infrastructure.Repository;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_SYS_ModuleActionRepository : IRepository<MyBlog_SYS_ModuleAction, EntityList<MyBlog_SYS_ModuleAction>>, IDependencyDynamicService
    {
        Task<List<LoginModultActionDTO>> GetModultAction();
    }
}
﻿using System.Collections.Generic;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_SYS_DistrictRepository : IRepository<MyBlog_SYS_District, EntityList<MyBlog_SYS_District>>, IDependencyDynamicService
    {
    }
}
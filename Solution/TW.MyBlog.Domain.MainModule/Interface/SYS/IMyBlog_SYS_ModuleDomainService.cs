﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO.Manager;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_SYS_ModuleDomainServices : IDependencyDynamicService
    {
        Task<string> GetLoginRoleMenu(LoginUserDTO loginUser);
        Task AddModule(MyBlog_SYS_Module item);
        Task<IEnumerable<string>> DelModule(IEnumerable<Guid> delId);
    }
}

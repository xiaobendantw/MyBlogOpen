﻿using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Infrastructure.Repository;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_SYS_RoleActionRepository : IRepository<MyBlog_SYS_RoleAction, EntityList<MyBlog_SYS_RoleAction>>, IDependencyDynamicService
    {

    }
}
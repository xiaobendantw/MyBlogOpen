using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Repository;
namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_Reptilian_InfoRepository : IRepository<MyBlog_Reptilian_Info, EntityList<MyBlog_Reptilian_Info>>, IDependencyDynamicService
    {
		/// <summary>
        /// 根据条件分页获取/导出一个List<ReptilianInfoDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		Task<EntityList<ReptilianInfoDTO>> GetInfoList(int page, int rows, ISpecification<ReptilianInfoDTO> specification,
            Dictionary<string, string> sort, bool export);
        /// <summary>
        /// 获取佳句
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        dynamic GetBeautifulsentence(int pageIndex, int pageSize);
    }
}
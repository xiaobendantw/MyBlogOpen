using System;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Repository;
namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_Post_PostInfoRepository : IRepository<MyBlog_Post_PostInfo, EntityList<MyBlog_Post_PostInfo>>, IDependencyDynamicService
    {
        /// <summary>
        /// 根据条件分页获取/导出一个List<PostPostInfoDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
        Task<EntityList<PostPostInfoDTO>> GetPostInfoList(int page, int rows, ISpecification<PostPostInfoDTO> specification,
            Dictionary<string, string> sort, bool export);

        /// <summary>
        /// 获取博文列表
        /// </summary>
        /// <returns></returns>
        dynamic GetPostList();

        /// <summary>
        /// 获取首页推荐博文列表
        /// </summary>
        /// <returns></returns>
        dynamic GetSelectPostList(int pageIndex, int pageSize);
        /// <summary>
        /// 获取首页热门博文列表
        /// </summary>
        /// <returns></returns>
        dynamic GetHotPostList();

        /// <summary>
        /// 获取博文列表
        /// </summary>
        /// <param name="categoryId">分类编号</param>
        /// <param name="pageIndex">分页下标</param>
        /// <param name="pageSize">分页大小</param>
        /// <returns></returns>
        dynamic GetPostLists(Guid categoryId, int pageIndex, int pageSize);
    }
}
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Repository;
namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_Post_PostCategoryRepository : IRepository<MyBlog_Post_PostCategory, EntityList<MyBlog_Post_PostCategory>>, IDependencyDynamicService
    {
		/// <summary>
        /// 根据条件分页获取/导出一个List<PostPostCategoryDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		Task<EntityList<PostPostCategoryDTO>> GetPostCategoryList(int page, int rows, ISpecification<PostPostCategoryDTO> specification,
            Dictionary<string, string> sort, bool export);
        dynamic GetPostCategory();
    }
}
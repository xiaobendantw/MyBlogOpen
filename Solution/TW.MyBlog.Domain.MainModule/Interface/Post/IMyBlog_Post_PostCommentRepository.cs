using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Repository;
namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_Post_PostCommentRepository : IRepository<MyBlog_Post_PostComment, EntityList<MyBlog_Post_PostComment>>, IDependencyDynamicService
    {
		/// <summary>
        /// 根据条件分页获取/导出一个List<PostPostCommentDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		Task<EntityList<PostPostCommentDTO>> GetPostCommentList(int page, int rows, ISpecification<PostPostCommentDTO> specification,
            Dictionary<string, string> sort, bool export);
    }
}
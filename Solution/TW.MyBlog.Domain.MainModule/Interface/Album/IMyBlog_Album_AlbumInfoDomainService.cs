using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TW.MyBlog.Domain.Model;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_Album_AlbumInfoDomainServices : IDependencyDynamicService
    {
	     /// <summary>
         /// 添加/修改一个MyBlog_Album_AlbumInfo
         /// </summary>
         /// <param name="item">实体</param>
         /// <returns></returns>
		 Task ModifyModel(MyBlog_Album_AlbumInfo item);

		 /// <summary>
         /// 逻辑删除实体
         /// </summary>
         /// <param name="ids"></param>
         /// <returns></returns>
		 Task DelModelsById(IEnumerable<Guid> ids);
    }
}

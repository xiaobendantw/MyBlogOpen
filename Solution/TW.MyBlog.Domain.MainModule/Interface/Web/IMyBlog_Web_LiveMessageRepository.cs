using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Repository;
namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_Web_LiveMessageRepository : IRepository<MyBlog_Web_LiveMessage, EntityList<MyBlog_Web_LiveMessage>>, IDependencyDynamicService
    {
		/// <summary>
        /// 根据条件分页获取/导出一个List<WebLiveMessageDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		Task<EntityList<WebLiveMessageDTO>> GetLiveMessageList(int page, int rows, ISpecification<WebLiveMessageDTO> specification,
            Dictionary<string, string> sort, bool export);
        /// <summary>
        /// 获取留言列表
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="pageSize"></param>
        /// <returns></returns>
        dynamic GetLiveMessage(int pageIndex, int pageSize);
    }
}
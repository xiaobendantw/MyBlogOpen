using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Repository;
namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_Web_WebSetingRepository : IRepository<MyBlog_Web_WebSeting, EntityList<MyBlog_Web_WebSeting>>, IDependencyDynamicService
    {
		/// <summary>
        /// 根据条件分页获取/导出一个List<WebWebSetingDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		Task<EntityList<WebWebSetingDTO>> GetWebSetingList(int page, int rows, ISpecification<WebWebSetingDTO> specification,
            Dictionary<string, string> sort, bool export);
    }
}
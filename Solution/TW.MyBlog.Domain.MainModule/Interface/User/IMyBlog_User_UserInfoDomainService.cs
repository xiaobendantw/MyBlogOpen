using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TW.MyBlog.Domain.Model;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_User_UserInfoDomainServices : IDependencyDynamicService
    {
	     /// <summary>
         /// 添加/修改一个MyBlog_User_UserInfo
         /// </summary>
         /// <param name="item">实体</param>
         /// <returns></returns>
		 Task ModifyModel(MyBlog_User_UserInfo item);

		 /// <summary>
         /// 逻辑删除实体
         /// </summary>
         /// <param name="ids"></param>
         /// <returns></returns>
		 Task DelModelsById(IEnumerable<Guid> ids);
    }
}

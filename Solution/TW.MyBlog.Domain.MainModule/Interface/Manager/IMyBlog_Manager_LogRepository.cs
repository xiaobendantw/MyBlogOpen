﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_Manager_LogRepository : IRepository<MyBlog_Manager_Log, EntityList<MyBlog_Manager_Log>>, IDependencyDynamicService
    {
        Task<EntityList<LogDTO>> GetLogList(int page, int rows, ISpecification<MyBlog_Manager_Log> specification,
            Dictionary<string, string> sort);
        Task<List<dynamic>> ManagerLogChart(ISpecification<MyBlog_Manager_Log> specification);
    }
}
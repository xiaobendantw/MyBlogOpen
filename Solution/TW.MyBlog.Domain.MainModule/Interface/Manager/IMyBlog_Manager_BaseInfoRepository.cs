﻿using System;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_Manager_BaseInfoRepository : IRepository<MyBlog_Manager_BaseInfo, EntityList<MyBlog_Manager_BaseInfo>>, IDependencyDynamicService
    {
        Task<EntityList<BaseInfoDTO>> GetBaseInfoList(int page, int rows, ISpecification<MyBlog_Manager_BaseInfo> specification,
             Dictionary<string, string> sort);

        Task<MyBlog_Manager_BaseInfo> GetLoginUser(ISpecification<MyBlog_Manager_BaseInfo> specification);
        Task<bool> ExistsRoleByRoleId(IEnumerable<Guid> ids);
    }
}
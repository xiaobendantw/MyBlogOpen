﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model.DTO.Manager;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_Manager_LogDomainServices : IDependencyDynamicService
    {
        void Log(string title, string Contents, LoginUserDTO mydto = null);
    }
}

﻿using TW.MyBlog.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model.DTO.Manager;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_Manager_BaseInfoDomainServices : IDependencyDynamicService
    {
        Task<LoginUserDTO> CheckLogin(string loginName, string passWord, bool remmberMe, bool? pwdIsMd5 = false, string validateCode = null);
        Task<LoginUserDTO> ChangeUserInfo(Guid ID, string nickName, string newPassword, string confirmPassword, int userChangeType,string oldpassword);
        Task<LoginUserDTO> AddMangerBaseInfo(MyBlog_Manager_BaseInfo model,Guid id);
        Task<IEnumerable<string>> DelManager(IEnumerable<Guid> deld, Guid uid);
        Task<LoginUserDTO> BindDTO(MyBlog_Manager_BaseInfo user);
    }
}

﻿using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Infrastructure.Repository;
using System;
using System.Collections.Generic;

namespace TW.MyBlog.Domain.MainModule.Interface
{
    public interface IMyBlog_Manager_RoleRepository : IRepository<MyBlog_Manager_Role, EntityList<MyBlog_Manager_Role>>, IDependencyDynamicService
    {
       
    }
}
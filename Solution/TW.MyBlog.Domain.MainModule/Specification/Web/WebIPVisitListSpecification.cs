using System;
using System.Linq.Expressions;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model.DTO;

namespace TW.MyBlog.Domain.MainModule.Specification
{
    public class WebIPVisitListSpecification : Specification<WebIPVisitDTO>
    {
        //private string _name;

        public WebIPVisitListSpecification(
            //string name
            )
        {
            //_name = name;
        }
		/// <summary>
        /// 查询规约
        /// </summary>
        /// <returns></returns>
        public override Expression<Func<WebIPVisitDTO, bool>> SatisfiedBy()
        {
            var where = PredicateBuilder.True<WebIPVisitDTO>();
            //if (!string.IsNullOrEmpty(_name))
            //{
            //    where = where.And(x => x.name.Contains(_name));
            //}
            return where;
        }
    }
}

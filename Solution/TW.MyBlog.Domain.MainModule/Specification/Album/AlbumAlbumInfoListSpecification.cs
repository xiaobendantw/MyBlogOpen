using System;
using System.Linq.Expressions;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model.DTO;

namespace TW.MyBlog.Domain.MainModule.Specification
{
    public class AlbumAlbumInfoListSpecification : Specification<AlbumAlbumInfoDTO>
    {
        //private string _name;

        public AlbumAlbumInfoListSpecification(
            //string name
            )
        {
            //_name = name;
        }
		/// <summary>
        /// 查询规约
        /// </summary>
        /// <returns></returns>
        public override Expression<Func<AlbumAlbumInfoDTO, bool>> SatisfiedBy()
        {
            var where = PredicateBuilder.True<AlbumAlbumInfoDTO>();
            //if (!string.IsNullOrEmpty(_name))
            //{
            //    where = where.And(x => x.name.Contains(_name));
            //}
            return where;
        }
    }
}

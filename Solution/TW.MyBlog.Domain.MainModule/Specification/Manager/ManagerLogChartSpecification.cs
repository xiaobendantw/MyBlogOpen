﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Domain.MainModule.Specification
{
    public class ManagerLogChartSpecification : Specification<MyBlog_Manager_Log>
    {
        private DateTime? _searchActivateStartTime;
        private DateTime? _searchActivateEndTime;

        public ManagerLogChartSpecification(
            DateTime? searchActivateStartTime,
            DateTime? searchActivateEndTime
            )
        {
            _searchActivateStartTime = searchActivateStartTime;
            _searchActivateEndTime = searchActivateEndTime;
        }

        public override Expression<Func<MyBlog_Manager_Log, bool>> SatisfiedBy()
        {
            var where = PredicateBuilder.True<MyBlog_Manager_Log>();
            where = where.And(x => !x.IsDeleted);
            if (_searchActivateStartTime != null)
            {
                where = where.And(x => x.CreateTime >= _searchActivateStartTime);
            }
            if (_searchActivateEndTime != null)
            {
                _searchActivateEndTime = _searchActivateEndTime.Value.AddDays(1);
                where = where.And(x => x.CreateTime < _searchActivateEndTime);
            }
            return where;
        }
    }
}

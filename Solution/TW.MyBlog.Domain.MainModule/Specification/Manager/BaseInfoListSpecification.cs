﻿using System;
using System.Linq.Expressions;
using System.Linq;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Infrastructure.Common.Enums;

namespace Domain.MainModule.Specification
{
    public class BaseInfoListSpecification : Specification<MyBlog_Manager_BaseInfo>
    {

        private Enum_ManagerUserState? _state { get; set; }
        private Guid? _role { get; set; }
        private string _seachlogin { get; set; }
        public BaseInfoListSpecification(
        Enum_ManagerUserState? state, Guid? role, string sealogin
            )
        {
            _state = state;
            _role = role;
            _seachlogin = sealogin;
        }

        public override Expression<Func<MyBlog_Manager_BaseInfo, bool>> SatisfiedBy()
        {

            var where = PredicateBuilder.True<MyBlog_Manager_BaseInfo>();
            where = where.And(x => !x.IsDeleted);
            if (_state != null)
            {
                where = where.And(x => x.State == _state);
            }
            if (!string.IsNullOrEmpty(_seachlogin))
            {
                where = where.And(x => x.LoginName.Contains(_seachlogin));
            }
            if (_role!=null)
            {
                where = where.And(x => x.Roles.Select(a => a.Role.ID.ToString()).Contains(_role.ToString()));
            }
            return where;
        }
    }
}

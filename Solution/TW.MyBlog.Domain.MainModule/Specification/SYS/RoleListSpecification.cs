﻿using System;
using System.Linq.Expressions;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Domain.MainModule.Specification
{
    public class RoleListSpecification : Specification<MyBlog_SYS_Role>
    {
        //private string _name;

        public RoleListSpecification(
            //string name
            )
        {
            //_name = name;
        }

        public override Expression<Func<MyBlog_SYS_Role, bool>> SatisfiedBy()
        {
            var where = PredicateBuilder.True<MyBlog_SYS_Role>();
            where = where.And(x => !x.IsDeleted);
            //if (string.IsNullOrEmpty(_name))
            //{
            //    where = where.And(x => x.RoleName.Contains(_name));
            //}
            return where;
        }
    }
}

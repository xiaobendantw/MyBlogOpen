﻿using System;
using System.Linq.Expressions;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Domain.MainModule.Specification
{
    public class WeChatSettingListSpecification : Specification<MyBlog_SYS_WeChatSetting>
    {
        //private string _name;

        public WeChatSettingListSpecification(
            //string name
            )
        {
            //_name = name;
        }

        public override Expression<Func<MyBlog_SYS_WeChatSetting, bool>> SatisfiedBy()
        {
            var where = PredicateBuilder.True<MyBlog_SYS_WeChatSetting>();
            where = where.And(x => !x.IsDeleted);
            //if (string.IsNullOrEmpty(_name))
            //{
            //    where = where.And(x => x.RoleName.Contains(_name));
            //}
            return where;
        }
    }
}

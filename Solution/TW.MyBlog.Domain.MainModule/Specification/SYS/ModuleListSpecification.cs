﻿using System;
using System.Linq.Expressions;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Domain.MainModule.Specification
{
    public class ModuleListSpecification : Specification<MyBlog_SYS_Module>
    {
        private Guid _pid;

        public ModuleListSpecification(
            Guid pid
            )
        {
            _pid = pid;
        }

        public override Expression<Func<MyBlog_SYS_Module, bool>> SatisfiedBy()
        {
            var where = PredicateBuilder.True<MyBlog_SYS_Module>();
            where = where.And(x => !x.IsDeleted && x.PID== _pid);
            return where;
        }
    }
}

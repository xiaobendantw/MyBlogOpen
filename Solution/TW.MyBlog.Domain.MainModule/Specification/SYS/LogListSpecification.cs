﻿using System;
using System.Linq.Expressions;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Domain.MainModule.Specification
{
    public class LogListSpecification : Specification<MyBlog_Manager_Log>
    {
        private readonly DateTime? _searchStartCreateTime; 
        private DateTime? _searchEndCreateTime;
        private readonly string _searchText;

        public LogListSpecification(DateTime? searchStartCreateTime, DateTime? searchEndCreateTime, string searchText
            )
        {
            _searchStartCreateTime = searchStartCreateTime;
            _searchEndCreateTime = searchEndCreateTime;
            _searchText = searchText;
        }

        public override Expression<Func<MyBlog_Manager_Log, bool>> SatisfiedBy()
        {
            var where = PredicateBuilder.True<MyBlog_Manager_Log>();
            where = where.And(x => !x.IsDeleted);
            if (_searchStartCreateTime != null)
            {
                where = where.And(x => x.CreateTime >= _searchStartCreateTime);
            }
            if (_searchEndCreateTime != null)
            {
                _searchEndCreateTime = _searchEndCreateTime.Value.AddDays(1);
                where = where.And(x => x.CreateTime < _searchEndCreateTime);
            }
            if (!string.IsNullOrEmpty(_searchText))
            {
                where = where.And(x => x.UserBaseInfo.LoginName.Contains(_searchText) || x.Titles.Contains(_searchText) || x.Contents.Contains(_searchText));
            }
            return where;
        }
    }
}

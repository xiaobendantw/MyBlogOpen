using System;
using System.Linq.Expressions;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model.DTO;

namespace TW.MyBlog.Domain.MainModule.Specification
{
    public class PostPostImageRelationListSpecification : Specification<PostPostImageRelationDTO>
    {
        //private string _name;

        public PostPostImageRelationListSpecification(
            //string name
            )
        {
            //_name = name;
        }
		/// <summary>
        /// 查询规约
        /// </summary>
        /// <returns></returns>
        public override Expression<Func<PostPostImageRelationDTO, bool>> SatisfiedBy()
        {
            var where = PredicateBuilder.True<PostPostImageRelationDTO>();
            //if (!string.IsNullOrEmpty(_name))
            //{
            //    where = where.And(x => x.name.Contains(_name));
            //}
            return where;
        }
    }
}

using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.Utility;
namespace TW.MyBlog.Domain.MainModule
{
    public class MyBlog_Album_AlbumInfoDomainServices : IMyBlog_Album_AlbumInfoDomainServices
    {
        private readonly IMyBlog_Album_AlbumInfoRepository _repository;

        public MyBlog_Album_AlbumInfoDomainServices(IMyBlog_Album_AlbumInfoRepository repository)
        {
            this._repository = repository;
        }

		/// <summary>
        /// 添加/修改一个MyBlog_Album_AlbumInfo
        /// </summary>
        /// <param name="item">实体</param>
        /// <returns></returns>
		public async Task ModifyModel(MyBlog_Album_AlbumInfo item)
        {
            if (item.ID == Guid.Empty)
            {
				//当实体ID为Empty时添加一个实体
                item.ID = Guid.NewGuid();
                item.CreateTime = DateTime.Now;
                _repository.Add(item);
            }
            else
            {
			    //当实体ID不为Empty并且在存在时,则更新一个实体
				var old = await _repository.GetByConditionAsync(new DirectSpecification<MyBlog_Album_AlbumInfo>(x => x.ID == item.ID && !x.IsDeleted), true);
				if (old == null)
				{
					throw new CustomException("没有查找到该条记录,请重试!");
				}
                _repository.Update(item, x => x.ImageUrl, x => x.ImageDecription, x => x.IsOpen, x => x.ImageCategory);
            }
        }

		/// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task DelModelsById(IEnumerable<Guid> ids)
        {
            //此处进行相应逻辑规则校验
            _repository.LogicDelete(new DirectSpecification<MyBlog_Album_AlbumInfo>(x => ids.Contains(x.ID)));
        }
    }
}

﻿using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Domain.MainModule
{
    public class MyBlog_Manager_RoleDomainServices : IMyBlog_Manager_RoleDomainServices
    {
        private readonly IMyBlog_Manager_RoleRepository _repository;

        public MyBlog_Manager_RoleDomainServices(IMyBlog_Manager_RoleRepository repository)
        {
            this._repository = repository;
        }
    }
}

﻿using System;
using System.Web;
using Newtonsoft.Json;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO.Manager;
using TW.Utility;
using TW.Utility.Encrypt;

namespace TW.MyBlog.Domain.MainModule
{
    public class MyBlog_Manager_LogDomainServices : IMyBlog_Manager_LogDomainServices
    {
        private readonly IMyBlog_Manager_LogRepository _repository;

        public MyBlog_Manager_LogDomainServices(IMyBlog_Manager_LogRepository repository)
        {
            this._repository = repository;
        }

        public void Log(string title, string Contents, LoginUserDTO mydto = null)
        {
            var dto = mydto ?? JsonConvert.DeserializeObject<LoginUserDTO>(JsonConvert.SerializeObject(Common.ReadSimpleLoginUser()));
            if (dto.ID == Guid.Empty)
            {
                throw new Exception("登录过期,日志写入失败。请重新登录!");
            }
            _repository.Add(new MyBlog_Manager_Log()
            {
                Contents = Contents,
                CreateTime = DateTime.Now,
                ID = Guid.NewGuid(),
                IsDeleted = false,
                UId = dto.ID,
                IpAddress = IPHelper.getIPAddr(),
                Titles = title
            });
        }
    }
}

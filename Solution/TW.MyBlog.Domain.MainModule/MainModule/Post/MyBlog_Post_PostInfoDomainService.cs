using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.Utility;
namespace TW.MyBlog.Domain.MainModule
{
    public class MyBlog_Post_PostInfoDomainServices : IMyBlog_Post_PostInfoDomainServices
    {
        private readonly IMyBlog_Post_PostInfoRepository _repository;

        public MyBlog_Post_PostInfoDomainServices(IMyBlog_Post_PostInfoRepository repository)
        {
            this._repository = repository;
        }

		/// <summary>
        /// 添加/修改一个MyBlog_Post_PostInfo
        /// </summary>
        /// <param name="item">实体</param>
        /// <returns></returns>
		public async Task ModifyModel(MyBlog_Post_PostInfo item)
        {
            if (item.ID == Guid.Empty)
            {
				//当实体ID为Empty时添加一个实体
                item.ID = Guid.NewGuid();
                item.CreateTime = DateTime.Now;
                _repository.Add(item);
            }
            else
            {
			    //当实体ID不为Empty并且在存在时,则更新一个实体
				var old = await _repository.GetByConditionAsync(new DirectSpecification<MyBlog_Post_PostInfo>(x => x.ID == item.ID && !x.IsDeleted), true);
				if (old == null)
				{
					throw new CustomException("没有查找到该条记录,请重试!");
				}
                item.UpdateTime=DateTime.Now;
                item.ImageUrl = string.IsNullOrEmpty(item.ImageUrl) ? old.ImageUrl : item.ImageUrl;
                _repository.Update(item, x => x.Title, x => x.Summary, x => x.Content, 
                    x => x.CategoryId, x => x.Author, x => x.Source, x => x.Liinks, x => x.PubDate,
                    x => x.UpdateTime, x => x.Clicks, x => x.ImageUrl, x => x.IsComment, x => x.SeoKey, x => x.SeoDescrition,x=>x.ImgUrlLink,x=>x.IsSelect);
            }
        }

		/// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task DelModelsById(IEnumerable<Guid> ids)
        {
            //此处进行相应逻辑规则校验
            _repository.LogicDelete(new DirectSpecification<MyBlog_Post_PostInfo>(x => ids.Contains(x.ID)));
        }
    }
}

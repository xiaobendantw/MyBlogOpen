using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.Utility;
namespace TW.MyBlog.Domain.MainModule
{
    public class MyBlog_Reptilian_InfoDomainServices : IMyBlog_Reptilian_InfoDomainServices
    {
        private readonly IMyBlog_Reptilian_InfoRepository _repository;

        public MyBlog_Reptilian_InfoDomainServices(IMyBlog_Reptilian_InfoRepository repository)
        {
            this._repository = repository;
        }

		/// <summary>
        /// 添加/修改一个MyBlog_Reptilian_Info
        /// </summary>
        /// <param name="item">实体</param>
        /// <returns></returns>
		public async Task ModifyModel(MyBlog_Reptilian_Info item)
        {
            if (item.ID == Guid.Empty)
            {
				//当实体ID为Empty时添加一个实体
                item.ID = Guid.NewGuid();
                item.GrabTime = DateTime.Now;
                _repository.Add(item);
            }
            else
            {
			    //当实体ID不为Empty并且在存在时,则更新一个实体
				var old = await _repository.GetByConditionAsync(new DirectSpecification<MyBlog_Reptilian_Info>(x => x.ID == item.ID), true);
				if (old == null)
				{
					throw new CustomException("没有查找到该条记录,请重试!");
				}
                _repository.Update(item, x => x.ReptilianId, x => x.Content, x => x.Source, x => x.FieryDegree, x => x.CategoryName, x => x.GrabTime);
            }
        }

		/// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task DelModelsById(IEnumerable<Guid> ids)
        {
            //此处进行相应逻辑规则校验
            _repository.LogicDelete(new DirectSpecification<MyBlog_Reptilian_Info>(x => ids.Contains(x.ID)));
        }
    }
}

﻿using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Domain.MainModule
{
    public class MyBlog_SYS_RoleActionDomainServices : IMyBlog_SYS_RoleActionDomainServices
    {
        private readonly IMyBlog_SYS_RoleActionRepository _repository;

        public MyBlog_SYS_RoleActionDomainServices(IMyBlog_SYS_RoleActionRepository repository)
        {
            this._repository = repository;
        }
    }
}

﻿using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.Utility;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Common;
using TW.MyBlog.Infrastructure.Common.Enums;

namespace TW.MyBlog.Domain.MainModule
{
    public class MyBlog_SYS_PhoneMessageRecordDomainServices : IMyBlog_SYS_PhoneMessageRecordDomainServices
    {
        private readonly IMyBlog_SYS_PhoneMessageRecordRepository _repository;

        public MyBlog_SYS_PhoneMessageRecordDomainServices(IMyBlog_SYS_PhoneMessageRecordRepository repository)
        {
            this._repository = repository;
        }

        public async Task<string> SendSingleMessage(string telPhone, Enum_PhoneMessageSendType sendType, 
            Enum_PhoneMessageSendUserType sendUserType, string sendOpenId, string sendIpAddr, Guid? sendUserId)
        {
            //做短信有效性校验
            if (string.IsNullOrEmpty(telPhone))
            {
                throw new CustomException("手机号码无效,请重试");
            }
            if (string.IsNullOrEmpty(sendIpAddr))
            {
                throw new CustomException("IP地址无效,请重试");
            }

            var parms = new List<string>();
            switch (sendType)
            {
                case Enum_PhoneMessageSendType.DealerActivate:
                case Enum_PhoneMessageSendType.MemberInfoChange:
                    parms.Add(Common.GetRandom(6));
                    parms.Add(SystemParameterHelper.MsgPastTime.ToString());
                    break;
            }
            var maxLimit = SystemParameterHelper.MsgSingleDaySendMax;
            switch (sendUserType)
            {
                   case Enum_PhoneMessageSendUserType.Customer:
                    //如果是访客，则只能通过手机号来校验有效次数
                    if ((await _repository.GetCountAsync(
                            new DirectSpecification<MyBlog_SYS_PhoneMessageRecord>(
                                x => !x.IsDeleted && (x.SendIpAddr == sendIpAddr || x.SendPhoneNumber == telPhone) && x.SendType == sendType))) >= maxLimit)
                    {
                        throw new CustomException("您当天发送短信数量已经超出了系统最大限制,请明天再试!");
                    }
                    break;
                case Enum_PhoneMessageSendUserType.Manager:
                    //如果是后台用户，则需要校验XXX
                    if (sendUserId == null)
                    {
                        throw new CustomException("请先登录,再重试!");
                    }
                    break;
                case Enum_PhoneMessageSendUserType.WeChatMember:
                    //如果是微信会员，则进行openid校验
                    if (string.IsNullOrEmpty(sendOpenId))
                    {
                        throw new CustomException("检测到您的微信授权已过期,请关闭微信重试!");
                    }
                    if (sendUserId == null)
                    {
                        throw new CustomException("检测到您的微信授权已过期,请关闭微信重试!");
                    }
                    if ((await _repository.GetCountAsync(
                            new DirectSpecification<MyBlog_SYS_PhoneMessageRecord>(
                                x => !x.IsDeleted && (x.SendIpAddr == sendIpAddr || x.SendOpenId == sendOpenId) && x.SendType == sendType))) >= maxLimit)
                    {
                        throw new CustomException("您当天发送短信数量已经超出了系统最大限制,请明天再试!");
                    }
                    break;
            }
            var code = "";
            var contents = SystemParameterHelper.MsgSign;
            switch (sendType)
            {
                case Enum_PhoneMessageSendType.DealerActivate:
                    code = parms[0];
                    contents += string.Format(SystemParameterHelper.MsgDealerActivateContent, parms.ToArray());
                    break;
                case Enum_PhoneMessageSendType.MemberInfoChange:
                    code = parms[0];
                    contents += string.Format(SystemParameterHelper.MsgMemberInfoChange, parms.ToArray());
                    break;
            }
            var item = new MyBlog_SYS_PhoneMessageRecord();
            item.SendPhone(sendType, sendUserType, sendUserId, code, contents, telPhone, sendOpenId, sendIpAddr);
            _repository.Add(item);
            return contents;
        }
    }
}

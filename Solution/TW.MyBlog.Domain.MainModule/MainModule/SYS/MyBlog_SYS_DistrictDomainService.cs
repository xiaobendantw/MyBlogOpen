﻿using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using System;
namespace TW.MyBlog.Domain.MainModule
{
    public class MyBlog_SYS_DistrictDomainServices : IMyBlog_SYS_DistrictDomainServices
    {
        private readonly IMyBlog_SYS_DistrictRepository _repository;

        public MyBlog_SYS_DistrictDomainServices(IMyBlog_SYS_DistrictRepository repository)
        {
            this._repository = repository;
        }
        
    }
}

﻿using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using System;
namespace TW.MyBlog.Domain.MainModule
{
    public class MyBlog_SYS_ProvinceDomainServices : IMyBlog_SYS_ProvinceDomainServices
    {
        private readonly IMyBlog_SYS_ProvinceRepository _repository;

        public MyBlog_SYS_ProvinceDomainServices(IMyBlog_SYS_ProvinceRepository repository)
        {
            this._repository = repository;
        }
        
    }
}

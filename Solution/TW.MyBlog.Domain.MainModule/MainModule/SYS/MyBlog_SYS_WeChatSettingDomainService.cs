﻿using System;
using System.Threading.Tasks;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Domain.MainModule
{
    public class MyBlog_SYS_WeChatSettingDomainServices : IMyBlog_SYS_WeChatSettingDomainServices
    {
        private readonly IMyBlog_SYS_WeChatSettingRepository _repository;

        public MyBlog_SYS_WeChatSettingDomainServices(IMyBlog_SYS_WeChatSettingRepository repository)
        {
            this._repository = repository;
        }

        public void ModifyModel(MyBlog_SYS_WeChatSetting item)
        {
            if (item.ID == Guid.Empty)
            {
                item.ID = Guid.NewGuid();
                item.CreateTime = DateTime.Now;
                _repository.Add(item);
            }
            else
            {
                _repository.Update(item, x => x.WeChatName, x => x.WxChat_gID, x => x.Token, x => x.BindUrl, x => x.AppID, x => x.AppSecret);
            }
        }
    }
}

﻿using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using System;
namespace TW.MyBlog.Domain.MainModule
{
    public class MyBlog_SYS_CityDomainServices : IMyBlog_SYS_CityDomainServices
    {
        private readonly IMyBlog_SYS_CityRepository _repository;

        public MyBlog_SYS_CityDomainServices(IMyBlog_SYS_CityRepository repository)
        {
            this._repository = repository;
        }
        
    }
}

﻿using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Domain.MainModule
{
    public class MyBlog_SYS_ModuleActionDomainServices : IMyBlog_SYS_ModuleActionDomainServices
    {
        private readonly IMyBlog_SYS_ModuleActionRepository _repository;

        public MyBlog_SYS_ModuleActionDomainServices(IMyBlog_SYS_ModuleActionRepository repository)
        {
            this._repository = repository;
        }
    }
}

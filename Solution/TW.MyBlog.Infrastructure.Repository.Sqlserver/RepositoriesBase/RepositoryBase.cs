﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.EFWork;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase
{
    public abstract class RepositoryBase<T> : RepositoryBaseFramework<T, Entities>
        where T : EntityBase, new()
    {

        protected RepositoryBase(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }


    }
}

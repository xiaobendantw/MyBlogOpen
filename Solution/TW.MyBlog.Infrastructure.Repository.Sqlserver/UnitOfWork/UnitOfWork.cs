﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.EFWork;
using Microsoft.EntityFrameworkCore;

namespace TW.MyBlog.Infrastructure.Repository
{
    public class UnitOfWork : IUnitOfWorkFramework
    {
        private readonly IDatabaseFactory databaseFactory;
        private Entities dataContext;

        public UnitOfWork(IDatabaseFactory databaseFactory)
        {
            this.databaseFactory = databaseFactory;
        }

        public Entities DataContext
        {
            get { return dataContext ?? (dataContext = databaseFactory.Get()); }
        }

        public async Task CommitAsync()
        {
            try
            {
                await DataContext.CommitAsync();
            }
            catch (Exception e)
            {
                throw new Exception(e.GetBaseException().Message);
            }
        }

        public void Commit()
        {
            try
            {
                 DataContext.Commit();
            }
            catch (Exception e)
            {
                throw new Exception(e.GetBaseException().Message);
            }
        }
    }
}

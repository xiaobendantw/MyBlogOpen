﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO.Manager;
using TW.Utility;
using Microsoft.EntityFrameworkCore;

namespace TW.MyBlog.Infrastructure.Repository.Sqlserver.EFWork
{
    public class Entities : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var dbType = Convert.ToInt32(ReadConfig.ReadAppSetting("dbType"));
            switch (dbType)
            {
                case 0:
                    optionsBuilder.UseSqlServer(ReadConfig.ReadAppSetting("sqlserver"));
                    break;
                case 1:
                    optionsBuilder.UseMySql(ReadConfig.ReadAppSetting("mysql"));
                    break;
            }

        }
        #region 实体引用

        public DbSet<MyBlog_Manager_BaseInfo> MyBlog_Manager_BaseInfo { get; set; }
        public DbSet<MyBlog_SYS_Module> MyBlog_SYS_Module { get; set; }
        public DbSet<MyBlog_SYS_ModuleAction> MyBlog_SYS_ModuleAction { get; set; }
        public DbSet<MyBlog_SYS_Role> MyBlog_SYS_Role { get; set; }
        public DbSet<MyBlog_SYS_RoleAction> MyBlog_SYS_RoleAction { get; set; }
        public DbSet<MyBlog_Manager_Role> MyBlog_Manager_Role { get; set; }
        public DbSet<MyBlog_Manager_Log> MyBlog_Manager_Log { get; set; }
        public DbSet<MyBlog_SYS_WeChatSetting> MyBlog_SYS_WeChatSetting { get; set; }
        public DbSet<MyBlog_SYS_City> MyBlog_SYS_City { get; set; }
        public DbSet<MyBlog_SYS_District> MyBlog_SYS_District { get; set; }
        public DbSet<MyBlog_SYS_Province> MyBlog_SYS_Province { get; set; }
        public DbSet<MyBlog_SYS_PhoneMessageRecord> MyBlog_SYS_PhoneMessageRecord { get; set; }

        public DbSet<RecursiveModuleDTO> RecursiveModuleDTO { get; set; }

        public DbSet<MyBlog_Album_AlbumCategory> MyBlog_Album_AlbumCategory { get; set; }
        public DbSet<MyBlog_Album_AlbumInfo> MyBlog_Album_AlbumInfo { get; set; }
        public DbSet<MyBlog_Post_PostCategory> MyBlog_Post_PostCategory { get; set; }
        public DbSet<MyBlog_Post_PostComment> MyBlog_Post_PostComment { get; set; }
        public DbSet<MyBlog_Post_PostImageRelation> MyBlog_Post_PostImageRelation { get; set; }
        public DbSet<MyBlog_Post_PostInfo> MyBlog_Post_PostInfo { get; set; }
        public DbSet<MyBlog_User_UserInfo> MyBlog_User_UserInfo { get; set; }
        public DbSet<MyBlog_Web_IPVisit> MyBlog_Web_IPVisit { get; set; }
        public DbSet<MyBlog_Web_LiveMessage> MyBlog_Web_LiveMessage { get; set; }
        public DbSet<MyBlog_Web_WebSeting> MyBlog_Web_WebSeting { get; set; }
        public DbSet<MyBlog_Reptilian_Info> MyBlog_Reptilian_Info { get; set; }
        #endregion

        public virtual async Task CommitAsync()
        {
            await SaveChangesAsync();
        }
        public virtual void Commit()
        {
            SaveChanges();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            #region old code

            //指定实体字段保留小数位数
            //modelBuilder.Entity<Experience>().Property(a => a.dlongitude).HasPrecision(18, 6);
            //modelBuilder.Entity<Experience>().Property(a => a.dlatitude).HasPrecision(18, 6);

            //解除实体与数据库之间的映射
            modelBuilder.Entity<MyBlog_SYS_Province>().Ignore(p => p.PyProvinceName);
            modelBuilder.Entity<MyBlog_SYS_City>().Ignore(p => p.PyCityName);

            //系统管理员与管理员角色的多对多关联 主外键关联请手动按需添加
            //modelBuilder.Entity<MyBlog_SYS_ModuleAction>().HasOne(a => a.Module).WithMany(c => c.ModuleActions).HasForeignKey(c => c.ModuleID);  //1:N
            //modelBuilder.Entity<MyBlog_SYS_ModuleAction>().HasOne(o => o.Category).WithOne(o => o.Product);//1:1

            modelBuilder.Entity<MyBlog_Manager_Role>().HasOne(a => a.BaseInfo).WithMany(a => a.Roles).HasForeignKey(a => a.MemberId); //1:N
            modelBuilder.Entity<MyBlog_SYS_RoleAction>().HasOne(a => a.Role).WithMany(a => a.RoleActions).HasForeignKey(a => a.RoleID);//1:N
            modelBuilder.Entity<MyBlog_SYS_ModuleAction>().HasOne(a => a.Module).WithMany(c => c.ModuleActions).HasForeignKey(c => c.ModuleID); //1:N
            modelBuilder.Entity<MyBlog_Manager_Log>().HasOne(a => a.UserBaseInfo).WithMany(c => c.Logs).HasForeignKey(c => c.UId);  //1:N
            #endregion
        }

        //方便排查异常信息
        public override async Task<int> SaveChangesAsync(CancellationToken cancellationToken = default(CancellationToken))
        {
            try
            {
                var result = await base.SaveChangesAsync(cancellationToken);
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public override int SaveChanges()
        {
            try
            {
                var result = base.SaveChanges();
                return result;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}

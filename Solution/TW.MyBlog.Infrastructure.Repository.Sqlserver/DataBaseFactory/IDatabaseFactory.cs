﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.EFWork;

namespace TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory
{
    public interface IDatabaseFactory : IDatabaseFrameworkFactory<Entities>
    {

    }
}

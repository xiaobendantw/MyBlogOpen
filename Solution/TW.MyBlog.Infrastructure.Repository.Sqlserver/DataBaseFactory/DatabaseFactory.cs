﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.EFWork;
using Microsoft.EntityFrameworkCore;

namespace TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory
{
    /// <summary>
    /// 使用单列模式获取上下文,保证全局上下只有一个实例
    /// </summary>
    public class DatabaseFactory : IDatabaseFactory
    {
        private Entities dataContext;
        public Entities Get()
        {
            return dataContext ?? (dataContext = new Entities());
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    if (dataContext != null)
                        dataContext.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory
{
    public interface IDatabaseFrameworkFactory<out T> : IDisposable
        where T : DbContext, new()
    {
        T Get();
    }
}

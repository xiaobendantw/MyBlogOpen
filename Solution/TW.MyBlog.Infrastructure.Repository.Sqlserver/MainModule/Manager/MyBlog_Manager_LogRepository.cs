﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using TW.Utility;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;

namespace TW.MyBlog.Infrastructure.Repository.Sqlserver
{
    public class MyBlog_Manager_LogRepository : RepositoryBase<MyBlog_Manager_Log>, IMyBlog_Manager_LogRepository
    {
        public MyBlog_Manager_LogRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
		public async Task<EntityList<LogDTO>> GetLogList(int page, int rows, ISpecification<MyBlog_Manager_Log> specification,
            Dictionary<string, string> sort)
		{
            var query = (from a in dbset.Where(specification.SatisfiedBy())
                         join b in DataContext.MyBlog_Manager_BaseInfo on a.UId equals b.ID
		        select new LogDTO()
		        {
		            ID = a.ID,
		            CreateTime = a.CreateTime,
		            LoginName = b == null ? "系统" : b.LoginName,
                    Titles=a.Titles,
                    Contents = a.Contents,
                    IpAddress = a.IpAddress
		        });
            var result=await FindAllAsyncAsQuery<LogDTO>(DataSort(query, sort), page, rows);
            result.Data.ForEach(x => { x.CreateTimeChs = x.CreateTime.ToString("yyyy年MM月dd日 HH:mm:ss");
                                         x.Contents = TString.NoHTML(x.Contents);
            });

            return result;
        }

        public async Task<List<dynamic>> ManagerLogChart(ISpecification<MyBlog_Manager_Log> specification)
        {
            var query =await (from d in dbset.Where(specification.SatisfiedBy()) select d.CreateTime).ToListAsync();
            var result = new List<dynamic>();
            dynamic obj = new ExpandoObject();
            obj.Title = "系统日志统计";
            obj.data = TW.Utility.Common.GetRange(query, query.Any() ? query.Min(x => x) : DateTime.MinValue,
               query.Any() ? query.Max(x => x) : DateTime.MaxValue);
            result.Add(obj);
            return result;
        }
    }
}
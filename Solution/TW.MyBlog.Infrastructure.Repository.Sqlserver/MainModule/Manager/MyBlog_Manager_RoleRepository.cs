﻿using System;
using System.Collections.Generic;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using System.Linq;

namespace TW.MyBlog.Infrastructure.Repository.Sqlserver
{
    public class MyBlog_Manager_RoleRepository : RepositoryBase<MyBlog_Manager_Role>, IMyBlog_Manager_RoleRepository
    {
        public MyBlog_Manager_RoleRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

       
    }
}
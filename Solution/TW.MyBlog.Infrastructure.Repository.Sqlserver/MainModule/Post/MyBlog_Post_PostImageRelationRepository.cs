using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
namespace TW.MyBlog.Infrastructure.Repository.Sql
{
    public class MyBlog_Post_PostImageRelationRepository : RepositoryBase<MyBlog_Post_PostImageRelation>, IMyBlog_Post_PostImageRelationRepository
    {
        public MyBlog_Post_PostImageRelationRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

		/// <summary>
        /// 根据条件分页获取/导出一个List<PostPostImageRelationDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		public async Task<EntityList<PostPostImageRelationDTO>> GetPostImageRelationList(int page, int rows, ISpecification<PostPostImageRelationDTO> specification,
            Dictionary<string, string> sort, bool export)
		{
			var query = (from a in dbset.Where(x => !x.IsDeleted)
                select new PostPostImageRelationDTO()
                {
                    ID = a.ID,
                    PostId = a.PostId,
                    ImageUrl = a.ImageUrl,
                    CreateTime = a.CreateTime
                }).Where(specification.SatisfiedBy());
			var result = new EntityList<PostPostImageRelationDTO>();
			if (export)
            {
                result = new EntityList<PostPostImageRelationDTO>() { Data = await DataSort(query, sort).ToListAsync() };
            }
            else
            {
                result = await FindAllAsyncAsQuery<PostPostImageRelationDTO>(DataSort(query, sort), page, rows); 
            }
			
            return result;
		}
    }
}
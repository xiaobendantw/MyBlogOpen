using System;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.Utility;
using System.Dynamic;
using TW.Utility.Extensions;

namespace TW.MyBlog.Infrastructure.Repository.Sql
{
    public class MyBlog_Post_PostInfoRepository : RepositoryBase<MyBlog_Post_PostInfo>, IMyBlog_Post_PostInfoRepository
    {
        private readonly string _imgurl = ReadConfig.ReadAppSetting("ImageUploadUrl");
        private readonly Guid _guidempty = Guid.Empty;
        public MyBlog_Post_PostInfoRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

        /// <summary>
        /// 根据条件分页获取/导出一个List<PostPostInfoDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
        public async Task<EntityList<PostPostInfoDTO>> GetPostInfoList(int page, int rows, ISpecification<PostPostInfoDTO> specification,
            Dictionary<string, string> sort, bool export)
        {
            var query = (from a in dbset.Where(x => !x.IsDeleted && x.CategoryId != _guidempty)
                         join b in DataContext.MyBlog_Post_PostCategory on a.CategoryId equals b.ID into temp1
                         from b1 in temp1.DefaultIfEmpty()
                         select new PostPostInfoDTO()
                         {
                             ID = a.ID,
                             Title = a.Title,
                             CategoryId = a.CategoryId,
                             Author = a.Author,
                             Source = a.Source,
                             Liinks = a.Liinks,
                             PubDate = a.PubDate,
                             UpdateTime = a.UpdateTime,
                             Clicks = a.Clicks,
                             CategoryName = b1 == null ? "" : b1.CategoryNAme,
                             ImageUrl = a.ImageUrl,
                             IsComment = a.IsComment,
                             CreateTime = a.CreateTime,
                             ImageUrllink = a.ImgUrlLink
                         }).AsNoTracking().Where(specification.SatisfiedBy());
            var result = new EntityList<PostPostInfoDTO>();
            if (export)
            {
                result = new EntityList<PostPostInfoDTO>() { Data = await DataSort(query, sort).ToListAsync() };
            }
            else
            {
                result = await FindAllAsyncAsQuery<PostPostInfoDTO>(DataSort(query, sort), page, rows);
            }
            result.Data.ForEach(x =>
            {
                x.PubDateStr = x.PubDate?.ToString("yyyy-MM-dd");
                x.UpdateTimeStr = x.UpdateTime?.ToString("yyyy-MM-dd HH:mm");
                x.IsCommentStr = x.IsComment == null ? "" : x.IsComment == true ? "是" : "否";
                x.ImageUrl = string.IsNullOrEmpty(x.ImageUrl) ? string.IsNullOrEmpty(x.ImageUrllink) ? "javascript:;" : x.ImageUrllink : _imgurl + x.ImageUrl;
            });
            return result;
        }

        #region WebSite
        public dynamic GetPostList()
        {
            List<dynamic> result = new List<dynamic>();
            var query = (from a in dbset.Where(x => !x.IsDeleted && x.CategoryId != _guidempty)
                         join b in DataContext.MyBlog_Post_PostCategory on a.CategoryId equals b.ID into temp1
                         from b1 in temp1.DefaultIfEmpty()
                         select new
                         {
                             a.ID,
                             a.Title,
                             a.CategoryId,
                             a.Author,
                             a.Source,
                             a.Liinks,
                             a.CreateTime,
                             a.Clicks,
                             CategoryName = b1 == null ? "" : b1.CategoryNAme,
                             a.ImageUrl,
                             a.Summary,
                             a.ImgUrlLink
                         }).OrderByDescending(x => x.CreateTime).AsNoTracking().Take(10);
            query.ToList().ForEach(x =>
            {
                dynamic obj = new ExpandoObject();
                obj.id = x.ID;
                obj.title = x.Title.Length <= 10 ? x.Title : x.Title.Substring(0, 10) + "...";
                obj.sumary = x.Summary.Length <= 100 ? x.Summary : x.Summary.Substring(0, 100) + "...";
                obj.categoryId = x.CategoryId;
                obj.author = x.Author;
                obj.source = x.Source;
                obj.liinks = x.Liinks;
                obj.clicks = x.Clicks;
                obj.categoryName = x.CategoryName;
                obj.imageUrl = string.IsNullOrEmpty(x.ImageUrl) ? string.IsNullOrEmpty(x.ImgUrlLink) ? "javascript:;" : x.ImgUrlLink : _imgurl + x.ImageUrl;
                obj.time = x.CreateTime.ToDisplayTime();
                result.Add(obj);
            });
            return result;
        }

        /// <summary>
        /// 获取首页推荐博文列表
        /// </summary>
        /// <returns></returns>
        public dynamic GetSelectPostList(int pageIndex, int pageSize)
        {

            List<dynamic> result = new List<dynamic>();
            var query = (from a in dbset.Where(x => !x.IsDeleted && x.IsSelect && x.CategoryId != _guidempty)
                         select new
                         {
                             a.ID,
                             a.Title,
                             a.CategoryId,
                             a.CreateTime,
                             a.Summary,
                             a.Clicks,
                             a.ImageUrl,
                             a.ImgUrlLink
                         }).OrderByDescending(x => x.CreateTime).AsNoTracking().Skip((pageIndex - 1) * pageSize).Take(pageSize);
            query.ToList().ForEach(x =>
            {
                dynamic obj = new ExpandoObject();
                obj.id = x.ID;
                obj.title = x.Title.Length <= 12 ? x.Title : x.Title.Substring(0, 12) + "...";
                obj.categoryId = x.CategoryId;
                obj.summary = x.Summary.Length <= 160 ? x.Summary : x.Summary.Substring(0, 160) + "...";
                obj.imageUrl = string.IsNullOrEmpty(x.ImageUrl) ? string.IsNullOrEmpty(x.ImgUrlLink) ? "javascript:;" : x.ImgUrlLink : _imgurl + x.ImageUrl;
                obj.clicks = x.Clicks.ToString("##.###");
                result.Add(obj);
            });
            return result;
        }

        /// <summary>
        /// 获取首页热门博文列表
        /// </summary>
        /// <returns></returns>
        public dynamic GetHotPostList()
        {
            List<dynamic> result = new List<dynamic>();
            var query = (from a in dbset.Where(x => !x.IsDeleted && x.CategoryId != _guidempty)
                         select new
                         {
                             a.ID,
                             a.Title,
                             a.Clicks
                         }).OrderByDescending(x => x.Clicks).AsNoTracking().Take(12);

            return query.ToList();
        }

        /// <summary>
        /// 获取博文列表
        /// </summary>
        /// <param name="categoryId">分类编号</param>
        /// <param name="pageIndex">分页下标</param>
        /// <param name="pageSize">分页大小</param>
        /// <returns></returns>
        public dynamic GetPostLists(Guid categoryId, int pageIndex, int pageSize)
        {
            List<dynamic> result = new List<dynamic>();
            var query = (from a in dbset.Where(x => !x.IsDeleted && x.CategoryId == categoryId)
                         select new
                         {
                             a.ID,
                             a.Title,
                             a.Summary,
                             a.ImageUrl,
                             a.ImgUrlLink,
                             a.CreateTime
                         }).OrderByDescending(x => x.CreateTime).AsNoTracking().Skip((pageIndex - 1) * pageSize).Take(pageSize);
            query.ToList().ForEach(x =>
            {
                dynamic obj = new ExpandoObject();
                obj.id = x.ID;
                obj.title = x.Title.Length <= 24 ? x.Title : x.Title.Substring(0, 24);
                obj.imageUrl = string.IsNullOrEmpty(x.ImageUrl) ? string.IsNullOrEmpty(x.ImgUrlLink) ? "javascript:;" : x.ImgUrlLink : _imgurl + x.ImageUrl;
                obj.summary = x.Summary.Length <= 160 ? x.Summary : x.Summary.Substring(0, 160) + "...";
                obj.time = x.CreateTime.ToDisplayTime();
                result.Add(obj);
            });
            return result;
        }

      
        #endregion
    }
}
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
namespace TW.MyBlog.Infrastructure.Repository.Sql
{
    public class MyBlog_Post_PostCommentRepository : RepositoryBase<MyBlog_Post_PostComment>, IMyBlog_Post_PostCommentRepository
    {
        public MyBlog_Post_PostCommentRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

		/// <summary>
        /// 根据条件分页获取/导出一个List<PostPostCommentDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		public async Task<EntityList<PostPostCommentDTO>> GetPostCommentList(int page, int rows, ISpecification<PostPostCommentDTO> specification,
            Dictionary<string, string> sort, bool export)
		{
			var query = (from a in dbset.Where(x => !x.IsDeleted)
                select new PostPostCommentDTO()
                {
                    ID = a.ID,
                    PostId = a.PostId,
                    CommentId = a.CommentId,
                    Comment = a.Comment,
                    UserId = a.UserId,
                    CommentIp = a.CommentIp,
                    CreateTime = a.CreateTime
                }).Where(specification.SatisfiedBy());
			var result = new EntityList<PostPostCommentDTO>();
			if (export)
            {
                result = new EntityList<PostPostCommentDTO>() { Data = await DataSort(query, sort).ToListAsync() };
            }
            else
            {
                result = await FindAllAsyncAsQuery<PostPostCommentDTO>(DataSort(query, sort), page, rows); 
            }
			
            return result;
		}
    }
}
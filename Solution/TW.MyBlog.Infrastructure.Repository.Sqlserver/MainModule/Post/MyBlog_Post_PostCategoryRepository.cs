using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
namespace TW.MyBlog.Infrastructure.Repository.Sql
{
    public class MyBlog_Post_PostCategoryRepository : RepositoryBase<MyBlog_Post_PostCategory>, IMyBlog_Post_PostCategoryRepository
    {
        public MyBlog_Post_PostCategoryRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

        /// <summary>
        /// 根据条件分页获取/导出一个List<PostPostCategoryDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
        public async Task<EntityList<PostPostCategoryDTO>> GetPostCategoryList(int page, int rows, ISpecification<PostPostCategoryDTO> specification,
            Dictionary<string, string> sort, bool export)
        {
            var query = (from a in dbset.Where(x => !x.IsDeleted)
                         select new PostPostCategoryDTO()
                         {
                             ID = a.ID,
                             CategoryNAme = a.CategoryNAme,
                             FId = a.FId,
                             CreateTime = a.CreateTime,

                         }).Where(specification.SatisfiedBy());
            var result = new EntityList<PostPostCategoryDTO>();
            if (export)
            {
                result = new EntityList<PostPostCategoryDTO>() { Data = await DataSort(query, sort).ToListAsync() };
            }
            else
            {
                result = await FindAllAsyncAsQuery<PostPostCategoryDTO>(DataSort(query, sort), page, rows);
            }
            result.Data.ForEach(x =>
            {

            });
            return result;
        }

        public dynamic GetPostCategory()
        {
            List<dynamic> result = new List<dynamic>();
            var query = (from a in dbset.Where(x => !x.IsDeleted)
                         let b = (from o in DataContext.MyBlog_Post_PostInfo.Where(x => !x.IsDeleted && x.CategoryId == a.ID) select o).AsNoTracking()
                         select new
                         {
                             a.ID,
                             a.CategoryNAme,
                             a.CreateTime,
                             count = b.Count(),
                         }).OrderByDescending(x => x.CreateTime).AsNoTracking();
            query.ToList().ForEach(x =>
            {
                dynamic obj = new ExpandoObject();
                obj.id = x.ID;
                obj.name = x.CategoryNAme;
                obj.count = x.count;
                result.Add(obj);
            });
            return result;

        }
    }
}
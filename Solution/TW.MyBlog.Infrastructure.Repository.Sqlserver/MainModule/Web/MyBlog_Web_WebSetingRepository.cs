using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
namespace TW.MyBlog.Infrastructure.Repository.Sql
{
    public class MyBlog_Web_WebSetingRepository : RepositoryBase<MyBlog_Web_WebSeting>, IMyBlog_Web_WebSetingRepository
    {
        public MyBlog_Web_WebSetingRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

		/// <summary>
        /// 根据条件分页获取/导出一个List<WebWebSetingDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		public async Task<EntityList<WebWebSetingDTO>> GetWebSetingList(int page, int rows, ISpecification<WebWebSetingDTO> specification,
            Dictionary<string, string> sort, bool export)
		{
			var query = (from a in dbset.Where(x => !x.IsDeleted)
                select new WebWebSetingDTO()
                {
                    ID = a.ID,
                    Title = a.Title,
                    SummaryTitle = a.SummaryTitle,
                    Copyright = a.Copyright,
                    BackGroundImage = a.BackGroundImage,
                    BackGroudVideo = a.BackGroudVideo,
                    SeoKey = a.SeoKey,
                    SeoDecrtion = a.SeoDecrtion,
                    CreateTime = a.CreateTime
                }).Where(specification.SatisfiedBy());
			var result = new EntityList<WebWebSetingDTO>();
			if (export)
            {
                result = new EntityList<WebWebSetingDTO>() { Data = await DataSort(query, sort).ToListAsync() };
            }
            else
            {
                result = await FindAllAsyncAsQuery<WebWebSetingDTO>(DataSort(query, sort), page, rows); 
            }
			
            return result;
		}
    }
}
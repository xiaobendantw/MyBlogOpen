using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.Utility.Extensions;

namespace TW.MyBlog.Infrastructure.Repository.Sql
{
    public class MyBlog_Web_LiveMessageRepository : RepositoryBase<MyBlog_Web_LiveMessage>, IMyBlog_Web_LiveMessageRepository
    {
        public MyBlog_Web_LiveMessageRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

        /// <summary>
        /// 根据条件分页获取/导出一个List<WebLiveMessageDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
        public async Task<EntityList<WebLiveMessageDTO>> GetLiveMessageList(int page, int rows, ISpecification<WebLiveMessageDTO> specification,
            Dictionary<string, string> sort, bool export)
        {
            var query = (from a in dbset.Where(x => !x.IsDeleted)
                         select new WebLiveMessageDTO()
                         {
                             ID = a.ID,
                             UserId = a.UserId,
                             IP = a.IP,
                             Message = a.Message,
                             CreateTime = a.CreateTime
                         }).Where(specification.SatisfiedBy());
            var result = new EntityList<WebLiveMessageDTO>();
            if (export)
            {
                result = new EntityList<WebLiveMessageDTO>() { Data = await DataSort(query, sort).ToListAsync() };
            }
            else
            {
                result = await FindAllAsyncAsQuery<WebLiveMessageDTO>(DataSort(query, sort), page, rows);
            }

            return result;
        }

        public dynamic GetLiveMessage(int pageIndex, int pageSize)
        {
            List<dynamic> result = new List<dynamic>();
            var query = (from a in dbset.Where(x => !x.IsDeleted)
                         select new
                         {
                             a.ID,
                             a.Message,
                             a.CreateTime,
                         }).OrderByDescending(x => x.CreateTime).AsNoTracking().Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var count = (from a in dbset.Where(x => !x.IsDeleted) select a.ID).Count();
            query.ToList().ForEach(x =>
            {
                dynamic obj = new ExpandoObject();
                obj.id = x.ID;
                obj.message = x.Message;
                obj.time = x.CreateTime.ToDisplayTime2();
                obj.totalPage = count;
                result.Add(obj);
            });
            return result;
        }
    }
}
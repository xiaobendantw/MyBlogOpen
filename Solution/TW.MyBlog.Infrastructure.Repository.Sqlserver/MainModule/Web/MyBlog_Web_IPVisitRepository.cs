using System;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Infrastructure.Common.Enums;

namespace TW.MyBlog.Infrastructure.Repository.Sql
{
    public class MyBlog_Web_IPVisitRepository : RepositoryBase<MyBlog_Web_IPVisit>, IMyBlog_Web_IPVisitRepository
    {
        public MyBlog_Web_IPVisitRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

        /// <summary>
        /// 根据条件分页获取/导出一个List<WebIPVisitDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
        public async Task<EntityList<WebIPVisitDTO>> GetIPVisitList(int page, int rows, ISpecification<WebIPVisitDTO> specification,
            Dictionary<string, string> sort, bool export)
        {
            var query = (from a in dbset.Where(x => !x.IsDeleted)
                         select new WebIPVisitDTO()
                         {
                             ID = a.ID,
                             IP = a.IP,
                             CreateTime = a.CreateTime
                         }).Where(specification.SatisfiedBy());
            var result = new EntityList<WebIPVisitDTO>();
            if (export)
            {
                result = new EntityList<WebIPVisitDTO>() { Data = await DataSort(query, sort).ToListAsync() };
            }
            else
            {
                result = await FindAllAsyncAsQuery<WebIPVisitDTO>(DataSort(query, sort), page, rows);
            }

            return result;
        }

        public async Task<List<dynamic>> GetChartData(DateTime? searchStartCreateTime, DateTime? searchEndCreateTime, Enum_VisitType visitType)
        {
            var dbwhere = PredicateBuilder.True<MyBlog_Web_IPVisit>();
            dbwhere = dbwhere.And(x => !x.IsDeleted);
            if (searchStartCreateTime != null)
            {
                dbwhere = dbwhere.And(x => x.CreateTime >= searchStartCreateTime);
            }
            if (searchEndCreateTime != null)
            {
                searchEndCreateTime = searchEndCreateTime.Value.AddDays(1);
                dbwhere = dbwhere.And(x => x.CreateTime < searchEndCreateTime);
            }

            var result = new List<dynamic>();
            var query = await (from a in dbset.Where(x => x.VisitType == visitType).Where(dbwhere) select a).AsNoTracking().Select(x => x.CreateTime).ToListAsync();
            dynamic obj = new ExpandoObject();
            obj.Title = visitType == Enum_VisitType.Home ? "网站访问统计" : "博文点击统计";
            obj.data = TW.Utility.Common.GetRange(query, query.Any() ? query.Min(x => x) : DateTime.MinValue,
                query.Any() ? query.Max(x => x) : DateTime.MaxValue);
            result.Add(obj);
            return result;
        }
    }
}
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
namespace TW.MyBlog.Infrastructure.Repository.Sql
{
    public class MyBlog_User_UserInfoRepository : RepositoryBase<MyBlog_User_UserInfo>, IMyBlog_User_UserInfoRepository
    {
        public MyBlog_User_UserInfoRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

		/// <summary>
        /// 根据条件分页获取/导出一个List<UserUserInfoDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		public async Task<EntityList<UserUserInfoDTO>> GetUserInfoList(int page, int rows, ISpecification<UserUserInfoDTO> specification,
            Dictionary<string, string> sort, bool export)
		{
			var query = (from a in dbset.Where(x => !x.IsDeleted)
                select new UserUserInfoDTO()
                {
                    ID = a.ID,
                    Nickame = a.Nickame,
                    PawssWord = a.PawssWord,
                    QQThird = a.QQThird,
                    WeChatThird = a.WeChatThird,
                    LastLoginTime = a.LastLoginTime,
                    Ip = a.Ip,
                    LoginCount = a.LoginCount,
                    Email = a.Email,
                    Tel = a.Tel,
                    CreateTime = a.CreateTime
                }).Where(specification.SatisfiedBy());
			var result = new EntityList<UserUserInfoDTO>();
			if (export)
            {
                result = new EntityList<UserUserInfoDTO>() { Data = await DataSort(query, sort).ToListAsync() };
            }
            else
            {
                result = await FindAllAsyncAsQuery<UserUserInfoDTO>(DataSort(query, sort), page, rows); 
            }
			result.Data.ForEach(x =>
		    {
		        x.LastLoginTimeStr = x.LastLoginTime?.ToString("yyyy-MM-dd");
		    });
            return result;
		}
    }
}
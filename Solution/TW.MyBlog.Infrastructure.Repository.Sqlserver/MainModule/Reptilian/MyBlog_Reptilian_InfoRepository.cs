using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
namespace TW.MyBlog.Infrastructure.Repository.Sql
{
    public class MyBlog_Reptilian_InfoRepository : RepositoryBase<MyBlog_Reptilian_Info>, IMyBlog_Reptilian_InfoRepository
    {
        public MyBlog_Reptilian_InfoRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

        /// <summary>
        /// 根据条件分页获取/导出一个List<ReptilianInfoDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
        public async Task<EntityList<ReptilianInfoDTO>> GetInfoList(int page, int rows, ISpecification<ReptilianInfoDTO> specification,
            Dictionary<string, string> sort, bool export)
        {
            var query = (from a in dbset
                         select new ReptilianInfoDTO()
                         {
                             ID = a.ID,
                             ReptilianId = a.ReptilianId,
                             Content = a.Content,
                             Source = a.Source,
                             FieryDegree = a.FieryDegree,
                             CategoryName = a.CategoryName,
                             GrabTime = a.GrabTime
                         }).Where(specification.SatisfiedBy());
            var result = new EntityList<ReptilianInfoDTO>();
            if (export)
            {
                result = new EntityList<ReptilianInfoDTO>() { Data = await DataSort(query, sort).ToListAsync() };
            }
            else
            {
                result = await FindAllAsyncAsQuery<ReptilianInfoDTO>(DataSort(query, sort), page, rows);
            }
            result.Data.ForEach(x =>
            {
                x.GrabTimeStr = x.GrabTime.ToString("yyyy-MM-dd");
            });
            return result;
        }

        public dynamic GetBeautifulsentence(int pageIndex, int pageSize)
        {
            List<dynamic> result = new List<dynamic>();
            var query = (from a in dbset
                         select new
                         {
                             a.ID,
                             a.Content,
                             a.FieryDegree,
                             a.GrabTime,
                             a.Source,
                         }).OrderByDescending(x => x.GrabTime).AsNoTracking().Skip((pageIndex - 1) * pageSize).Take(pageSize);
            var index = 1;
            query.ToList().ForEach(x =>
            {
                dynamic obj = new ExpandoObject();
                obj.id = x.ID;
                obj.content = x.Content;
                obj.fieryDegree = x.FieryDegree;
                obj.grabTime = x.GrabTime;
                obj.source = x.Source;
                obj.isright = index % 2 == 0;
                result.Add(obj);
                index++;
            });
            return result;
        }
    }
}
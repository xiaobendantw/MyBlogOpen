﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using TW.MyBlog.Domain.Model.DTO.Manager;
using Microsoft.EntityFrameworkCore;

namespace TW.MyBlog.Infrastructure.Repository.Sqlserver
{
    public class MyBlog_SYS_RoleRepository : RepositoryBase<MyBlog_SYS_Role>, IMyBlog_SYS_RoleRepository
    {
        public MyBlog_SYS_RoleRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
        public async Task<EntityList<RoleDTO>> GetRoleList(int page, int rows, ISpecification<MyBlog_SYS_Role> specification,
            Dictionary<string, string> sort)
        {
            var query = (from a in dbset.Where(specification.SatisfiedBy())
                         select new RoleDTO()
                         {
                             ID = a.ID,
                             RoleName = a.RoleName,
                             Memo = a.Memo,
                             OrderID = a.OrderID
                         });
            return await FindAllAsyncAsQuery<RoleDTO>(DataSort(query, sort), page, rows);
        }

        public async Task<List<RoleListDTO>> LoadMangerRoleList(Guid id)
        {
            if (id == Guid.Empty)//Add
            {
                var allroes = (from roles in DataContext.MyBlog_SYS_Role
                    orderby roles.OrderID ascending
                    where !roles.IsDeleted
                    select new RoleListDTO()
                    {

                        ID = roles.ID,
                        RoleName = roles.RoleName,
                        Ischeck = false

                    });
                return (await allroes.ToListAsync());
            }
            else//Editor
            {
                var Roles =await (from mamager in DataContext.MyBlog_Manager_BaseInfo.Include(x => x.Roles).ThenInclude(x => x.BaseInfo).Where(x => x.ID == id) where !mamager.IsDeleted select mamager.Roles.Where(x=>!x.IsDeleted).Select(x => x.RoleId)).ToListAsync();
                var chkRole = Roles.Aggregate((a, b) => a.Concat(b));
                var allrole = (from role in dbset.Where(x => !x.IsDeleted)
                               select new RoleListDTO()
                               {
                                   ID = role.ID,
                                   RoleName = role.RoleName,
                                   Ischeck = chkRole.Count(x => x == role.ID) > 0
                               }).ToList();
                return allrole;
            }
        }

        public async Task<MyBlog_SYS_Role> GetRoleAndAction(Guid id)
        {
            return await (from a in dbset.Where(x => !x.IsDeleted && x.ID == id).Include(x => x.RoleActions)
                .ThenInclude(x => x.Role) select a).FirstOrDefaultAsync();
        }
    }
}
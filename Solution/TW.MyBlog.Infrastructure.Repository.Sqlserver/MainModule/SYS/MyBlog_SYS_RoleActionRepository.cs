﻿using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;

namespace TW.MyBlog.Infrastructure.Repository.Sqlserver
{
    public class MyBlog_SYS_RoleActionRepository : RepositoryBase<MyBlog_SYS_RoleAction>, IMyBlog_SYS_RoleActionRepository
    {
        public MyBlog_SYS_RoleActionRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
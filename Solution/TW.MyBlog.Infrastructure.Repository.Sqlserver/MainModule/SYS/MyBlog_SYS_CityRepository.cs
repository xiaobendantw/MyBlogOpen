﻿using System.Collections.Generic;
using System.Linq;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;

namespace TW.MyBlog.Infrastructure.Repository.Sqlserver
{
    public class MyBlog_SYS_CityRepository : RepositoryBase<MyBlog_SYS_City>, IMyBlog_SYS_CityRepository
    {
        public MyBlog_SYS_CityRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
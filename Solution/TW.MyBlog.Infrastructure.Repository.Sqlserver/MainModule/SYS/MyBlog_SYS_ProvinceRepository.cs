﻿using System.Collections.Generic;
using System.Linq;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;

namespace TW.MyBlog.Infrastructure.Repository.Sqlserver
{
    public class MyBlog_SYS_ProvinceRepository : RepositoryBase<MyBlog_SYS_Province>, IMyBlog_SYS_ProvinceRepository
    {
        public MyBlog_SYS_ProvinceRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
    }
}
﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO.Manager;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;

namespace TW.MyBlog.Infrastructure.Repository.Sqlserver
{
    public class MyBlog_SYS_ModuleActionRepository : RepositoryBase<MyBlog_SYS_ModuleAction>, IMyBlog_SYS_ModuleActionRepository
    {
        public MyBlog_SYS_ModuleActionRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }
        public async Task<List<LoginModultActionDTO>> GetModultAction()
        {
            var query = await (from a in dbset.Where(x => !x.IsDeleted)
                               join b in DataContext.MyBlog_SYS_Module.Where(x => !x.IsDeleted) on a.ModuleID equals b.ID
                               join c1 in DataContext.MyBlog_SYS_Module.DefaultIfEmpty() on b.PID equals c1.ID into tmp1
                               from c in tmp1.DefaultIfEmpty()
                               select new LoginModultActionDTO
                               {
                                   ActionUrl = a.ActionUrl,
                                   ShowEnum = a.ShowEnum,
                                   ActionCls = a.ActionCls,
                                   ActionFun = a.ActionFun,
                                   OrderID = a.OrderID,
                                   ActionName = a.ActionName,
                                   FatherModuleName = c == null ? "" : c.ModuleName,
                                   ModuleName = b.ModuleName,
                                   ModuleID = a.ModuleID,
                                   Weight = a.Weight
                               }).ToListAsync();
            return query;
        }
    }
}
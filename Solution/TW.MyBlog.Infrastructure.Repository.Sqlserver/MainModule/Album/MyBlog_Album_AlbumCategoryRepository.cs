using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.DataBaseFactory;
using TW.MyBlog.Infrastructure.Repository.Sqlserver.RepositoriesBase;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Base;
namespace TW.MyBlog.Infrastructure.Repository.Sql
{
    public class MyBlog_Album_AlbumCategoryRepository : RepositoryBase<MyBlog_Album_AlbumCategory>, IMyBlog_Album_AlbumCategoryRepository
    {
        public MyBlog_Album_AlbumCategoryRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {

        }

		/// <summary>
        /// 根据条件分页获取/导出一个List<AlbumAlbumCategoryDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="specification">规约条件</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		public async Task<EntityList<AlbumAlbumCategoryDTO>> GetAlbumCategoryList(int page, int rows, ISpecification<AlbumAlbumCategoryDTO> specification,
            Dictionary<string, string> sort, bool export)
		{
			var query = (from a in dbset.Where(x => !x.IsDeleted)
                select new AlbumAlbumCategoryDTO()
                {
                    ID = a.ID,
                    CategoryName = a.CategoryName,
                    CategoryDecrtion = a.CategoryDecrtion,
                    CreateTime = a.CreateTime
                }).Where(specification.SatisfiedBy());
			var result = new EntityList<AlbumAlbumCategoryDTO>();
			if (export)
            {
                result = new EntityList<AlbumAlbumCategoryDTO>() { Data = await DataSort(query, sort).ToListAsync() };
            }
            else
            {
                result = await FindAllAsyncAsQuery<AlbumAlbumCategoryDTO>(DataSort(query, sort), page, rows); 
            }
			
            return result;
		}
    }
}
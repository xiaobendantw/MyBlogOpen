﻿
var hasMoreData = true;
var myparams;
var mypageIndex;
var baseApi = "http://localhost:8002";
/*---------------获取分页数据 ------------*/
//url:服务器获取数据的地址
//params:需要提交到后台的数据 例如{pageSize:10,pageIndex:1,...}
//pageIndex: 需要获取的页索引
//container:装载获取后数据的容器 例如 $("#div1")
//event: 获取数据后需要执行的函数 如果不执行任何函数 可以不传值
function ajaxPageList(url, params, pageIndex, container, event) {
    mypageIndex = pageIndex;
    myparams = params;
    //var tmpToken = $.GetCookie(tokenName);
    //if (params == null || params == "") {
    //    params = { WxTempToken: tmpToken };
    //} else {
    //    params.WxTempToken = tmpToken;
    //}
    //if (tmpToken == null || tmpToken == "") {
    //    location.reload();//刷新页面;
    //}
    if (myparams["pageIndex"] && (myparams.pageIndex)) {
        myparams.pageIndex = pageIndex;
    } else {
        myparams['pageIndex'] = pageIndex;
    }
    $.ajax({
        url: baseApi + url,
        data: JSON.stringify(params),
        type: 'post',
        async: false,
        dataType: "json",
        beforeSend: function () { $(".load").show(); },
        success: function (data) {
            try {
                if (data.code != 0) {
                    location.href = "/error.html";
                } else {
                    if (data.data.length <= 0 && mypageIndex == 1) {
                        $(container).html("<div class=\"line-more-txt\">暂无数据</div>");
                    }
                    hasMoreData = data.data.length > 0;
                    if (event) {
                        //data = { hasMoreData: hasMoreData, data: data }
                        $.Callbacks().add(event).fire(data);
                    };
                }
               
            } catch (e) {

            }
        },
        complete: function () {
            $(".load").hide();
        }
    });
    //滑动滚动条
    $(window).scroll(function () {
        if (hasMoreData) {
            var bottomHeight = $(document).height() - $(window).height() - $(window).scrollTop();
            if (bottomHeight < 150) {
                if (myparams.pageIndex != pageIndex) {
                    pageIndex = myparams.pageIndex;
                    ajaxPageList(url, myparams, pageIndex, container, event);
                } else {
                    pageIndex++;
                }
                return;
            }
        }
    });
}

//调用实列
//$(function () {
//    var pageIndex = 1;
// 参数：url,parm,pageIndex,Element(容器)
//    ajaxPageList("/Home/GetAccordList", { pageIndex: 1, pageSize: 10 }, pageIndex, $("#AccordList"));

//});


//function ajaxPageMVVM(url, params, pageIndex, container, event) {
//    function viewModel() {
//        var self = this;
//        self.datas = ko.observableArray([]);
//    }
//    var model = new viewModel();
//    ko.applyBindings(model, container[0]);
//    if (params["pageIndex"] && (params.pageIndex)) {
//        params.pageIndex = pageIndex;
//    } else {
//        params['pageIndex'] = pageIndex;
//    }
//    $.getJSON(url, params, function (data) {
//        if (data.Data.length > 0) {
//            $(data.Data).each(function(n,item) {
//                model.datas.push(item);
//            });
//        }
//        hasMoreData = (data.TotalPage > params.pageIndex);
//        if (event)
//            $.Callbacks().add(event).fire();
//    });
//    $(window).scroll(function () {
//        if (hasMoreData) {
//            var bottomHeight = $(document).height() - $(window).height() - $(window).scrollTop();
//            if (bottomHeight < 150) {
//                pageIndex++;
//                ajaxPageMVVM(url, params, pageIndex, container, event);
//                return;
//            }
//        }
//    });
//}
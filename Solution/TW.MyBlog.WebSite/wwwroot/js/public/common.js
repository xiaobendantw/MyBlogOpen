﻿$(function () {
    //$("#header").load("/Distribution/Header");
    //$("#fotter").load("/Distribution/Fotter");
});
var baseApi = "http://localhost:8002";

/*---------------公共参数 开始 ------------*/
var hasMoreData = true;
var myparams;
var mypageIndex;
var errorhtml = "/error.html";
var tokenName = "WxTempToken";

/*---------------公共参数 结束 ------------*/

/*---------------分页方法 开始 ------------*/
//url:服务器获取数据的地址
//params:需要提交到后台的数据 例如{pageSize:10,pageIndex:1,...}
//pageIndex: 需要获取的页索引
//container:装载获取后数据的容器 例如 $("#div1")
//event: 获取数据后需要执行的函数 如果不执行任何函数 可以不传值
$.AjaxPageList = function (url, params, pageIndex, container, event) {
    mypageIndex = pageIndex;
    myparams = params;
    if (myparams["pageIndex"] && (myparams.pageIndex)) {
        myparams.pageIndex = pageIndex;
    } else {
        myparams['pageIndex'] = pageIndex;
    }
    $.ajax({
        url: url,
        data: myparams,
        type: 'post',
        async: false,
        beforeSend: function () { $(".load").show(); },
        success: function (data) {
            data = $.trim(data);
            hasMoreData = data.length > 0;
            if (pageIndex === 1) {
                container.html(!data ? "<div style='text-align:center;font-size:10px;color:#6c6c6c;margin-top:100px'>没有查询到数据</div>" : data);
            } else {
                container.append(data);
                if (event)
                    $.Callbacks().add(event).fire();
            }
        },
        complete: function () {
            $(".load").hide();
            if (event)
                $.Callbacks().add(event).fire();
        }
    });
}
/*---------------分页方法 结束 ------------*/

/*---------------通用ajax方法 开始 ------------*/

$.CommonAjax = function (api, data, successfn, errorfn) {
    $.ajax({
        url: baseApi + api,
        type: "post",
        data: JSON.stringify(data),
        dataType: "json",
        //contentType: "application/json",
        success: function (result) {
            if (result.code != 0) {
                console.log(result.message);
                location.href = errorhtml;
            }
            if (result.code == 0) {
                successfn(result);
            } else {
                if (typeof (errorfn) == "undefined") {
                    console.log(result);
                } else {
                    errorfn(result);
                }

            }
        },
        error: function (e) {
            location.href = errorhtml;//转到错误页面

        }
    });
};
$.CommonAjaxNotResult = function (api, data) {
    $.ajax({
        url: baseApi + api,
        type: "post",
        data: JSON.stringify(data),
        dataType: "json",
        success: function (result) {
        },
        error: function(e) {
            
        }
    });
};
/*---------------通用ajax方法 结束 ------------*/

/*---------------其他帮助类 开始 ------------*/

//写cookies
function setCookie(name, value) {
    var Days = 30;
    var exp = new Date();
    exp.setTime(exp.getTime() + Days * 24 * 60 * 60 * 1000);
    document.cookie = name + "=" + escape(value) + ";expires=" + exp.toGMTString();
}

//取cookies
function getCookie(c_name) {
    if (document.cookie.length > 0) {
        var c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            var c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) {
                c_end = document.cookie.length;
            }
            return unescape(document.cookie.substring(c_start, c_end));
        }
    }
    return "";
}
//移除cookies
function delCookie(name) {
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval = getCookie(name);
    if (cval != null)
        document.cookie = name + "=" + cval + ";expires=" + exp.toGMTString();
}
$.GetCookie = function (cName) {
    if (document.cookie.length > 0) {
        var cStart = document.cookie.indexOf(cName + "=");
        if (cStart != -1) {
            cStart = cStart + cName.length + 1;
            var cEnd = document.cookie.indexOf(";", cStart);
            if (cEnd == -1) {
                cEnd = document.cookie.length;
            }
            return unescape(document.cookie.substring(cStart, cEnd));
        }
    }
    return "";
}
//日期格式化
//// 例子： 
// (new Date()).Format("yyyy-MM-dd hh:mm:ss.S") ==> 2006-07-02 08:09:04.423 
// (new Date()).Format("yyyy-M-d h:m:s.S")      ==> 2006-7-2 8:9:4.18 
Date.prototype.Format = function (fmt) { //author: meizz 
    var o = {
        "M+": this.getMonth() + 1, //月份 
        "d+": this.getDate(), //日 
        "h+": this.getHours(), //小时 
        "m+": this.getMinutes(), //分 
        "s+": this.getSeconds(), //秒 
        "q+": Math.floor((this.getMonth() + 3) / 3), //季度 
        "S": this.getMilliseconds() //毫秒 
    };
    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
    for (var k in o)
        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
    return fmt;
}

/*---------------其他帮助类 结束 ------------*/

function getQueryString(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
    var r = window.location.search.substr(1).match(reg);
    if (r != null)
        return decodeURI(r[2]);
    return null;
}
//判断访问模式
if (/Android|webOS|iPhone|iPod|BlackBerry/i.test(navigator.userAgent)) {

} else {

}

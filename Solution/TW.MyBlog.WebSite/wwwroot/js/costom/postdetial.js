﻿$(function () {
    GetHotPostList();
    GetPostDetial();
});
//首页博文详情
function GetPostDetial() {
    var Id = getQueryString("Id");
    $.CommonAjax("/api/Client/v1/GetPostDetial", { id: Id }, function (result) {
        if (result.data == null) {
            location.href = "/";
        } else {
            var bindfund = new Vue({
                el: '.row',
                data: result.data,
                methods: {}
            });
        }
    });
}
//热门博文
function GetHotPostList() {
    var bindfund;
    $.CommonAjax("/api/Client/v1/GetHotPostList", {}, function (result) {
        if (!bindfund) {
            bindfund = new Vue({
                el: '#div24hourview',
                data: {
                    items: result.data
                },
                methods: {
                    getHref: function (val) {
                        return '/Home/PostDetial?Id=' + val;
                    },
                    getcategoryHref: function (val) {
                        return '';
                    },
                    addclick: function (val) {
                        AddClicks(val);//增加点击数
                    }
                }
            });
        } else {
            $(result.data).each(function (i, v) {
                bindfund.items.push(v);
            });
        }
    });
}
//增加点击数
function AddClicks(val) {
    $.CommonAjaxNotResult("/api/Client/v1/AddPostClicks", { id: val });
}
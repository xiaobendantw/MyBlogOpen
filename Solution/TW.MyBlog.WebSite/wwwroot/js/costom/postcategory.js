﻿
$(function () {
    var bindfund;
    $.CommonAjax("/api/Client/v1/GetPostCategory", {  }, function (result) {
        if (result.data == null) {
            location.href = "/";
        } else {
            if (!bindfund) {

                bindfund = new Vue({
                    el: '#lispic',
                    data: {
                        items: result.data
                    },
                    methods: {
                        getHref: function (val) {
                            return '/Home/PostList?categoryid=' + val;
                        }
                    }
                });
            } else {
                $(result.data).each(function (i, v) {
                    bindfund.items.push(v);
                });
            }
        }
    });
});
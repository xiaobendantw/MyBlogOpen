﻿$(function () {
    var bindfund;
    var categoryid = getQueryString("categoryid");
    ajaxPageList("/api/Client/v1/GetPostLists",
        { pageIndex: 1, pageSize: 7, categoryId: categoryid },
        1, "#articles", function (result) {
           
            if (!bindfund) {
            
                bindfund = new Vue({
                    el: '#articles',
                    data: {
                        items: result.data
                    },
                    methods: {
                        getHref: function (val) {
                            return '/Home/PostDetial?Id=' + val;
                        },
                        getTime: function (time) {
                            return new Date(time).Format("yyyy.MM.dd");
                        }
                    }
                });
            } else {
                $(result.data).each(function (i, v) {
                    bindfund.items.push(v);
                });
            }
        });
    GetHotPostList();
});


//热门博文
function GetHotPostList() {
    var bindfund;
    $.CommonAjax("/api/Client/v1/GetHotPostList", {}, function (result) {
        if (!bindfund) {
            bindfund = new Vue({
                el: '#div24hourview',
                data: {
                    items: result.data
                },
                methods: {
                    getHref: function (val) {
                        return '/Home/PostDetial?Id=' + val;
                    },
                    getcategoryHref: function (val) {
                        return '';
                    },
                    addclick: function (val) {
                        AddClicks(val);//增加点击数
                    }
                }
            });
        } else {
            $(result.data).each(function (i, v) {
                bindfund.items.push(v);
            });
        }
    });
}
//增加点击数
function AddClicks(val) {
    $.CommonAjaxNotResult("/api/Client/v1/AddPostClicks", { id: val });
}
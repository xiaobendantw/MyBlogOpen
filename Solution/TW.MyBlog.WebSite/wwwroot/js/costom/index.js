﻿$(function () {
    GetNewsPost();
    GetSelectPostList(1, 4, "#select_bloglist1");
    GetSelectPostList(2, 4, "#select_bloglist2");
    GetHotPostList();
    $("#weixin").click(function () {
        layer.open({
            type: 1,
            title: false,
            area: ['300px', '398px'],
            shadeClose: true, //点击遮罩关闭
            content: '<img src="/images/35.jpg" />'
        });
    });
    $("#wxgzh").click(function () {
        layer.open({
            type: 1,
            title: false,
            area: ['344px', '344px'],
            shadeClose: true, //点击遮罩关闭
            content: '<img src="/images/wxgzh.jpg" />'
        });
    });
});
//首页博文
function GetNewsPost() {
    var bindfund;
    $.CommonAjax("/api/Client/v1/GetNewPostList", {}, function (result) {
        if (!bindfund) {
            bindfund = new Vue({
                el: '#articles',
                data: {
                    items: result.data
                },
                methods: {
                    getHref: function (val) {
                        return '/Home/PostDetial?Id=' + val;//跳转到详情
                    },
                    getcategoryHref: function (val) {
                        return '/Home/PostList?categoryid=' + val;
                    },
                    addclick: function (val) {
                        AddClicks(val);//增加点击数
                    }
                }
            });
        } else {
            $(result.data).each(function (i, v) {
                bindfund.items.push(v);
            });
        }
    });
}
//精选博文
function GetSelectPostList(pageIndex, pageSize, contaier) {
    var bindfund;
    $.CommonAjax("/api/Client/v1/GetSelectPostList", { pageIndex: pageIndex, pageSize: pageSize }, function (result) {
        if (!bindfund) {
            bindfund = new Vue({
                el: "" + contaier + "",
                data: {
                    items: result.data
                },
                methods: {
                    getHref: function (val) {
                        return '/Home/PostDetial?Id=' + val;
                    },
                    getcategoryHref: function (val) {
                        return '/Home/PostList?categoryid=' + val;
                    },
                    addclick: function (val) {
                        AddClicks(val);//增加点击数
                    }
                }
            });
        } else {
            $(result.data).each(function (i, v) {
                bindfund.items.push(v);
            });
        }
    });
}
//热门博文
function GetHotPostList() {
    var bindfund;
    $.CommonAjax("/api/Client/v1/GetHotPostList", {}, function (result) {
        if (!bindfund) {
            bindfund = new Vue({
                el: '#div24hourview',
                data: {
                    items: result.data
                },
                methods: {
                    getHref: function (val) {
                        return '/Home/PostDetial?Id=' + val;
                    },
                    getcategoryHref: function (val) {
                        return '';
                    },
                    addclick: function (val) {
                        AddClicks(val);//增加点击数
                    }
                }
            });
        } else {
            $(result.data).each(function (i, v) {
                bindfund.items.push(v);
            });
        }
    });
}
//增加点击数
function AddClicks(val) {
    $.CommonAjaxNotResult("/api/Client/v1/AddPostClicks", { id: val });
}
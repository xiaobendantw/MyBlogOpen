﻿$(function () {
    var bindfund;
    ajaxPageList("/api/Client/v1/GetBeautifulsentence",
        { pageIndex: 1, pageSize: 8 }, 1, "#sentencelist", function (result) {
            if (!bindfund) {
                bindfund = new Vue({
                    el: '#sentencelist',
                    data: {
                        items: result.data
                    },
                    methods: {
                       
                    }
                });
            } else {
                $(result.data).each(function (i, v) {
                    bindfund.items.push(v);
                });
            }
        });
});
﻿$(function () {
    GetPostDetial();
   
});
//个人简介
function GetPostDetial() {
    var Id = "FF6480D7-46D2-406C-8BBC-21BF642CAFEB";
    $.CommonAjax("/api/Client/v1/GetPostDetial", { id: Id }, function (result) {
        if (result.data == null) {
            location.href = "/";
        } else {
            var bindfund = new Vue({
                el: '.about_box',
                data: result.data,
                methods: {}
            });
        }
        //GetHotPostList();
    });
}
//热门博文
function GetHotPostList() {
    var bindfund;
    $.CommonAjax("/api/Client/v1/GetHotPostList", {}, function (result) {
        if (!bindfund) {
            bindfund = new Vue({
                el: '#div24hourview',
                data: {
                    items: result.data
                },
                methods: {
                    getHref: function (val) {
                        return '/Home/PostDetial?Id=' + val;
                    },
                    getcategoryHref: function (val) {
                        return '';
                    },
                    addclick: function (val) {
                        AddClicks(val);//增加点击数
                    }
                }
            });
        } else {
            $(result.data).each(function (i, v) {
                bindfund.items.push(v);
            });
        }
    });
}
//增加点击数
function AddClicks(val) {
    $.CommonAjaxNotResult("/api/Client/v1/AddPostClicks", { id: val });
}
﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TW.MyBlog.WebSite.Models;

namespace TW.MyBlog.WebSite.Controllers
{
    public class HomeController : Controller
    {
        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        public IActionResult Index()
        {
            return View();
        }
       /// <summary>
       /// 详情
       /// </summary>
       /// <returns></returns>
        public IActionResult PostDetial()
        {
            return View();
        }
        /// <summary>
        /// 文章列表
        /// </summary>
        /// <returns></returns>
        public IActionResult PostList()
        {
            return View();
        }
        /// <summary>
        /// 相册
        /// </summary>
        /// <returns></returns>
        public IActionResult ImageShow()
        {
            return View();
        }
        /// <summary>
        /// 简介
        /// </summary>
        /// <returns></returns>
        public IActionResult Profile()
        {
            return View();
        }
        /// <summary>
        /// 博文分类
        /// </summary>
        /// <returns></returns>
        public IActionResult PostCategory()
        {
            return View();
        }
        /// <summary>
        /// 我的日志
        /// </summary>
        /// <returns></returns>
        public IActionResult LogList()
        {
            return View();
        }
        /// <summary>
        /// 留言
        /// </summary>
        /// <returns></returns>
        public IActionResult Message()
        {
            return View();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Utility;

namespace TW.MyBlog.Infrastructure.Common.Enums
{
    public enum Enum_PhoneMessageSendType
    {
        /// <summary>
        /// 会员完善资料
        /// </summary>
        [Description("会员完善资料")]
        MemberInfoChange = 0,

        /// <summary>
        /// 经销商激活
        /// </summary>
        [Description("经销商激活")]
        DealerActivate = 1
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TW.MyBlog.Infrastructure.Common.Enums
{
    /// <summary>
    /// 统计图类型
    /// </summary>
    public enum Enum_ChartsType
    {
        /// <summary>
        /// 柱状图
        /// </summary>
        bar = 0,
        /// <summary>
        /// 饼状图
        /// </summary>
        pie = 1,
        /// <summary>
        /// 曲线图
        /// </summary>
        line = 2
    }
}

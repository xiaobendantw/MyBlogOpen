﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.Utility;

namespace TW.MyBlog.Infrastructure.Common.Enums
{
    public enum Enum_PhoneMessageSendUserType
    {

        /// <summary>
        /// 访问者
        /// </summary>
        [Description("访问者")]
        Customer = 0,
        /// <summary>
        /// 微信会员
        /// </summary>
        [Description("微信会员")]
        WeChatMember = 1,
        /// <summary>
        /// 后台用户
        /// </summary>
        [Description("后台用户")]
        Manager = 2,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TW.MyBlog.Infrastructure.Common.Enums
{
    public  enum Enum_VisitType
    {
        /// <summary>
        /// 首页访问
        /// </summary>
        Home,
        /// <summary>
        /// 博文点击
        /// </summary>
        Blog
    }
}

﻿using System.ComponentModel;
using TW.MyBlog.Infrastructure.Common;

using TW.Utility;
namespace TW.MyBlog.Infrastructure.Common.Enums
{
    public enum Enum_ManagerUserState
    {
        /// <summary>
        /// 正常
        /// </summary>
        [Description("正常")]
        Normal = 0,
        /// <summary>
        /// 锁定
        /// </summary>
        [Description("锁定")]
        Locked = 1
    }
}

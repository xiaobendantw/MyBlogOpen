﻿using TW.Utility;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Linq;

namespace TW.MyBlog.Infrastructure.Common
{
    public static class SystemParameterHelper
    {

        private static string GetValueFromXml(string elementName)
        {
            var filepath = AppDomain.CurrentDomain.BaseDirectory + @"/Config/Parameter.config";
            string result;
            var cache =new DataCache().GetCache<string>("SystemParam_ConfigFile_" + elementName);
            if (cache == null)
            {
                XElement xml = XElement.Load(filepath);
                result = xml.Element(elementName).Value;
                new DataCache().SetCache("SystemParam_ConfigFile_" + elementName, result);
            }
            else
                result = cache.ToString();
            return result;
        }

        /// <summary>
        /// 获取微信AppId
        /// </summary>
        public static string WeChatAppId => GetValueFromXml("WeChatAppId");

        /// <summary>
        /// 获取微信AppSecret
        /// </summary>
        public static string WeChatAppSecret => GetValueFromXml("WeChatAppSecret");
        /// <summary>
        /// 发送模板消息URL
        /// </summary>
        public static string SendMessageApi => GetValueFromXml("SendMessageApi");
        /// <summary>
        /// 获取基本信息URL
        /// </summary>
        public static string GetBaseUserInfoApi => GetValueFromXml("GetBaseUserInfoApi");
        /// <summary>
        /// 模板消息编号
        /// </summary>
        public static string TemplateIdCollarNotice => GetValueFromXml("TemplateIdCollarNotice");
        /// <summary>
        /// 手机验证码有效时间(分钟)
        /// </summary>
        public static int MsgPastTime => Convert.ToInt32(GetValueFromXml("MsgPastTime"));
        /// <summary>
        /// 单日限制
        /// </summary>
        public static int MsgSingleDaySendMax => Convert.ToInt32(GetValueFromXml("MsgSingleDaySendMax"));

        /// <summary>
        /// 短信签名
        /// </summary>
        public static string MsgSign => GetValueFromXml("MsgSign");

        #region 手机验证码内容
        /// <summary>
        /// 会员完善资料
        /// </summary>
        public static string MsgMemberInfoChange => GetValueFromXml("MsgMemberInfoChangeContent");
        /// <summary>
        /// 经销商激活
        /// </summary>
        public static string MsgDealerActivateContent => GetValueFromXml("MsgDealerActivateContent");
        #endregion
    }
}

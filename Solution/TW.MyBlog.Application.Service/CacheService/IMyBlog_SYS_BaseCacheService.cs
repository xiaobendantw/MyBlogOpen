﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model.DTO.Manager;

namespace TW.MyBlog.Application.Service.CacheService
{
    public interface IMyBlog_SYS_BaseCacheService : IBaseCacheService, IDependencyDynamicService
    {
        void CacheLogin(LoginUserDTO dto);

        void RemoveLogin(IEnumerable<Guid> deld);

        List<LoginUserDTO> GetAllLogin();

        Task UpdateLogin(LoginUserDTO dto, int type);
    }
}

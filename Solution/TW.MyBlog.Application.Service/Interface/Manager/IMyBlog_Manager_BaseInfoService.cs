﻿using System;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Domain.Base;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using TW.MyBlog.Infrastructure.Common.Enums;

namespace TW.MyBlog.Application.Service.Interface
{
    public interface IMyBlog_Manager_BaseInfoServices : IBaseServices<MyBlog_Manager_BaseInfo>, IDependencyDynamicService
    {
        Task CheckLogin(string loginName,string passWord,bool remmberMe, bool? pwdIsMd5 = false, string validateCode = null);
        Task ChangeUserInfo(Guid ID, string nickName, string newPassword, string confirmPassword,int UserChangeType, string oldpassword);
        Task<EntityList<BaseInfoDTO>> GetManagerList(int page, int rows, Dictionary<string, string> sort, Enum_ManagerUserState? State,Guid? Role, string sealogin);
        Task AddMangerBaseInfo(MyBlog_Manager_BaseInfo model,Guid id) ;

        Task DelManager(IEnumerable<Guid> deld , Guid uid);
    }
}
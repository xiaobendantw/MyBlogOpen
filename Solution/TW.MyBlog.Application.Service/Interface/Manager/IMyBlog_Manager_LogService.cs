﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Domain.Model.DTO.Manager;

namespace TW.MyBlog.Application.Service.Interface
{
    public interface IMyBlog_Manager_LogServices : IBaseServices<MyBlog_Manager_Log>, IDependencyDynamicService
    {
		Task<EntityList<LogDTO>> GetLogList(int page, int rows, Dictionary<string, string> sort, DateTime? searchStartCreateTime, DateTime? searchEndCreateTime, string searchText);
        Task LogAsync(string title, string Contents, bool isBulk = false, LoginUserDTO mydto = null);
        Task<List<dynamic>> ManagerLogChart(DateTime? searchStartCreateTime, DateTime? searchEndCreateTime);
    }
}
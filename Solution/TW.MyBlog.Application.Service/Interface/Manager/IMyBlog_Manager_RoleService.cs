﻿using System;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Model;
using System.Collections.Generic;
using TW.MyBlog.Domain.Base;

namespace TW.MyBlog.Application.Service.Interface
{
    public interface IMyBlog_Manager_RoleServices : IBaseServices<MyBlog_Manager_Role>, IDependencyDynamicService
    {
       
    }
}
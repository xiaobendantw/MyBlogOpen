using System;
using System.Collections.Generic;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common.Enums;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
namespace TW.MyBlog.Application.Service.Interface
{
    public interface IMyBlog_Album_AlbumInfoServices : IBaseServices<MyBlog_Album_AlbumInfo>, IDependencyDynamicService
    {	
	    /// <summary>
        /// 根据条件分页获取/导出一个List<AlbumAlbumInfoDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		Task<EntityList<AlbumAlbumInfoDTO>> GetAlbumInfoList(int page, int rows, Dictionary<string, string> sort, bool export);

		/// <summary>
        /// 添加/修改一个MyBlog_Album_AlbumInfo
        /// </summary>
        /// <param name="item">实体</param>
        /// <returns></returns>
		Task ModifyModel(MyBlog_Album_AlbumInfo item);

		/// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
		Task DelModelsById(IEnumerable<Guid> ids);
    }
}
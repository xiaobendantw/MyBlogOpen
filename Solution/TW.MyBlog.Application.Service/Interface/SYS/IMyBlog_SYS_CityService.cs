﻿using System.Collections.Generic;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;

namespace TW.MyBlog.Application.Service.Interface
{
    public interface IMyBlog_SYS_CityServices : IBaseServices<MyBlog_SYS_City>, IDependencyDynamicService
    {
    }
}
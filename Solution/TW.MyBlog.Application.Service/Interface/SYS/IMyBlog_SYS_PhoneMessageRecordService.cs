﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common.Enums;

namespace TW.MyBlog.Application.Service.Interface
{
    public interface IMyBlog_SYS_PhoneMessageRecordServices : IBaseServices<MyBlog_SYS_PhoneMessageRecord>, IDependencyDynamicService
    {
        Task SendSingleMessage(string telPhone, Enum_PhoneMessageSendType sendType, string sendOpenId, Guid? sendUserId);
    }
}
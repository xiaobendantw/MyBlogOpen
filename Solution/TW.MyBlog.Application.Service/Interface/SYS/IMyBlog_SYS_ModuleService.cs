﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO.Manager;

namespace TW.MyBlog.Application.Service.Interface
{
    public interface IMyBlog_SYS_ModuleServices : IBaseServices<MyBlog_SYS_Module>, IDependencyDynamicService
    {
        Task AddModule(MyBlog_SYS_Module item);
        Task<dynamic> GetFatherModule(Guid fatherId, Guid itemId);
        Task<List<MenuModuleListDTO>> LoadModuleList(Guid RoleId);
        Task<EntityList<ModuleDTO>> GetModuleList(int page, int rows,Guid pid, Dictionary<string, string> sort);

        Task DelModule(IEnumerable<Guid> delId);
    }
}
﻿using System.Collections.Generic;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using System;
using System.Threading.Tasks;
using TW.MyBlog.Domain.Model.DTO.Manager;

namespace TW.MyBlog.Application.Service.Interface
{
    public interface IMyBlog_SYS_RoleServices : IBaseServices<MyBlog_SYS_Role>, IDependencyDynamicService
    {
        Task<EntityList<RoleDTO>> GetRoleList(int page, int rows, Dictionary<string, string> sort);
        Task AddRole(MyBlog_SYS_Role model, List<MyBlog_SYS_ModuleAction> moduleAction);
        Task<List<RoleListDTO>> LoadMangerRoleList(Guid id);
        Task DelRole(IEnumerable<Guid> ids);
    }
}
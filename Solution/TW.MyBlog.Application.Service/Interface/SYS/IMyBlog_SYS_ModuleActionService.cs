﻿using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Application.Service.Interface
{
    public interface IMyBlog_SYS_ModuleActionServices : IBaseServices<MyBlog_SYS_ModuleAction>, IDependencyDynamicService
    {

    }
}
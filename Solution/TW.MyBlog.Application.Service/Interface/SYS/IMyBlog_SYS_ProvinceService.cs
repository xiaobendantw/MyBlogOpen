﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;

namespace TW.MyBlog.Application.Service.Interface
{
    public interface IMyBlog_SYS_ProvinceServices : IBaseServices<MyBlog_SYS_Province>, IDependencyDynamicService
    {

        /// <summary>
        /// 从数据库加载省市列表存入缓存
        /// </summary>
        /// <returns></returns>
        Task<Tuple<List<MyBlog_SYS_Province>, List<MyBlog_SYS_City>>> LoadProvinceandCity();
    }
}
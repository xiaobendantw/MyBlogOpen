﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;

namespace TW.MyBlog.Application.Service.Interface
{
    public interface IMyBlog_SYS_WeChatSettingServices : IBaseServices<MyBlog_SYS_WeChatSetting>, IDependencyDynamicService
    {
        Task ModifyModel(MyBlog_SYS_WeChatSetting item);
    }
}
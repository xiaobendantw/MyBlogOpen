using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.MainModule.Specification;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.Utility;
namespace TW.MyBlog.Application.Service
{
    public class MyBlog_Post_PostCategoryServices : BaseServices<MyBlog_Post_PostCategory>, IMyBlog_Post_PostCategoryServices
    {
        private readonly IMyBlog_Post_PostCategoryDomainServices _domainServices;
        private readonly IMyBlog_Post_PostCategoryRepository _repository;
        private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_Post_PostCategoryServices(IUnitOfWorkFramework unitOfWork, IMyBlog_Post_PostCategoryRepository repository, IMyBlog_Post_PostCategoryDomainServices domainService
        , IMyBlog_Manager_LogServices logServices
        )
            : base(unitOfWork, repository)
        {
            this._domainServices = domainService;
            this._repository = repository;
            _logServices = logServices;
        }

        /// <summary>
        /// 根据条件分页获取/导出一个List<PostPostCategoryDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
        public async Task<EntityList<PostPostCategoryDTO>> GetPostCategoryList(int page, int rows, Dictionary<string, string> sort, bool export)
        {
            return await _repository.GetPostCategoryList(page, rows, new PostPostCategoryListSpecification(), sort, export);
        }

        /// <summary>
        /// 调用领域服务添加/修改MyBlog_Post_PostCategory
        /// </summary>
        /// <param name="item">实体</param>
        /// <returns></returns>
        public async Task ModifyModel(MyBlog_Post_PostCategory item)
        {
            var ID = item.ID;
            await _domainServices.ModifyModel(item);
            await _logServices.LogAsync((ID == Guid.Empty ? "添加" : "修改") + "功能名称", $"CategoryNAme=[{item.CategoryNAme}],IsDeletd=[{item.IsDeleted}]");
        }

        /// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task DelModelsById(IEnumerable<Guid> ids)
        {
            await _domainServices.DelModelsById(ids);
            await _logServices.LogAsync("删除功能名称", "编号:[" + string.Join(",", ids) + "]");
        }

        #region API
        /// <summary>
        /// 获取博文分类
        /// </summary>
        /// <returns></returns>
        public dynamic GetPostCategory()
        {
            return _repository.GetPostCategory();
        }


        #endregion
    }
}
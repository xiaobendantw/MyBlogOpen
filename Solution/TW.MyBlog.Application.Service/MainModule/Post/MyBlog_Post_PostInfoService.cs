using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.MainModule.Specification;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.Utility;
namespace TW.MyBlog.Application.Service
{
    public class MyBlog_Post_PostInfoServices : BaseServices<MyBlog_Post_PostInfo>, IMyBlog_Post_PostInfoServices
    {
        private readonly IMyBlog_Post_PostInfoDomainServices _domainServices;
        private readonly IMyBlog_Post_PostInfoRepository _repository;
        private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_Post_PostInfoServices(IUnitOfWorkFramework unitOfWork, IMyBlog_Post_PostInfoRepository repository, IMyBlog_Post_PostInfoDomainServices domainService
        , IMyBlog_Manager_LogServices logServices
        )
            : base(unitOfWork, repository)
        {
            this._domainServices = domainService;
            this._repository = repository;
            _logServices = logServices;
        }

        /// <summary>
        /// 根据条件分页获取/导出一个List<PostPostInfoDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
        public async Task<EntityList<PostPostInfoDTO>> GetPostInfoList(int page, int rows, Dictionary<string, string> sort, bool export)
        {
            return await _repository.GetPostInfoList(page, rows, new PostPostInfoListSpecification(), sort, export);
        }

        /// <summary>
        /// 调用领域服务添加/修改MyBlog_Post_PostInfo
        /// </summary>
        /// <param name="item">实体</param>
        /// <returns></returns>
        public async Task ModifyModel(MyBlog_Post_PostInfo item)
        {
            var ID = item.ID;
            await _domainServices.ModifyModel(item);
            await _logServices.LogAsync((ID == Guid.Empty ? "添加" : "修改") + "博文信息", $"Title=[{item.Title}],Summary=[{item.Summary}],Content=[{item.Content}],CategoryId=[{item.CategoryId}],Author=[{item.Author}],Source=[{item.Source}],Liinks=[{item.Liinks}],PubDate=[{item.PubDate}],UpdateTime=[{item.UpdateTime}],Clicks=[{item.Clicks}],ImageUrl=[{item.ImageUrl}],IsComment=[{item.IsComment}],SeoKey=[{item.SeoKey}],SeoDescrition=[{item.SeoDescrition}],CreatTime=[{item.CreateTime}]");
        }

        /// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task DelModelsById(IEnumerable<Guid> ids)
        {
            await _domainServices.DelModelsById(ids);
            await _logServices.LogAsync("删除功能名称", "编号:[" + string.Join(",", ids) + "]");
        }

        #region WebSite
        /// <summary>
        /// 获取博文列表
        /// </summary>
        /// <returns></returns>
        public dynamic GetPostList()
        {
            return _repository.GetPostList();
        }

        /// <summary>
        /// 获取首页推荐博文列表
        /// </summary>
        /// <returns></returns>
        public dynamic GetSelectPostList(int pageIndex, int pageSize)
        {
            return _repository.GetSelectPostList(pageIndex, pageSize);
        }

        /// <summary>
        /// 获取首页热门博文列表
        /// </summary>
        /// <returns></returns>
        public dynamic GetHotPostList()
        {
            return _repository.GetHotPostList();
        }

        /// <summary>
        /// 获取博文列表
        /// </summary>
        /// <param name="categoryId">分类编号</param>
        /// <param name="pageIndex">分页下标</param>
        /// <param name="pageSize">分页大小</param>
        /// <returns></returns>
        public dynamic GetPostLists(Guid categoryId, int pageIndex, int pageSize)
        {
            return _repository.GetPostLists(categoryId, pageIndex, pageSize);
        }
        #endregion

    }
}
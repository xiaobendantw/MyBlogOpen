using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.MainModule.Specification;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.Utility;
namespace TW.MyBlog.Application.Service
{
    public class MyBlog_Post_PostCommentServices : BaseServices<MyBlog_Post_PostComment>, IMyBlog_Post_PostCommentServices
    {
        private readonly IMyBlog_Post_PostCommentDomainServices _domainServices;
        private readonly IMyBlog_Post_PostCommentRepository _repository;
        private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_Post_PostCommentServices(IUnitOfWorkFramework unitOfWork, IMyBlog_Post_PostCommentRepository repository, IMyBlog_Post_PostCommentDomainServices domainService
		,IMyBlog_Manager_LogServices logServices
		)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
			_logServices = logServices;
        }

		/// <summary>
        /// 根据条件分页获取/导出一个List<PostPostCommentDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		public async Task<EntityList<PostPostCommentDTO>> GetPostCommentList(int page, int rows, Dictionary<string, string> sort, bool export)
        {
            return await _repository.GetPostCommentList(page, rows, new PostPostCommentListSpecification(), sort, export);
        }

		/// <summary>
        /// 调用领域服务添加/修改MyBlog_Post_PostComment
        /// </summary>
        /// <param name="item">实体</param>
        /// <returns></returns>
		public async Task ModifyModel(MyBlog_Post_PostComment item)
        {
            var ID = item.ID;
            await _domainServices.ModifyModel(item);
            await _logServices.LogAsync((ID == Guid.Empty ? "添加" : "修改") + "功能名称",$"Comment=[{item.Comment}],CommentIp=[{item.CommentIp}]");
        }

		/// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
		public async Task DelModelsById(IEnumerable<Guid> ids)
        {
            await _domainServices.DelModelsById(ids);
            await _logServices.LogAsync("删除功能名称", "编号:[" + string.Join(",", ids) + "]");
        }
    }
}
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.MainModule.Specification;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.Utility;
namespace TW.MyBlog.Application.Service
{
    public class MyBlog_Post_PostImageRelationServices : BaseServices<MyBlog_Post_PostImageRelation>, IMyBlog_Post_PostImageRelationServices
    {
        private readonly IMyBlog_Post_PostImageRelationDomainServices _domainServices;
        private readonly IMyBlog_Post_PostImageRelationRepository _repository;
        private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_Post_PostImageRelationServices(IUnitOfWorkFramework unitOfWork, IMyBlog_Post_PostImageRelationRepository repository, IMyBlog_Post_PostImageRelationDomainServices domainService
		,IMyBlog_Manager_LogServices logServices
		)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
			_logServices = logServices;
        }

		/// <summary>
        /// 根据条件分页获取/导出一个List<PostPostImageRelationDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		public async Task<EntityList<PostPostImageRelationDTO>> GetPostImageRelationList(int page, int rows, Dictionary<string, string> sort, bool export)
        {
            return await _repository.GetPostImageRelationList(page, rows, new PostPostImageRelationListSpecification(), sort, export);
        }

		/// <summary>
        /// 调用领域服务添加/修改MyBlog_Post_PostImageRelation
        /// </summary>
        /// <param name="item">实体</param>
        /// <returns></returns>
		public async Task ModifyModel(MyBlog_Post_PostImageRelation item)
        {
            var ID = item.ID;
            await _domainServices.ModifyModel(item);
            await _logServices.LogAsync((ID == Guid.Empty ? "添加" : "修改") + "功能名称",$"ImageUrl=[{item.ImageUrl}]");
        }

		/// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
		public async Task DelModelsById(IEnumerable<Guid> ids)
        {
            await _domainServices.DelModelsById(ids);
            await _logServices.LogAsync("删除功能名称", "编号:[" + string.Join(",", ids) + "]");
        }
    }
}
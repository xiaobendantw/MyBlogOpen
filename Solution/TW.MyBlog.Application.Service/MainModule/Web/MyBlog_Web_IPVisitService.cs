using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.MainModule.Specification;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.Utility;
namespace TW.MyBlog.Application.Service
{
    public class MyBlog_Web_IPVisitServices : BaseServices<MyBlog_Web_IPVisit>, IMyBlog_Web_IPVisitServices
    {
        private readonly IMyBlog_Web_IPVisitDomainServices _domainServices;
        private readonly IMyBlog_Web_IPVisitRepository _repository;
        private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_Web_IPVisitServices(IUnitOfWorkFramework unitOfWork, IMyBlog_Web_IPVisitRepository repository, IMyBlog_Web_IPVisitDomainServices domainService
        , IMyBlog_Manager_LogServices logServices
        )
            : base(unitOfWork, repository)
        {
            this._domainServices = domainService;
            this._repository = repository;
            _logServices = logServices;
        }

        /// <summary>
        /// 根据条件分页获取/导出一个List<WebIPVisitDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
        public async Task<EntityList<WebIPVisitDTO>> GetIPVisitList(int page, int rows, Dictionary<string, string> sort, bool export)
        {
            return await _repository.GetIPVisitList(page, rows, new WebIPVisitListSpecification(), sort, export);
        }

        /// <summary>
        /// 调用领域服务添加/修改MyBlog_Web_IPVisit
        /// </summary>
        /// <param name="item">实体</param>
        /// <returns></returns>
        public async Task ModifyModel(MyBlog_Web_IPVisit item)
        {
            var ID = item.ID;
            await _domainServices.ModifyModel(item);
            await _logServices.LogAsync((ID == Guid.Empty ? "添加" : "修改") + "功能名称", $"IP=[{item.IP}]");
        }

        /// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task DelModelsById(IEnumerable<Guid> ids)
        {
            await _domainServices.DelModelsById(ids);
            await _logServices.LogAsync("删除功能名称", "编号:[" + string.Join(",", ids) + "]");
        }

        public async Task<List<dynamic>> GetChartData(DateTime? searchStartCreateTime, DateTime? searchEndCreateTime, Enum_VisitType visitType)
        {
            return await _repository.GetChartData(searchStartCreateTime, searchEndCreateTime, visitType);
        }
    }
}
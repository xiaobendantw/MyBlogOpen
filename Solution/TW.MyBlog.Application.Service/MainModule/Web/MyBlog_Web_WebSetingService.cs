using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.MainModule.Specification;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.Utility;
namespace TW.MyBlog.Application.Service
{
    public class MyBlog_Web_WebSetingServices : BaseServices<MyBlog_Web_WebSeting>, IMyBlog_Web_WebSetingServices
    {
        private readonly IMyBlog_Web_WebSetingDomainServices _domainServices;
        private readonly IMyBlog_Web_WebSetingRepository _repository;
        private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_Web_WebSetingServices(IUnitOfWorkFramework unitOfWork, IMyBlog_Web_WebSetingRepository repository, IMyBlog_Web_WebSetingDomainServices domainService
		,IMyBlog_Manager_LogServices logServices
		)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
			_logServices = logServices;
        }

		/// <summary>
        /// 根据条件分页获取/导出一个List<WebWebSetingDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		public async Task<EntityList<WebWebSetingDTO>> GetWebSetingList(int page, int rows, Dictionary<string, string> sort, bool export)
        {
            return await _repository.GetWebSetingList(page, rows, new WebWebSetingListSpecification(), sort, export);
        }

		/// <summary>
        /// 调用领域服务添加/修改MyBlog_Web_WebSeting
        /// </summary>
        /// <param name="item">实体</param>
        /// <returns></returns>
		public async Task ModifyModel(MyBlog_Web_WebSeting item)
        {
            var ID = item.ID;
            await _domainServices.ModifyModel(item);
            await _logServices.LogAsync((ID == Guid.Empty ? "添加" : "修改") + "网站基本设置",$"Title=[{item.Title}],SummaryTitle=[{item.SummaryTitle}],Copyright=[{item.Copyright}],BackGroundImage=[{item.BackGroundImage}],BackGroudVideo=[{item.BackGroudVideo}],SeoKey=[{item.SeoKey}],SeoDecrtion=[{item.SeoDecrtion}]");
        }

		/// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
		public async Task DelModelsById(IEnumerable<Guid> ids)
        {
            await _domainServices.DelModelsById(ids);
            await _logServices.LogAsync("删除功能名称", "编号:[" + string.Join(",", ids) + "]");
        }
    }
}
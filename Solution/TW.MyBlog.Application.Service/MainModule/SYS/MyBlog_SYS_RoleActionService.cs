﻿using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Application.Service
{
    public class MyBlog_SYS_RoleActionServices : BaseServices<MyBlog_SYS_RoleAction>, IMyBlog_SYS_RoleActionServices
    {
        private readonly IMyBlog_SYS_RoleActionDomainServices _domainServices;
        private readonly IMyBlog_SYS_RoleActionRepository _repository;
        public MyBlog_SYS_RoleActionServices(IUnitOfWorkFramework unitOfWork, IMyBlog_SYS_RoleActionRepository repository, IMyBlog_SYS_RoleActionDomainServices domainService)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
        }

    }
}
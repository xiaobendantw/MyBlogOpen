﻿using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Application.Service
{
    public class MyBlog_SYS_ModuleActionServices : BaseServices<MyBlog_SYS_ModuleAction>, IMyBlog_SYS_ModuleActionServices
    {
        private readonly IMyBlog_SYS_ModuleActionDomainServices _domainServices;
        private readonly IMyBlog_SYS_ModuleActionRepository _repository;
        public MyBlog_SYS_ModuleActionServices(IUnitOfWorkFramework unitOfWork, IMyBlog_SYS_ModuleActionRepository repository, IMyBlog_SYS_ModuleActionDomainServices domainService)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
        }

    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.Utility;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.MainModule.Specification;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common;
using TW.MyBlog.Infrastructure.Common.Enums;

namespace TW.MyBlog.Application.Service
{
    public class MyBlog_SYS_PhoneMessageRecordServices : BaseServices<MyBlog_SYS_PhoneMessageRecord>, IMyBlog_SYS_PhoneMessageRecordServices
    {
        private readonly IMyBlog_SYS_PhoneMessageRecordDomainServices _domainServices;
        private readonly IMyBlog_SYS_PhoneMessageRecordRepository _repository;
        public MyBlog_SYS_PhoneMessageRecordServices(IUnitOfWorkFramework unitOfWork, IMyBlog_SYS_PhoneMessageRecordRepository repository, IMyBlog_SYS_PhoneMessageRecordDomainServices domainService

		)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
        }

        public async Task SendSingleMessage(string telPhone, Enum_PhoneMessageSendType sendType, string sendOpenId, Guid? sendUserId)
        {
            try
            {
                var contents = await _domainServices.SendSingleMessage(telPhone, sendType, Enum_PhoneMessageSendUserType.WeChatMember, sendOpenId,
                    IPHelper.getIPAddr(), sendUserId);
                await unitOfWork.CommitAsync();
                //TODO:接入短信接口

            }
            catch (CustomException ce)
            {
                throw ce;
            }
            catch (Exception e)
            {
                LoggerHelper.Log("短信发送异常:" + e.Message + ",详细错误:" + e.GetBaseException().Message);
                throw new CustomException("短信发送失败,请稍后重试!");
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.MainModule.Specification;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Domain.Model.DTO.Manager;

namespace TW.MyBlog.Application.Service
{
    public class MyBlog_SYS_RoleServices : BaseServices<MyBlog_SYS_Role>, IMyBlog_SYS_RoleServices
    {
        private readonly IMyBlog_SYS_RoleDomainServices _domainServices;
        private readonly IMyBlog_SYS_RoleRepository _repository;
        private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_SYS_RoleServices(IUnitOfWorkFramework unitOfWork, IMyBlog_SYS_RoleRepository repository, IMyBlog_SYS_RoleDomainServices domainService,
            IMyBlog_Manager_LogServices logServices)
            : base(unitOfWork, repository)
        {
            this._domainServices = domainService;
            this._repository = repository;
            _logServices = logServices;
        }
        public async Task<EntityList<RoleDTO>> GetRoleList(int page, int rows, Dictionary<string, string> sort)
        {
            return await _repository.GetRoleList(page, rows, new RoleListSpecification(), sort);
        }

        public async Task AddRole(MyBlog_SYS_Role model, List<MyBlog_SYS_ModuleAction> moduleAction)
        {
            var ID = model.ID;
            await _domainServices.AddRole(model, moduleAction);
            await _logServices.LogAsync((ID == Guid.Empty ? "添加" : "修改") + "角色", $"角色名称{model.RoleName},排序编号{model.OrderID},角色描述{model.Memo}");
        }

        public async Task <List<RoleListDTO>> LoadMangerRoleList(Guid id)
        {
            return await _repository.LoadMangerRoleList(id);
        }

        public async Task DelRole(IEnumerable<Guid> ids)
        {
            var delName = await _domainServices.DelRole(ids);
            await _logServices.LogAsync("删除角色", $"角色名称{string.Join(",", delName)}");
        }
    }
}
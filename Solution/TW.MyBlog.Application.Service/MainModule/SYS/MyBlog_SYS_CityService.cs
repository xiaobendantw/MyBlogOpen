﻿using System.Collections.Generic;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.MainModule.Specification;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;

namespace TW.MyBlog.Application.Service
{
    public class MyBlog_SYS_CityServices : BaseServices<MyBlog_SYS_City>, IMyBlog_SYS_CityServices
    {
        private readonly IMyBlog_SYS_CityDomainServices _domainServices;
        private readonly IMyBlog_SYS_CityRepository _repository;
        //private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_SYS_CityServices(IUnitOfWorkFramework unitOfWork, IMyBlog_SYS_CityRepository repository, IMyBlog_SYS_CityDomainServices domainService
		//,IMyBlog_Manager_LogServices logServices
		)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
			//_logServices = logServices;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.Utility;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Application.Service.CacheService;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.MainModule.Specification;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;

namespace TW.MyBlog.Application.Service
{
    public class MyBlog_SYS_ProvinceServices : BaseServices<MyBlog_SYS_Province>, IMyBlog_SYS_ProvinceServices
    {
        private readonly IMyBlog_SYS_ProvinceDomainServices _domainServices;
        private readonly IMyBlog_SYS_ProvinceRepository _repository;
        private readonly IMyBlog_SYS_BaseCacheService _baseCacheService;
        private readonly IMyBlog_SYS_CityRepository _cityRepository;
        public MyBlog_SYS_ProvinceServices(IUnitOfWorkFramework unitOfWork, IMyBlog_SYS_ProvinceRepository repository, IMyBlog_SYS_ProvinceDomainServices domainService,
            IMyBlog_SYS_BaseCacheService baseCacheService,
            IMyBlog_SYS_CityRepository cityRepository
        //,IMyBlog_Manager_LogServices logServices
        )
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
            _baseCacheService = baseCacheService;
            _cityRepository = cityRepository;
        }

        /// <summary>
        /// 从数据库加载省市列表存入缓存
        /// </summary>
        /// <returns></returns>
        public async Task<Tuple<List<MyBlog_SYS_Province>, List<MyBlog_SYS_City>>> LoadProvinceandCity()
        {
            List<MyBlog_SYS_Province> pubPorv;
            List<MyBlog_SYS_City> pubCity;
            var wxProvince = _baseCacheService.GetCache<List<MyBlog_SYS_Province>>("WxProvince");
            if (wxProvince == null)
            {
                var myprov = await _repository.GetAllAsync();
                var prov = myprov.ToList();
                prov.ForEach(x =>
                {
                    x.PyProvinceName =
                        x.ProvinceName.ToCharArray()
                            .ToList()
                            .ConvertAll(y => y.ToString())
                            .Aggregate((a, b) => PinYinHelper.Get(a) + PinYinHelper.Get(b));
                });
                _baseCacheService.Add("WxProvince", prov);
                pubPorv = prov.ToList();
            }
            else
            {
                pubPorv = wxProvince;
            }
            var wxCity = _baseCacheService.GetCache<List<MyBlog_SYS_City>>("WxCity");
            if (wxCity == null)
            {
                var mycity = await _cityRepository.GetAllAsync();
                var city = mycity.ToList();
                city.ForEach(x =>
                {
                    x.PyCityName =
                        x.CityName.ToCharArray()
                            .ToList()
                            .ConvertAll(y => y.ToString())
                            .Aggregate((a, b) => PinYinHelper.Get(a) + PinYinHelper.Get(b));
                });
                _baseCacheService.Add("WxCity", city);
                pubCity = city;
            }
            else
            {
                pubCity = wxCity;
            }
            return new Tuple<List<MyBlog_SYS_Province>, List<MyBlog_SYS_City>>(pubPorv, pubCity);
        }
    }
}
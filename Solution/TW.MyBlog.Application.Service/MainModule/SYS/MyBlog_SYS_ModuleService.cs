﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.MainModule.Specification;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO.Manager;

namespace TW.MyBlog.Application.Service
{
    public class MyBlog_SYS_ModuleServices : BaseServices<MyBlog_SYS_Module>, IMyBlog_SYS_ModuleServices
    {
        private readonly IMyBlog_SYS_ModuleDomainServices _domainServices;
        private readonly IMyBlog_SYS_ModuleRepository _repository;
        private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_SYS_ModuleServices(IUnitOfWorkFramework unitOfWork, IMyBlog_SYS_ModuleRepository repository, IMyBlog_SYS_ModuleDomainServices domainService,
            IMyBlog_Manager_LogServices logServices)
            : base(unitOfWork, repository)
        {
            this._domainServices = domainService;
            this._repository = repository;
            _logServices = logServices;
        }

        public async Task AddModule(MyBlog_SYS_Module item)
        {
            var ID = item.ID;
            await _domainServices.AddModule(item);
            await _logServices.LogAsync((ID == Guid.Empty ? "添加" : "修改") + "模块", $"模块名称=[{item.ModuleName}],模块地址=[{item.ModuleUrl}],排序编号=[{item.OrderID}],=[{item.ModuleCls}]");
        }

        public async Task<dynamic> GetFatherModule(Guid fatherId, Guid itemId)
        {
            return await _repository.GetFatherModule(fatherId, itemId);
        }

        public async Task<List<MenuModuleListDTO>> LoadModuleList(Guid RoleId)
        {
            return await _repository.LoadModuleList(RoleId);
        }


        public async Task<EntityList<ModuleDTO>> GetModuleList(int page, int rows,Guid pid, Dictionary<string, string> sort)
        {
            return await _repository.GetModuleList(page, rows, new ModuleListSpecification(pid), sort);
        }

        public async Task DelModule(IEnumerable<Guid> delId)
        {
            var delName =
                (await _domainServices.DelModule(delId));
            await _logServices.LogAsync("删除模块", "模块名称:[" + string.Join(",", delName) + "]");
        }
    }
}
﻿using System.Collections.Generic;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.MainModule.Specification;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;

namespace TW.MyBlog.Application.Service
{
    public class MyBlog_SYS_DistrictServices : BaseServices<MyBlog_SYS_District>, IMyBlog_SYS_DistrictServices
    {
        private readonly IMyBlog_SYS_DistrictDomainServices _domainServices;
        private readonly IMyBlog_SYS_DistrictRepository _repository;
        //private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_SYS_DistrictServices(IUnitOfWorkFramework unitOfWork, IMyBlog_SYS_DistrictRepository repository, IMyBlog_SYS_DistrictDomainServices domainService
		//,IMyBlog_Manager_LogServices logServices
		)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
			//_logServices = logServices;
        }
    }
}
﻿using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.MainModule.Specification;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;

namespace TW.MyBlog.Application.Service
{
    public class MyBlog_SYS_WeChatSettingServices : BaseServices<MyBlog_SYS_WeChatSetting>, IMyBlog_SYS_WeChatSettingServices
    {
        private readonly IMyBlog_SYS_WeChatSettingDomainServices _domainServices;
        private readonly IMyBlog_SYS_WeChatSettingRepository _repository;
        private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_SYS_WeChatSettingServices(IUnitOfWorkFramework unitOfWork, IMyBlog_SYS_WeChatSettingRepository repository, IMyBlog_SYS_WeChatSettingDomainServices domainService
            ,IMyBlog_Manager_LogServices logServices
            )
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
            _logServices = logServices;
        }

        public async Task ModifyModel(MyBlog_SYS_WeChatSetting item)
        {
            _domainServices.ModifyModel(item);
            await _logServices.LogAsync("微信公众号设置", $"公众号名称=[{item.WeChatName}],公众号原始ID=[{item.WxChat_gID}],微信Token=[{item.Token}],微信绑定Url=[{item.BindUrl}],AppID=[{item.AppID}],AppSecret=[{item.AppSecret}]");
        }
    }
}
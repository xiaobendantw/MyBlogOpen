﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.MainModule.Specification;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Domain.Model.DTO.Manager;

namespace TW.MyBlog.Application.Service
{
    public class MyBlog_Manager_LogServices : BaseServices<MyBlog_Manager_Log>, IMyBlog_Manager_LogServices
    {
        private readonly IMyBlog_Manager_LogDomainServices _domainServices;
        private readonly IMyBlog_Manager_LogRepository _repository;
        public MyBlog_Manager_LogServices(IUnitOfWorkFramework unitOfWork, IMyBlog_Manager_LogRepository repository, IMyBlog_Manager_LogDomainServices domainService)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
        }
		public async Task<EntityList<LogDTO>> GetLogList(int page, int rows, Dictionary<string, string> sort, DateTime? searchStartCreateTime, DateTime? searchEndCreateTime, string searchText)
        {
            return await _repository.GetLogList(page, rows, new LogListSpecification(searchStartCreateTime, searchEndCreateTime, searchText), sort);
        }

        public async Task LogAsync(string title, string Contents, bool isBulk = false, LoginUserDTO mydto = null)
        {
            _domainServices.Log(title, Contents, mydto);
            await unitOfWork.CommitAsync();
        }
        public async Task<List<dynamic>> ManagerLogChart(DateTime? searchStartCreateTime, DateTime? searchEndCreateTime)
        {
            return await _repository.ManagerLogChart(new ManagerLogChartSpecification(searchStartCreateTime, searchEndCreateTime));
        }
    }
}
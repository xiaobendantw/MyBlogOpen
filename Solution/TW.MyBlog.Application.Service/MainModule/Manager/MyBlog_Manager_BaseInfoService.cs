﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using TW.MyBlog.Application.BaseService;
using AutoMapper;
using TW.MyBlog.Application.Service.CacheService;
using Domain.MainModule.Specification;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Infrastructure.Common;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO.Manager;
using Newtonsoft.Json;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Domain.MainModule.Specification;
using TW.Utility;

namespace TW.MyBlog.Application.Service
{
    public class MyBlog_Manager_BaseInfoServices : BaseServices<MyBlog_Manager_BaseInfo>, IMyBlog_Manager_BaseInfoServices
    {
        private readonly IMyBlog_Manager_BaseInfoDomainServices _domainServices;
        private readonly IMyBlog_Manager_BaseInfoRepository _repository;
        private readonly IMyBlog_Manager_LogServices _logServices;
        private readonly IMyBlog_SYS_BaseCacheService _baseCacheService;
        public MyBlog_Manager_BaseInfoServices(IUnitOfWorkFramework unitOfWork, IMyBlog_Manager_BaseInfoRepository repository,
            IMyBlog_Manager_BaseInfoDomainServices domainService, IMyBlog_Manager_LogServices logServices, IMyBlog_SYS_BaseCacheService baseCacheService)
            : base(unitOfWork, repository)
        {
            this._domainServices = domainService;
            this._repository = repository;
            _logServices = logServices;
            _baseCacheService = baseCacheService;
        }
        /// <summary>
        /// 异步检测登录
        /// </summary>
        /// <param name="loginName"></param>
        /// <param name="passWord"></param>
        /// <param name="remmberMe"></param>
        /// <param name="pwdIsMd5"></param>
        /// <param name="validateCode"></param>
        /// <param name="configureAwait"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public async Task CheckLogin(string loginName, string passWord, bool remmberMe, bool? pwdIsMd5 = false, string validateCode = null)
        {
            //调用登录领域方法
            var result = await _domainServices.CheckLogin(loginName, passWord, remmberMe, pwdIsMd5, validateCode);
            await _logServices.LogAsync("账号登录", "登录名:" + loginName + ",登录时间:" + DateTime.Now + ",登录IP:" + IPHelper.getIPAddr(), false, result);
            await Common.LoginIn(result.LoginName, result.ID, result.PassWord, result.LoginIp);
            _baseCacheService.CacheLogin(result);
        }
        /// <summary>
        /// 修改用户基本信息
        /// </summary>
        /// <param name="ID"></param>
        /// <param name="nickName"></param>
        /// <param name="newPassword"></param>
        /// <param name="confirmPassword"></param>
        /// <param name="userChangeType"></param>
        /// <param name="oldpassword"></param>
        /// <returns></returns>
        public async Task ChangeUserInfo(Guid ID, string nickName, string newPassword, string confirmPassword, int userChangeType, string oldpassword)
        {
            var result = await _domainServices.ChangeUserInfo(ID, nickName, newPassword, confirmPassword, userChangeType, oldpassword);
            await _logServices.LogAsync("修改用户基本信息", "昵称:" + nickName + ",");
            await _baseCacheService.UpdateLogin(result, 1);
        }
        /// <summary>
        /// 获取管理员分页信息集
        /// </summary>
        /// <param name="page"></param>
        /// <param name="rows"></param>
        /// <param name="sort"></param>
        /// <param name="State"></param>
        /// <param name="Role"></param>
        /// <param name="sealogin"></param>
        /// <returns></returns>
        public async Task<EntityList<BaseInfoDTO>> GetManagerList(int page, int rows, Dictionary<string, string> sort, Enum_ManagerUserState? State, Guid? Role, string sealogin)
        {
            return await _repository.GetBaseInfoList(page, rows, new BaseInfoListSpecification(State, Role, sealogin), sort);
        }
        /// <summary>
        /// 添加/修改一个管理员信息
        /// </summary>
        /// <param name="model"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task AddMangerBaseInfo(MyBlog_Manager_BaseInfo model, Guid id)
        {
            var ID = model.ID;
            var result = await _domainServices.AddMangerBaseInfo(model, id);
            await _logServices.LogAsync($"{(ID == Guid.Empty ? "新增" : "修改")}用户", $"用户名称:{model.NickName},状态:{model.State.GetLocalizedDescription()}");
            await _baseCacheService.UpdateLogin(result, 2);
        }
        /// <summary>
        /// 逻辑删除管理员
        /// </summary>
        /// <param name="deld"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        public async Task DelManager(IEnumerable<Guid> deld, Guid uid)
        {
            var delName = await _domainServices.DelManager(deld, uid);
            await _logServices.LogAsync("删除会员", $"登录名:[" + string.Join(",", delName) + "]");
            _baseCacheService.RemoveLogin(deld);
        }
    }
}
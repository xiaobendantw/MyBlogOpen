﻿using System;
using System.Collections.Generic;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Base;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;

namespace TW.MyBlog.Application.Service
{
    public class MyBlog_Manager_RoleServices : BaseServices<MyBlog_Manager_Role>, IMyBlog_Manager_RoleServices
    {
        private readonly IMyBlog_Manager_RoleDomainServices _domainServices;
        private readonly IMyBlog_Manager_RoleRepository _repository;
        public MyBlog_Manager_RoleServices(IUnitOfWorkFramework unitOfWork, IMyBlog_Manager_RoleRepository repository, IMyBlog_Manager_RoleDomainServices domainService)
            : base(unitOfWork, repository)
        {
            this._domainServices = domainService;
            this._repository = repository;
        }

    }
}
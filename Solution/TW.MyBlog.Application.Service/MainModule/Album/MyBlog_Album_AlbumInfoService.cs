using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.MainModule.Specification;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.Utility;
namespace TW.MyBlog.Application.Service
{
    public class MyBlog_Album_AlbumInfoServices : BaseServices<MyBlog_Album_AlbumInfo>, IMyBlog_Album_AlbumInfoServices
    {
        private readonly IMyBlog_Album_AlbumInfoDomainServices _domainServices;
        private readonly IMyBlog_Album_AlbumInfoRepository _repository;
        private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_Album_AlbumInfoServices(IUnitOfWorkFramework unitOfWork, IMyBlog_Album_AlbumInfoRepository repository, IMyBlog_Album_AlbumInfoDomainServices domainService
		,IMyBlog_Manager_LogServices logServices
		)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
			_logServices = logServices;
        }

		/// <summary>
        /// 根据条件分页获取/导出一个List<AlbumAlbumInfoDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		public async Task<EntityList<AlbumAlbumInfoDTO>> GetAlbumInfoList(int page, int rows, Dictionary<string, string> sort, bool export)
        {
            return await _repository.GetAlbumInfoList(page, rows, new AlbumAlbumInfoListSpecification(), sort, export);
        }

		/// <summary>
        /// 调用领域服务添加/修改MyBlog_Album_AlbumInfo
        /// </summary>
        /// <param name="item">实体</param>
        /// <returns></returns>
		public async Task ModifyModel(MyBlog_Album_AlbumInfo item)
        {
            var ID = item.ID;
            await _domainServices.ModifyModel(item);
            await _logServices.LogAsync((ID == Guid.Empty ? "添加" : "修改") + "功能名称",$"ImageUrl=[{item.ImageUrl}],ImageDecription=[{item.ImageDecription}],IsOpen=[{item.IsOpen}]");
        }

		/// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
		public async Task DelModelsById(IEnumerable<Guid> ids)
        {
            await _domainServices.DelModelsById(ids);
            await _logServices.LogAsync("删除功能名称", "编号:[" + string.Join(",", ids) + "]");
        }
    }
}
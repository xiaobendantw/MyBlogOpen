using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Domain.Model.DTO;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Domain.MainModule.Specification;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using TW.MyBlog.Application.BaseService;
using TW.MyBlog.Domain.Base;
using TW.Utility;
namespace TW.MyBlog.Application.Service
{
    public class MyBlog_Album_AlbumCategoryServices : BaseServices<MyBlog_Album_AlbumCategory>, IMyBlog_Album_AlbumCategoryServices
    {
        private readonly IMyBlog_Album_AlbumCategoryDomainServices _domainServices;
        private readonly IMyBlog_Album_AlbumCategoryRepository _repository;
        private readonly IMyBlog_Manager_LogServices _logServices;
        public MyBlog_Album_AlbumCategoryServices(IUnitOfWorkFramework unitOfWork, IMyBlog_Album_AlbumCategoryRepository repository, IMyBlog_Album_AlbumCategoryDomainServices domainService
		,IMyBlog_Manager_LogServices logServices
		)
            : base(unitOfWork, repository)
        {
			this._domainServices = domainService;
            this._repository = repository;
			_logServices = logServices;
        }

		/// <summary>
        /// 根据条件分页获取/导出一个List<AlbumAlbumCategoryDTO>
        /// </summary>
        /// <param name="page">页码</param>
        /// <param name="rows">条目</param>
        /// <param name="sort">表达式排序</param>
        /// <param name="export">是否是导出</param>
        /// <returns></returns>
		public async Task<EntityList<AlbumAlbumCategoryDTO>> GetAlbumCategoryList(int page, int rows, Dictionary<string, string> sort, bool export)
        {
            return await _repository.GetAlbumCategoryList(page, rows, new AlbumAlbumCategoryListSpecification(), sort, export);
        }

		/// <summary>
        /// 调用领域服务添加/修改MyBlog_Album_AlbumCategory
        /// </summary>
        /// <param name="item">实体</param>
        /// <returns></returns>
		public async Task ModifyModel(MyBlog_Album_AlbumCategory item)
        {
            var ID = item.ID;
            await _domainServices.ModifyModel(item);
            await _logServices.LogAsync((ID == Guid.Empty ? "添加" : "修改") + "功能名称",$"CategoryName=[{item.CategoryName}],CategoryDecrtion=[{item.CategoryDecrtion}]");
        }

		/// <summary>
        /// 逻辑删除实体
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
		public async Task DelModelsById(IEnumerable<Guid> ids)
        {
            await _domainServices.DelModelsById(ids);
            await _logServices.LogAsync("删除功能名称", "编号:[" + string.Join(",", ids) + "]");
        }
    }
}
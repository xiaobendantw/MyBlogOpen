﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TW.MyBlog.Domain.Base
{
    public interface IUnitOfWorkFramework
    {
        Task CommitAsync();
        void Commit();
    }
}

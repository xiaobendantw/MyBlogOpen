﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace TW.MyBlog.Domain.Base
{
    public class SortParms<T> where T : class, new()
    {
        public Expression<Func<T, dynamic>> Func { get; set; }
        public bool ISDESC { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TW.MyBlog.Domain.Base
{
    public interface IEntity
    {
        object Key { get; }
    }
}

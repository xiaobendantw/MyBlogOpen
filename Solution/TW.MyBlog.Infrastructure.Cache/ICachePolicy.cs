﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TW.MyBlog.Infrastructure.Cache
{
    public interface ICachePolicy
    {
        void Add<T>(string key, T value);

        void Add<T>(string key, T value, DateTime dt);

        void Add<T>(string key, T value, TimeSpan dt);

        T Get<T>(string key) where T : class;


        void Add(string key, object value);

        void Add(string key, object value, DateTime dt);

        void Add(string key, object value, TimeSpan dt);

        object Get(string key);

        void Delete(string key);

    }
}

﻿using TW.Utility;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace TW.MyBlog.Infrastructure.Cache
{
    public class HttpRunTimeCachePolicy : ICachePolicy
    {
        #region ICachePolicy 成员

        public void Add<T>(string key, T value)
        {
            new DataCache().SetCache(key, value);
        }

        public void Add<T>(string key, T value, DateTime dt)
        {
            new DataCache().SetCache(key, value, new TimeSpan(dt.Ticks));
        }

        public void Add<T>(string key, T value, TimeSpan dt)
        {
            new DataCache().SetCache(key, value, dt);
        }

        public T Get<T>(string key) where T : class
        {
            try
            {
                return (T)new DataCache().GetCache<T>(key);
            }
            catch (Exception e)
            {
                LoggerHelper.Log("cache异常" + e.GetBaseException().Message);
                new DataCache().RemoveCache(key);
                return default(T);
            }

        }

        public void Add(string key, object value)
        {
            new DataCache().SetCache(key, value);
        }

        public void Add(string key, object value, DateTime dt)
        {
            new DataCache().SetCache(key, value, new TimeSpan(dt.Ticks));
        }

        public void Add(string key, object value, TimeSpan dt)
        {
            new DataCache().SetCache(key, value, dt);
        }

        public object Get(string key)
        {
            return new DataCache().GetCache<object>(key);
        }

        public void Delete(string key)
        {
            new DataCache().RemoveCache(key);
        }

        #endregion
    }
}

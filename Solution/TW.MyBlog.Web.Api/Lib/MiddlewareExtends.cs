﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;

namespace TW.MyBlog.Web.Api.Lib
{
    public static class MiddlewareExtends
    {
        public static IApplicationBuilder ConvertResponse(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<CustomerConvertMiddleware>();
        }
    }
}

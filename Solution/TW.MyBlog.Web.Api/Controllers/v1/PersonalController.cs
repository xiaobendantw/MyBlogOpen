﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Web.Api.Dto;
using TW.MyBlog.Web.Api.Lib;
using TW.Utility;

namespace TW.MyBlog.Web.Api.Controllers.v1
{
    [Route("api/Client/v1")]//指定路由配置
    public class PersonalController : BaseApiController
    {
        private readonly IMyBlog_Post_PostInfoServices _postinfoServices;
        private readonly IMyBlog_Web_IPVisitServices _blogWebIpVisitServices;
        public PersonalController(IMyBlog_Post_PostInfoServices postinfoServices, IMyBlog_Web_IPVisitServices blogWebIpVisitServices)
        {
            _postinfoServices = postinfoServices;
            _blogWebIpVisitServices = blogWebIpVisitServices;
        }
        /// <summary>
        /// 首页访问增加
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("AddIPVisit")]
        public APITipResult AddIPVisit()
        {
            return Do<EmptyDto>((dto, result) =>
            {
                _blogWebIpVisitServices.Add(new MyBlog_Web_IPVisit() { IP = IPHelper.getIPAddr(), VisitType = Enum_VisitType.Home, CreateTime = DateTime.Now });
                result.message = "操作成功!";
            });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TW.MyBlog.Application.Service.Interface;
using TW.MyBlog.Domain.Model;
using TW.MyBlog.Infrastructure.Common.Enums;
using TW.MyBlog.Web.Api.Dto;
using TW.MyBlog.Web.Api.Dto.Test;
using TW.MyBlog.Web.Api.Dto.WebSite;
using TW.MyBlog.Web.Api.Lib;
using TW.Utility;

namespace TW.MyBlog.Web.Api.Controllers.v1
{

    [Route("api/Client/v1")]//指定路由配置
    public class PostController : BaseApiController
    {
        private readonly IMyBlog_Post_PostInfoServices _postinfoServices;
        private readonly IMyBlog_Post_PostCategoryServices _categoryServices;
        private readonly IMyBlog_Web_IPVisitServices _myBlogWebIpVisitServices;
        private readonly IMyBlog_Reptilian_InfoServices _reptilianInfoServices;
        private readonly IMyBlog_Web_LiveMessageServices _liveMessageServices;
        public PostController(IMyBlog_Post_PostInfoServices postinfoServices, IMyBlog_Post_PostCategoryServices categoryServices, IMyBlog_Web_IPVisitServices myBlogWebIpVisitServices, IMyBlog_Reptilian_InfoServices reptilianInfoServices, IMyBlog_Web_LiveMessageServices liveMessageServices)
        {
            _postinfoServices = postinfoServices;
            _categoryServices = categoryServices;
            _myBlogWebIpVisitServices = myBlogWebIpVisitServices;
            _reptilianInfoServices = reptilianInfoServices;
            _liveMessageServices = liveMessageServices;
        }


        [HttpGet, Route("Get")]
        public string Get()
        {
            return "我是服务器v1";
        }

        /// <summary>
        /// 获取首页最新博文
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("GetNewPostList")]
        public APITipResult GetNewPostList()
        {
            return Do<EmptyDto>((dto, result) =>
           {
               result.data = _postinfoServices.GetPostList();
               result.message = "操作成功!";
           });
        }
        /// <summary>
        /// 获取首页推荐博文
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("GetSelectPostList")]
        public APITipResult GetSelectPostList()
        {
            return Do<PageDto>((dto, result) =>
            {
                result.data = _postinfoServices.GetSelectPostList(dto.pageIndex, dto.pageSize);
                result.message = "操作成功!";
            });
        }
        /// <summary>
        /// 获取首页热门博文
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("GetHotPostList")]
        public APITipResult GetHotPostList()
        {
            return Do<EmptyDto>((dto, result) =>
            {
                result.data = _postinfoServices.GetHotPostList();
                result.message = "操作成功!";
            });
        }
        /// <summary>
        /// 获取博文详情
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("GetPostDetial")]
        public APITipResult GetPostDetial()
        {
            return Do<DetialDto>((x, result) =>
          {
              var model = _postinfoServices.GetByKey(x.id);
              result.data = new { content = model.Content, title = model.Title, pubTime = model.PubDate?.ToString("yyyy-MM-dd HH:mm"), model.Source, model.Author, model.Clicks };
              result.message = "操作成功!";
          });
        }
        /// <summary>
        /// 增加博文点击数
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("AddPostClicks")]
        public APITipResult AddPostClicks()
        {
            return Do<DetialDto>((x, result) =>
            {
                var model = _postinfoServices.GetByKey(x.id);
                model.Clicks += 1;
                _postinfoServices.Update(model, a => a.Clicks);
                _myBlogWebIpVisitServices.Add(new MyBlog_Web_IPVisit() { IP = IPHelper.getIPAddr(), VisitType = Enum_VisitType.Blog, CreateTime = DateTime.Now });
                result.message = "操作成功!";
            });
        }
        /// <summary>
        /// 获取博文列表
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("GetPostLists")]
        public APITipResult GetPostLists()
        {
            return Do<PostListDto>((x, result) =>
            {
                result.data = _postinfoServices.GetPostLists(x.categoryId, x.pageIndex, x.pageSize);
                result.message = "操作成功!";
            });
        }

        /// <summary>
        /// 获取博文分类
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("GetPostCategory")]
        public APITipResult GetPostCategory()
        {
            return Do<EmptyDto>((x, result) =>
            {
                result.data = _categoryServices.GetPostCategory();
                result.message = "操作成功!";
            });
        }
        /// <summary>
        /// 获取佳句
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("GetBeautifulsentence")]
        public APITipResult GetBeautifulsentence()
        {
            return Do<PageDto>((x, result) =>
            {
                result.data = _reptilianInfoServices.GetBeautifulsentence(x.pageIndex, x.pageSize);
                result.message = "操作成功!";
            });
        }
        /// <summary>
        /// 获取留言列表
        /// </summary>
        /// <returns></returns>
        [HttpPost, Route("GetLiveMessage")]
        public APITipResult GetLiveMessage()
        {
            return Do<PageDto>((x, result) =>
            {
                result.data = _liveMessageServices.GetLiveMessage(x.pageIndex, x.pageSize);
                result.message = "操作成功!";
            });
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using TW.MyBlog.Web.Api.Lib;

namespace TW.MyBlog.Web.Api.Controllers.v2
{

    [Route("api/Client/v2")]//指定路由配置
    public class PostController : BaseApiController
    {
        [HttpGet, Route("Get")]
        public string Get()
        {
            return "我是服务器v2";
        }

    }
}
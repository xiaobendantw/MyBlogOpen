﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Domain.MainModule.Interface;
using TW.MyBlog.Web.Api.Lib;
using TW.Utility;
using Microsoft.AspNetCore.Mvc;
using TW.MyBlog.Web.Api.Dto.Test;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TW.MyBlog.Web.Api.Controllers
{
    [Route("api/Client")]
    public class HomeController : BaseApiController
    {
        public HomeController()
        {
            
        }
        // GET: api/values
        [HttpGet, Route("Get")]
        public string Get()
        {
            return "我是服务器C";
        }
        [HttpPost, Route("Post")]
        public async Task<APITipResult> Post()
        {
            return await DoAsync<MyTestInputDto>((dto, result) =>
            {
                result.data = new MyTestOutputDto() {Name = "王军", AgeValue = 5};
                result.message = "操作成功!";
            });
        }

    }
}

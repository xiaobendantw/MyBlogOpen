﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Web.Api.Lib;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Web.Api.Dto.Test
{
    /// <summary>
    /// API接口输入参数
    /// </summary>
    public class MyTestInputDto : BaseDto
    {
        [BaseCustom(DisplayName = "用户编号", MaxLen = 36, MinLen = 36, NullField = false, NullValue = false)]
        public Guid Id { get; set; }

        [BaseCustom(DisplayName = "用户曾用名", MaxLen = 10, MinLen = 0, NullField = false, NullValue = false)]
        public List<string> Names { get; set; }
        [BaseCustom(DisplayName = "用户年龄", MaxLen = 3, MinLen = 1, NullField = false, NullValue = false)]
        public int Age { get; set; }
        [BaseCustom(DisplayName = "用户性别", MaxLen = 5, MinLen = 4, NullField = false, NullValue = false)]
        public bool Sex { get; set; }
        public virtual List<ChildOne> MyChildOnes { get; set; }
        public virtual ChildTwo MyChildTwo { get; set; }
    }
    public class ChildOne
    {
        [BaseCustom(DisplayName = "用户成员姓名", MaxLen = 10, MinLen = 0, NullField = false, NullValue = false)]
        public string Names { get; set; }
    }

    public class ChildTwo
    {
        [BaseCustom(DisplayName = "用户成员姓名(业主)", MaxLen = 10, MinLen = 0, NullField = false, NullValue = false)]
        public string Names { get; set; }
    }

    /// <summary>
    /// API接口输出参数
    /// </summary>
    public class MyTestOutputDto
    {
        /// <summary>
        /// 用户姓名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 用户年龄
        /// </summary>
        public int AgeValue { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Web.Api.Lib;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Web.Api.Dto.WebSite
{
    public class PostListDto : BaseDto
    {
        [BaseCustom(DisplayName = "分类编号", MaxLen = 36, MinLen = 36, NullField = false, NullValue = false)]
        public Guid categoryId { get; set; }
        [BaseCustom(DisplayName = "分页下标", MinLen = 1, NullField = true, NullValue = true)]
        public int pageIndex { get; set; }
        [BaseCustom(DisplayName = "分页大小", MinLen = 1, NullField = true, NullValue = true)]
        public int pageSize { get; set; }

    }
}

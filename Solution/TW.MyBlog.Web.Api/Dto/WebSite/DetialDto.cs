﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TW.MyBlog.Web.Api.Lib;
using TW.Utility.CustomerAttribute;

namespace TW.MyBlog.Web.Api.Dto
{
    public class DetialDto: BaseDto
    {
        [BaseCustom(DisplayName = "编号", MaxLen = 36, MinLen = 36, NullField = false, NullValue = false)]
        public Guid id { get; set; }
    }
}
